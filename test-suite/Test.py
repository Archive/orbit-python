#!/usr/bin/env python

import unittest,os, time

import BasicTest,ORBPOATest, ClientServer, BasicORB

suite = unittest.TestSuite( (BasicTest.bt,ORBPOATest.orbpoa,
	ClientServer.cs, BasicORB.ot) )
runner = unittest.TextTestRunner(verbosity=10)
runner.run(suite)
