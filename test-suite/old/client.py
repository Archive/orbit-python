#! /usr/bin/python

import CORBA
import sys
import time
import foobar

orb = CORBA.ORB_init ((), CORBA.ORB_ID)
poa = orb.resolve_initial_references("RootPOA")

ior = open ("ior").readline ()
import foobar
ref = orb.string_to_object (ior)
print ref

b = None
n = sys.argv [1]
n = int (n)
type = sys.argv [2]
i = 0
while i < n:
    i = i + 1
    if (i % 1000) == 0 or i <= 5:
        print i, '/', n, '(' + str (i * 100.0 / n) + ' %)'
    if type == 'm':
        b = ref.meth ()
    if type == 'M':
        b = ref.meth_with_args (i, "He's not pining, he's passed on!  This parrot is no more!  He has ceased to be!  It's a stiff!  Bereft of life!  He rests in peace!  He snuffed it! [...] He's extinct in his entirety!  This is an ex-parrot!")
    if type == 'g':
        b = ref.attr
##     if type == 'wg':
##         b = ref.ws
##         f = open ("result-u8", "w")
##         f.write (b.encode ("utf-8"))
    if type == 'G':
        b = ref._get_attr ()
    if type == 's':
        ref.attr = i
    if type == 'S':
        ref._set_attr (i)
    if type == 'c':
        ref.create_and_destroy ()
    if type == 'struct':
        zog = ref.gimme ()
        strs = list (zog.strs)
        strs.append ("bla")
        zog.strs = tuple (strs)
        # print str (zog.strs)
        ref.pri (zog)
        zog = ref.raz (zog)
        ref.pri (zog)
        # print str (zog.strs)

print "OK, done.  Sleeping for a while."
#if b != None: print b.encode ("latin-1")
time.sleep (1)
print "Well, I'm done.  Every delay from now on is due to memory freeing."
