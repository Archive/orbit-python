#! /usr/bin/python

import CORBA
import foobar, foobar__POA

orb = CORBA.ORB_init ((), CORBA.ORB_ID)
poa = orb.resolve_initial_references("RootPOA")

print "IDL loaded OK"

count = 0

class baz (foobar__POA.baz):
    def __init__ (self):
        self.attr = 0
##         f = open ("data-u8")
##         d = f.readline () + f.readline ()
##         self.ws = unicode (d, "utf-8")
##         print self.ws.encode ("utf-8")
    def _get_attr (self):
        return self.attr
    def _set_attr (self, a):
        self.attr = a
    def meth (self):
        global count
        count = count + 1
        return count
    def meth_with_args (self, arg_long, arg_string):
        global count
        count = count + 1
        return count
    def create_and_destroy (self):
        instance = baz ()
        poa.activate_object (instance)
        ref = poa.servant_to_reference (instance)
        poa.deactivate_object (instance)
    def gimme (self):
        z = foobar.zog (l = 0, strs = ())
        return z
    def pri (self, z):
##         print "l = ", z.l
##         print "strs = ", str (z.strs)
        pass
    def raz (self, z):
        z.l = 0
        z.strs = ()
        return z

metasyntactic_variable = baz ()
servant = foobar__POA.baz (metasyntactic_variable)
print "ms", metasyntactic_variable
print "servant", servant
servant.attr = 2
print "this", servant._this ()
# poa.activate_object (servant)
ref = poa.servant_to_reference (servant)
print "ref", ref
servant.attr = 2
ior = orb.object_to_string (ref)
open ("ior", "w").write (ior)

poa._get_the_POAManager().activate ()
print "OK, running."
orb.run ()
