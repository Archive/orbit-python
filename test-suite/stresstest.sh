#!/bin/sh
if [ x$1 == x-nopp ]; then
	shift
	export PYTHONPATH=
else
	export PYTHONPATH=../src:$PYTHONPATH
fi
export IDLPATH=.:$IDLPATH
python2.2 ./test-server.py &
SERVERPID=$!
sleep 1s
DEBUG=

# simple hack to get it to run inside gdb
if [ x$1 == x-gdb ]; then
	shift
	gdb python2.2 << __EOF__
r Stress.py $*
where
__EOF__
else
	python2.2 Stress.py $*
fi

rm ior
rm pid
kill $SERVERPID
