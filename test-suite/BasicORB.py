#! /usr/bin/python
import unittest, sys
import CORBA

# Tests ORB initialization and POA resolution. Separate because of the
# need to import CORBA and the fact that ORB_init() and resolve_* is a
# major prerequisite

class BasicORB(unittest.TestCase):
	def test_O1_ORBinit(self):
		orb = CORBA.ORB_init(["XXX","YYY"],CORBA.ORB_ID)

	def test_O2_DoubleORBinit(self):
		orb = CORBA.ORB_init(["XXX","YYY"],CORBA.ORB_ID)
		orb2 = CORBA.ORB_init(["YYY","XXX"],CORBA.ORB_ID)
	
	def test_O3_ORBinitParameterless(self):
		orb = CORBA.ORB_init()
	
	def test_O4_DoubleORBinitParameterless(self):
		orb = CORBA.ORB_init()
		orb2 = CORBA.ORB_init()

	def test_O5_ResolveRootPOA(self):
		orb = CORBA.ORB_init(["XXX","YYY"],CORBA.ORB_ID)
		poa = orb.resolve_initial_references("RootPOA")

	def test_O6_DoubleResolveRootPOA(self):
		orb = CORBA.ORB_init(["XXX","YYY"],CORBA.ORB_ID)
		poa = orb.resolve_initial_references("RootPOA")
		poa2 = orb.resolve_initial_references("RootPOA")

ot = unittest.makeSuite(BasicORB,'test')

if __name__ == "__main__":
	runner = unittest.TextTestRunner()
	runner.run(ot)
