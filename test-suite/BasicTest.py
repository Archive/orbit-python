#! /usr/bin/python
import unittest, sys

# This class tests all basic import (including IDL parsing) and simple
# servant instantiation (no activation).

# An interesting point: the first import CORBA we do inside a test case
# includes CORBA in sys.modules, but the local name is lost on method
# exit.  However, the IDL import mechanism still works. That's why we
# have to keep importing CORBA if we need the local name.

class BasicTest(unittest.TestCase):

    # Just testing if unittest is okay.
    def test_B0_Sanity(self):
        assert(1)

    # Test import. Stress repeats do not leak because they will not
    # really re-import
    def test_B1_ImportCORBA(self):
        import CORBA
        assert('CORBA' in dir())
    
    def test_B2_DoubleImportCORBA(self):
        import CORBA
        import CORBA
        assert('CORBA' in dir())

    # Do we use PortableServer? Does it need a test?
    def test_B3_ImportCORBAPortableServer(self):
        import CORBA, PortableServer
        assert('CORBA' in dir())
        assert('PortableServer' in dir())

    # Test automatically importing CORBA base classes
    def test_B4_AutoImportFromIDL(self):
        import BasicTestIDL
        assert('BasicTestIDL' in dir())
        import BasicTestIDL__POA
        assert('BasicTestIDL__POA' in dir())
        assert('Foo' in dir(BasicTestIDL))

    # Test manually importing the IDL
    # Uses another extension so we don't trigger auto-load
    def test_B5_ImportIDL(self):
        # do not include this test for 0.3.0 (tack)
        return 

        import CORBA
        # Breaks stress test - why?
        self.assertRaises(ImportError, self.doImportIDLTest)
        CORBA._load_idl('basic.idx')
        import LoadIDLTest
        assert('LoadIDLTest' in dir())
        assert('Foo' in dir(LoadIDLTest))
        # This causes the next _load_idl to SEGV on stress tests.
        del sys.modules["LoadIDLTest"]
        del sys.modules["LoadIDLTest__POA"]
    
    # Helper function for catching exceptions
    def doImportIDLTest(self):
        import LoadIDLTest

    # Test import of missing class 
    def test_B6_ImportMissing(self):
        self.assertRaises(ImportError, self.doImportMissing)

    # Helper for test 5
    def doImportMissing(self):
        import MissingFromIDL

    # 
    def test_B7_InstantiateFromIDLClass(self):
        import BasicTestIDL
        # Tests instantiating base classes (no servant)
        f = BasicTestIDL.Foo()
        import BasicTestIDL__POA
        f = BasicTestIDL__POA.Foo()
    
    def test_B8_DoubleInstFromIDLClass(self):
        import BasicTestIDL, BasicTestIDL__POA
        f = BasicTestIDL.Foo()
        g = BasicTestIDL.Foo()
        g = BasicTestIDL.Foo()
        h = BasicTestIDL__POA.Foo()
        h = BasicTestIDL__POA.Foo()
        i = BasicTestIDL__POA.Foo()

    # Test inheritance of CORBA class
    def test_B9_InstServantByInheritance(self):
        import BasicTestIDL, BasicTestIDL__POA
        class Foo(BasicTestIDL__POA.Foo):
            pass
        f = Foo()

    def test_B10_DoubleInstServantByInherit(self):
        import BasicTestIDL, BasicTestIDL__POA
        class Foo(BasicTestIDL__POA.Foo):
            pass
        f = Foo()
        f = Foo()
        g = Foo()

    # Test basic delegation
    def test_B11_InstServantByDeleg(self):
        import BasicTestIDL, BasicTestIDL__POA
        class Foo:
            pass
        f = BasicTestIDL__POA.Foo(Foo())
    
    def test_B12_DoubleInstServantByDeleg(self):
        import BasicTestIDL, BasicTestIDL__POA
        class Foo:
            pass
        f = BasicTestIDL__POA.Foo(Foo())
        f = BasicTestIDL__POA.Foo(Foo())
        g = BasicTestIDL__POA.Foo(Foo())
    
    def test_B13_CompareInheritDelegType(self):
        import BasicTestIDL, BasicTestIDL__POA
        class Bar:
            pass
        class Foo(BasicTestIDL__POA.Foo):
            pass
        f = BasicTestIDL__POA.Foo(Bar())
        f = BasicTestIDL__POA.Foo(Bar())
        g = Foo()
        g = Foo()
        assert(type(f)==type(g))

    def test_B14_super_import(self):
        self.assertRaises(TypeError, __import__)
        self.assertRaises(TypeError, __import__)
    
    def test_B15_super_import2(self):
        self.assertRaises(ValueError, __import__, "")
        self.assertRaises(ValueError, __import__, "")
    
    def test_B16_super_import2(self):
       self.assertRaises(ImportError, __import__, "xxx")
       self.assertRaises(ImportError, __import__, "xxx")
   
    def test_B17_super_import3(self):
        m = __import__("sys")
        m = __import__("sys")
    
    def test_B18_super_import4(self):
        m = __import__("BasicTestIDL")
        m = __import__("BasicTestIDL")
    
    def test_B19_import_doc(self):
        m = __import__.__doc__
        m = __import__.__doc__
    
bt = unittest.makeSuite(BasicTest,'test')

if __name__ == "__main__":
    runner = unittest.TextTestRunner()
    runner.run(bt)
