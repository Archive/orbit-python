#!/usr/bin/env python
import CORBA, os
import TestHarness

orb = CORBA.ORB_init([],CORBA.ORB_ID)
poa = orb.resolve_initial_references("RootPOA")
poa._get_the_POAManager().activate()

t = TestHarness.TestHarness(orb, poa)
f = open("ior","w")
f.write(orb.object_to_string(t._this()))
f.close()
f = open("pid","w")
f.write(str(os.getpid()))
f.close()
orb.run()
