#/usr/bin/env python
import sys, time, string, os, unittest, getopt, StressSuite
    
try:
    opts,args = getopt.getopt(sys.argv[1:],"hn:")
    for o,a in opts:
        if o == "-n":
            RUNS = int(a)
        elif o == "-h":
            print \
"""Usage: stresstest.sh [-nopp] [-gdb] [-n numruns] [T1 T2 TX-TY ... TN]"""
            sys.exit(2)

except getopt.error, e:
    print sys.argv[0]+": "+str(e)
    sys.exit(2)

tl=[]
if args:
    for f in args:
        if f.find("-") != -1:
            limits = f.split("-")
            try:
                for r in range(int(limits[0][1:]),int(limits[1][1:])+1):
                    tl.append(limits[0][0]+str(r))
            except ValueError:
                print sys.argv[0]+": Error: "+args[0]+ \
                    " is a stupid test range"
                sys.exit(2)
        else:
            tl.append(f)
    StressSuite.TESTS = tl
else: 
    StressSuite.TESTS = 0
StressSuite.DEBUG = 0

def uniq(seq):
    dict = {}
    for e in seq: dict[e] = 1
    return dict.keys()

# On-demand loading of suites so a partially broken O-P can still be
# used for stressing certain parts.

suites = []
sh = { 
    "A": [ "ClientServer", "AccessorTest" ],
    "V": [ "ClientServer", "ParameterReturnValueTest" ],
    "J": [ "ClientServer", "ObjectReferenceTest" ],
    "R": [ "ClientServer", "ReturnValueTest" ],
    "T": [ "ClientServer", "ParameterTest" ],
    "P": [ "ORBPOATest", "ORBPOATest" ],
    "O": [ "BasicORB", "BasicORB" ],
    "B": [ "BasicTest", "BasicTest" ]
}

if StressSuite.TESTS:
    # Get first char of each test
    for t in uniq(map(lambda x: x[0], StressSuite.TESTS)):
        m = __import__( sh[t][0] )
        c = getattr(m, sh[t][1] )
        suites.append(
            unittest.makeSuite( c, suiteClass = StressSuite.StressSuite )
        )
else:
    for s in sh.values():
        m = __import__( s[0] )
        c = getattr(m, s[1] )
        suites.append(     
            unittest.makeSuite( c, suiteClass = StressSuite.StressSuite ) )

suite = unittest.TestSuite ( suites )
runner = unittest.TextTestRunner( verbosity = 0)
runner.run(suite)
