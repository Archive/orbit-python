#!/bin/sh
PYTHON=python
export PYTHONPATH=../src/:$PYTHONPATH
export IDLPATH=.:$IDLPATH

DEBUG=

# simple hack to get it to run inside gdb
if [ x$1 == x-d ]; then
    shift

    gdb $PYTHON << __EOF__ &
r test-server.py 
where
__EOF__
SERVERPID=$!
sleep 1s

    gdb $PYTHON << __EOF__
r Test.py
where
__EOF__
else
    if [ x$1 == x-gdb ]; then
        shift
        $PYTHON test-server.py  &
        sleep 1s
        file=$RANDOM

        echo "run -c 'import os, signal, CORBA; os.kill ( os.getpid(), signal.SIGINT ); import Test' $*" >> /tmp/$file

        gdb $PYTHON -x /tmp/$file
        rm -f /tmp/$file
    else
        $PYTHON test-server.py &
        SERVERPID=$!
        sleep 1s
        $PYTHON Test.py $*
    fi
fi
rm ior
rm pid
kill $SERVERPID
