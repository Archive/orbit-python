#! /usr/bin/python
import CORBA
import CompleteTest, CompleteTest__POA
import RefTestIDL, RefTestIDL__POA
import RefTest2IDL, RefTest2IDL__POA

class Dummy(CompleteTest__POA.Dummy):
    string = "dummy"
    def get_string(self):
        return self.string

class Foo(RefTestIDL__POA.Foo):
    string = "foo"

class Foo2(RefTest2IDL__POA.Foo):
    string = "money"
    def get_money(self):
        return self.string

class TestHarness(CompleteTest__POA.TestHarness):
    def __init__ (self, orb, poa):
        self.attr = 0
        self.orb = orb
        self.poa = poa
        self.foo = "This is class TestHarness"
        self.objlist = []

    def get_string(self):
        return "hello"

    def get_long(self):
        return 627

    def get_null(self):
        return None

    def get_float(self):
        return 627.23

    def get_octet_int(self):
        return 0x68

    def get_octet_char(self):
        return "a"

    def get_dummy(self):
        obj = Dummy()
        self.objlist.append(obj)
        return obj._this()

    def get_object(self):
        obj = Dummy()
        self.objlist.append(obj)
        return obj._this()
    
    def get_any(self):
        typecode = CORBA.TypeCode("IDL:CORBA/String:1.0")
        return CORBA.Any(typecode, "Test String")

    def cleanup_obj(self):
         for o in self.objlist:
            self.poa.deactivate_object(o)
         del self.objlist
         self.objlist = []

    def get_noobject(self):
        return None

    def get_string_seq(self):
        return ["hello","goodbye","farewell"]

    def get_long_seq(self):
        return [1,3,5,7]

    def get_float_seq(self):
        return [1.11,3.11,5.33,7.55,9.77]

    def get_octet_string_seq(self):
        return "abcdef"
    
    def get_octet_int_seq(self):
        return map(ord,"abcdef")
    
    def get_octet_char_seq(self):
        r = ( "a","b","c","d","e","f" )
        return r
    
    def get_dummy_seq(self):
        l = []
        for f in range(0,10):
            o = Dummy()
            self.objlist.append(o)
            l.append(o._this())
        return l

    def get_object_seq(self):
        l = []
        for f in range(0,10):
            o = Dummy()
            self.objlist.append(o)
            l.append(o._this())
        return l

    def get_noobject_seq(self):
        l = []
        for f in range(0,10):
            l.append(None)
        return l
    
    def send_string(self, str):
        if str == "dummy":
            return 1
        else:
            return 0
        
    def send_long(self,p):
        if p == 666:
            return 1
        else:
            return 0

    def send_float(self,p):
        if round(p,2) == 6.66:
            return 1
        else:
            return 0

    def send_octet_int(self,p):
        if p == 0x68:
            return 1
        else:
            return 0

    def send_octet_char(self,p):
        # remember, octet_chars become ints
        if chr(p) == "a":
            return 1
        else:
            return 0

    def send_octet_string(self,p):
        if p == "abcdef":
            return 1
        else:
            return 0

    def send_octet_string_seq(self,p):
        if p == "abcdef":
            return 1
        else:
            return 0

    def send_object(self,p):
        if p.get_string() == "dummy":
            return 1
        else:
            return 0

    def send_noobject(self,p):
        if p == None:
            return 1
        else:
            return 0

    def send_dummy(self):
        if p.get_string() == "dummy":
            return 1
        else:
            return 0

    def send_string_seq(self, s):
        if s == ( "dummy", "ymmud", "muddy", "yddum"):
            return 1
        else:
            return 0
        
    def send_long_seq(self,p):
        if p == (666,111,222,777,888):
            return 1
        else:
            return 0

    def send_float_seq(self,p):
        if round(p[0],2) == 6.66 and \
            round(p[1],2) == 1.11 and \
            round(p[2],2) == 2.22 and \
            round(p[3],2) == 7.77 and \
            round(p[4],2) == 8.88:
            return 1
        else:
            return 0

    def send_octet_int_seq(self,p):
        if map(ord,p) == [ 0x31, 0x58, 0x68, 0x78, 0x78, 0x68 ]:
            return 1
        else:
            return 0

    def send_octet_char_seq(self,p):
        if p == "acbed":
            return 1
        else:
            return 0

    def send_octet_mixed_seq(self,p):
        if p == "ahbxd":
            return 1
        else:
            return 0

    def send_object_seq(self,ps):
        for p in ps:
            if p.get_string() != "dummy":
                return 0
        return 1

    def send_noobject_seq(self,ps):
        for p in ps:
            if p != None:
                return 0
        return 1

    def send_dummy_seq(self,ps):
        for p in ps:
            if p.get_string() != "dummy":
                return 0
        return 1

    def shutdown_orb(self):
        self.orb.shutdown(0)

    def get_reftest(self):
        f = Foo()
        self.objlist.append(f)
        return f._this()
    
    def get_reftest2(self):
        f = Foo2()
        self.objlist.append(f)
        return f._this()

    # OLD STUFF: FIXME INTEGRATE
    def _get_attr (self):
        return self.attr

    def _set_attr (self, a):
        self.attr = a

if __name__ == "__main__":
    orb = CORBA.ORB_init([], CORBA.ORB_ID)
    poa = orb.resolve_initial_references("RootPOA")
    poa._get_the_POAManager().activate()
    th = TestHarness(orb,poa)
    print "Instance: ", th
    th.shutdown_orb()
