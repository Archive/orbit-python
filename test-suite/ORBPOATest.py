import unittest
import CORBA
import BasicTestIDL, BasicTestIDL__POA,sys
import traceback

class Foo(BasicTestIDL__POA.Foo):
	pass

class Bar:
	pass

class ORBPOATest(unittest.TestCase):
	orb = CORBA.ORB_init([],CORBA.ORB_ID)
	poa = orb.resolve_initial_references("RootPOA")

	def test_P1_s2o_o2s(self):
		# stress gives me a MARSHAL error?
		f = Foo()
		ref = f._this()
		#assert(ref._is_equivalent(self.orb.string_to_object(self.orb.object_to_string(ref))))
		#assert(ref._is_equivalent(self.orb.string_to_object(self.orb.object_to_string(ref))))
		#self.orb.string_to_object(self.orb.object_to_string(ref))
		# This leaks inside ORBit - O-P bug #4, GNOME bug #55907
		self.orb.object_to_string(ref)
		self.poa.deactivate_object(f)

	def test_P2_bad_string_to_object(self):
		# Catch stupid input
		self.assertRaises(TypeError,self.orb.string_to_object,self.orb)
		# The following exposes a bug in ORBit.  No work-around available
		# in O-P: http://bugzilla.gnome.org/show_bug.cgi?id=55729
		#self.assertRaises(CORBA.MARSHAL,self.orb.string_to_object,"foo")

	def test_P3_bad_object_to_string(self):
		class Foo(BasicTestIDL__POA.Foo):
			pass
		f = Foo()
		self.assertRaises(CORBA.BAD_PARAM,self.orb.object_to_string,f)

	def test_P4_bad_resolve_refs(self):
		# Causes segfault in ORBit (gnome bugzilla #55726)
		# O-P implements a work-around
		self.assertRaises(CORBA.BAD_PARAM,
			self.orb.resolve_initial_references,"XXX")
	
	def test_P5_resolve_ns(self):
		# breaks on stress - but why?
		ns = self.orb.resolve_initial_references("NameService")
		# how do we import CosNaming and make this work?
	
	def test_P6_bad_deactivate(self):
		f = Foo()
		self.assertRaises(CORBA.BAD_INV_ORDER, self.poa.deactivate_object,f)

	def test_P7_bad_activate(self):
		self.assertRaises(CORBA.BAD_PARAM,self.poa.activate_object,self.orb)

	def test_P7_bad_activate(self):
		self.assertRaises(CORBA.BAD_PARAM,self.poa.activate_object,"crack!")

	def test_P8_activation(self):
		g = Foo()
		#print 'activate_object'
		#try:
		#    self.poa.activate_object(g)
		#except:
		#    traceback.print_exc ()
		#obj = g._this ()
		#g = self.poa.servant_to_reference (g)
		#print 'obj =', obj
		#print 'poa =', self.poa
		#print 'g =', g
		
		# check implicit reference creation
		assert(self.poa.servant_to_reference(g)._is_equivalent(g._this()))
		assert(g._this()._is_equivalent(self.poa.servant_to_reference(g)))
		self.poa.deactivate_object(g)

	def test_P9_activation_equalities(self):
		g = Foo()
		g._this()
		f = Foo()
		f._this()
		# check reference inequality
		assert(self.poa.servant_to_reference(g) != f._this())
		self.poa.deactivate_object(f)
		self.poa.deactivate_object(g)

	def test_P10_bad_serv_to_ref(self):
		g = Foo()
		self.assertRaises(CORBA.BAD_PARAM,
			self.poa.servant_to_reference,"foo")
		self.assertRaises(CORBA.BAD_PARAM,
			self.poa.servant_to_reference,self.orb)

	def test_P11_bad_ref_to_serv(self):
		g = Foo()
		# orb isn't a CORBA object ref
		self.assertRaises(CORBA.BAD_PARAM,
			self.poa.reference_to_servant,self.orb)
		# neither is g - it's a servant
		self.assertRaises(CORBA.BAD_PARAM,
			self.poa.reference_to_servant,g)
		# neither is crack - it's a drug
		self.assertRaises(CORBA.BAD_PARAM,
			self.poa.reference_to_servant,"crack!")
	
	def test_P12_dead_ref_to_serv(self):
		g = Foo()
		r = g._this()
		self.poa.deactivate_object(g)
		# FIXME: SHOULD THIS FAIL?
		self.poa.reference_to_servant(r)

	def test_P13_r2s_s2r(self):
		# check objref and back
		g = Foo()
		g._this()
		assert(g == \
			self.poa.reference_to_servant(self.poa.servant_to_reference(g)))
		self.poa.deactivate_object(g)

	def test_P14_activate_with_id(self):
		# FIXME: test proper function when it's there
		#self.poa.activate_object_with_id()
		pass

	def test_P15_deactivate(self): 
		f = Foo()
		f._this()
		self.poa.deactivate_object(f)
		self.poa.activate_object(f)
		self.poa.deactivate_object(f)

	def test_P16_multiple_implicit_activation(self):
		f = Foo()
		f._this()
		f._this()
		f._this()
		f._this()
        # though implicit activation should allow multiple invocations,
        # explicit activation through poa should not.
		self.assertRaises(CORBA.BAD_INV_ORDER,
			self.poa.activate_object,f)
		self.poa.deactivate_object(f)
	
	def test_P17_DoubleActivate(self):
		f = Foo()
		self.poa.activate_object(f)
		self.assertRaises(CORBA.BAD_INV_ORDER, self.poa.activate_object,f)
		self.poa.deactivate_object(f)
	
	def test_P18_DoubleDeactivate(self):
		f = Foo()
		f._this()
		self.poa.deactivate_object(f)
		self.assertRaises(CORBA.BAD_INV_ORDER,self.poa.deactivate_object,f)

	def test_P19_ActivatePOAManager(self):
		mgr = self.poa._get_the_POAManager()
		mgr.activate()

orbpoa = unittest.makeSuite(ORBPOATest,'test')

if __name__ == "__main__":
	runner = unittest.TextTestRunner()
	runner.run(orbpoa)

