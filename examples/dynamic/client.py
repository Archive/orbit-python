#!/usr/bin/env python

import urllib, os, sys

idlurl = "http://orbit-python.sault.org/demo/demo.idl"
iorurl = "http://orbit-python.sault.org/demo/demo.ior"

# Fetch the IDL from the server.  We need to do this before we import
# CORBA, because importing CORBA will implicitly preprocess this IDL.
idl = urllib.urlopen(idlurl).read()
open("demo.idl", "w").write(idl)

import CORBA
from _GlobalIDL import Demo

orb = CORBA.ORB_init()
# Fetch the IOR from the server
ior = urllib.urlopen(iorurl).readline()
o = orb.string_to_object(ior)
try:
	version = o.demo_version
	print "Server is version", version
except CORBA.COMM_FAILURE:
	print "The remote server appears to be down.  Try later?"
	sys.exit(1)

print "Message of the day:", o.motd()

print "The server provides these methods:"
# Filter out attribute accessors (prefixed with _)
methods = filter(lambda x: x[0] != '_', dir(o.__class__))
for method in methods:
	print "   %s: %s" % (method, o.doc(method))

print "\nNow invoking available methods:"
for method in methods:
	if method != "doc" and method != "motd":
		print "%s: %s" % (method, eval("o.%s()" % method))

os.unlink("demo.idl")
