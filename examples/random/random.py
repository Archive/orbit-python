#!/usr/bin/env python

import urllib
import CORBA
from _GlobalIDL import Random

orb = CORBA.ORB_init()
print "Fetching IOR from random.org, hang on ..."
ior = urllib.urlopen("http://www.random.org/Random.ior").readline()
o = orb.string_to_object(ior)

try:
	print "Here are 5 random numbers:"
	for i in range(5):
		print o.lrand48()
except CORBA.COMM_FAILURE:
	print "Couldn't connect to random.org CORBA server.  Perhaps it's down?"
