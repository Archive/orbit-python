#!/usr/bin/env python
import sys
import CORBA
import random

from Foo__POA import Echo

class Echo_Impl(Echo):
    def echoString(self, astring):
	outnum = random.randint(0, 100) / 100.0
	print "[server] %s -> %f" % (astring, outnum)
	return self._this(), outnum

orb = CORBA.ORB_init (sys.argv, CORBA.ORB_ID)
poa = orb.resolve_initial_references ('RootPOA')

servant = Echo_Impl()
obj = servant._this()

ior = orb.object_to_string (obj)
open("ior", "w").write(ior)

poamgr = poa._get_the_POAManager()
poamgr.activate()
orb.run()

