#!/usr/bin/env python

import sys, CORBA
from Foo import Echo

if len(sys.argv) < 2:
	print "Usage: %s [num iterations]" % sys.argv[0]
	sys.exit(1)

orb = CORBA.ORB_init()
ior = open("ior").readline()
client = orb.string_to_object(ior)
print client
for i in range( int(sys.argv[1]) ):
        client, f = client.echoString("Hello, world [%d]" % i)
	print "[client]", f
