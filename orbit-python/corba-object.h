/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Johan Dahlin
 *
 */

#ifndef __CORBA_OBJECT_H__
#define __CORBA_OBJECT_H__

#include <Python.h>
#include <orbit/orbit-types.h>

extern GHashTable *object_instance_glue;
extern GHashTable *object_glue;
extern GHashTable *object_to_instances_hash;
extern PyObject   *corba_object_class;

PyObject * CORBA_Object_to_PyObject      (CORBA_Object);

PyObject * CORBA_PyClass__init           (PyObject *_,
					  PyObject *args,
					  PyObject *k);

PyObject * CORBA_PyClass__del            (PyObject *_,
					  PyObject *args,
					  PyObject *k);

PyObject * CORBA_PyClass___invoke        (PyObject *_,
					  PyObject *args,
					  PyObject *k);

PyObject * CORBA_PyClass__setattr__      (PyObject *_,
					  PyObject *args,
					  PyObject *k);

PyObject * CORBA_PyClass__getattr__      (PyObject *_,
					  PyObject *args,
					  PyObject *k);

PyObject * CORBA_PyClass___narrow        (PyObject *_,
					  PyObject *args,
					  PyObject *k);

PyObject * CORBA_PyClass___is_a          (PyObject *_,
					  PyObject *args,
					  PyObject *k);

PyObject * CORBA_PyClass___is_equivalent (PyObject *_,
					  PyObject *args,
					  PyObject *k);

PyObject * CORBA_PyClass___hash          (PyObject *_,
					  PyObject *args,
					  PyObject *k);

PyObject * CORBA_PyClass___non_existent  (PyObject *_,
					  PyObject *args,
					  PyObject *k);

#endif /* __CORBA_OBJECT_H__ */



