/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000,2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2001,2002 Johan Dahlin <jdahlin@telia.com> 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include "corba-object.h"

#include "client.h"
#include "corba-orb.h"
#include "exceptions.h"
#include "idl.h"
#include "orbit-python-types.h"
#include "server.h"
#include "util.h"

#include <sys/stat.h>

/* Private functions */
static PyObject * CORBA_Object_to_PyObject_with_type (CORBA_Object        object,
						      CORBA_PyClass_Glue *class_glue,
						      CORBA_boolean       release_obj);

/* maps instances to instance_glue */
GHashTable *object_instance_glue;

/* maps object ids to classes */
GHashTable *object_glue;

/* maps instances to instance_glue hashes. The instance glue hash stores
 * instance_glue keyed by class. This exists to implement the following
 * behaviour:
 * f._this() == f._this(), however given ref1=f._this();
 * ref2=ref1._narrow(SomethingElse) then ref1 != ref2.
 */
GHashTable *object_to_instances_hash;

PyObject *corba_object_class; /* CORBA.Object base class */

/*************************************
 *
 *  TAKE 2
 */

/* release_obj: boolean flag indicating if we are to release the
 *              original CORBA Object ( used by _narrow() )
 */
static PyObject *
CORBA_Object_to_PyObject_with_type (CORBA_Object        object,
				    CORBA_PyClass_Glue *class_glue,
				    CORBA_boolean       release_obj)
{
        CORBA_PyInstance_Glue *inst_glue;
	PyObject              *inst;
	PyObject              *class_obj;
	char                  *repo_id;
	CORBA_ORB_PyObject    *orb;
	
	/* this is a subhash that the object_to_instances_hash stores */
	GHashTable            *repo_id_to_instance;

	orb = g_hash_table_lookup (orb_objects, object->orb);
	if (!orb) {
		orb = CORBA_ORB_PyObject__new (object->orb);
		d_assert (orb != NULL);
	}

	/* prepare to create new reference */
	if (class_glue) {
		class_obj = class_glue->class_obj;
		repo_id = class_glue->repo_id;
	} else { 
		class_obj = corba_object_class;
		repo_id = "IDL:CORBA/Object:1.0";
	}

	/* return existing instance only if we actually have a reference for
	 * that specific interface and corba object
	 */
	repo_id_to_instance = g_hash_table_lookup (object_to_instances_hash, 
						   object);
	if (repo_id_to_instance) {
		inst = g_hash_table_lookup (repo_id_to_instance, repo_id);
		if (inst) {
			/* we already had an instance for that reference */

			/* when _narrow()ing, we don't want to release the Object,
			 * just return a new reference to it.
			 */
			if (release_obj) {
				inst_glue = g_hash_table_lookup (object_instance_glue, inst);
				d_assert (inst_glue);
				
				CORBA_Object_release (object, &inst_glue->ev);
			}
			
			d_message (1, "Found existing reference at %x for %s", 
				   (guint)inst, repo_id);
			
			Py_INCREF(inst);
			return inst;
		}
	}

	inst_glue = g_new0 (CORBA_PyInstance_Glue, 1);
	inst_glue->obj        = object;
	inst_glue->class_glue = class_glue;
	inst_glue->repo_id    = repo_id;
	inst_glue->orb        = orb;
	Py_INCREF(orb);
	
	inst = PyInstance_New (class_obj, NULL, NULL);
	CORBA_exception_init (&inst_glue->ev);

	g_hash_table_insert (object_instance_glue, inst, inst_glue);
	d_message (1, "Created instance %x for %s", (guint)inst, repo_id);

	/* take care of that pesky subhash -- create it if necessary */
	if (!repo_id_to_instance) {
		d_message (1, "Creating new repo_id hash for %x", (guint)inst);
		
		repo_id_to_instance = g_hash_table_new (g_str_hash, g_str_equal);
		g_hash_table_insert (object_to_instances_hash, object, 
				     repo_id_to_instance);
	}
	
	g_hash_table_insert (repo_id_to_instance,
			     repo_id,
			     inst);
	return inst;
}

PyObject *
CORBA_Object_to_PyObject (CORBA_Object object)
{
	CORBA_PyClass_Glue *class_glue;
	const gchar        *type;
	
	/* Nil CORBA objects map onto Py_None */
	if (object == CORBA_OBJECT_NIL) {
		Py_INCREF (Py_None);
		return Py_None;
	}
	
	type = g_quark_to_string (object->type_qid);
	class_glue = g_hash_table_lookup (object_glue, type);
	
	return CORBA_Object_to_PyObject_with_type (object,
						   class_glue, 
						   CORBA_TRUE);
}

PyObject *
CORBA_PyClass__init (PyObject *_,
		     PyObject *args,
		     PyObject *keys)
{
/* d_message(2, "CORBA_PyClass__init: entered"); */
	Py_INCREF(Py_None);
	return Py_None;
}

PyObject *
CORBA_PyClass__del (PyObject *_,
		    PyObject *args,
		    PyObject *keys)
{
	CORBA_PyInstance_Glue *inst_glue;
	PyObject *self, *inst;
	GHashTable *repo_id_to_instance;

	d_message (1, "CORBA_PyClass__del: entered");
	if (!PyArg_ParseTuple (args, "O:", &self)) {
		return NULL;
	}

	inst_glue = g_hash_table_lookup (object_instance_glue, self);
	
	Py_INCREF (Py_None);
	if (!inst_glue) {
		return Py_None;
	}

	/* Remove stuff from the hash tables */
	repo_id_to_instance = g_hash_table_lookup (object_to_instances_hash,
						   inst_glue->obj);
	d_assert (repo_id_to_instance != NULL);

	/* redundant check - there has to be a hash for the instance */
	d_message (1,"looking up %s in repo_id hash", inst_glue->repo_id);
	inst = g_hash_table_lookup (repo_id_to_instance, inst_glue->repo_id);
	d_assert (inst != NULL);
	d_assert (inst == self);

	g_hash_table_remove (repo_id_to_instance, inst_glue->repo_id);
	
	/* only kill hash table if it is empty */
	if (g_hash_table_size (repo_id_to_instance) == 0) {
		g_hash_table_remove (object_to_instances_hash, inst_glue->obj);
		g_hash_table_destroy (repo_id_to_instance);
		d_message (1, "Freeing CORBA object for this object");
		CORBA_Object_release (inst_glue->obj, &inst_glue->ev);
		CORBA_exception_free (&inst_glue->ev);
	}

	g_hash_table_remove (object_instance_glue, self);
	Py_DECREF (inst_glue->orb);
	g_free (inst_glue);

	d_message (1, "Freed instance %x and glue %x",
		   (guint)self, (guint)inst_glue);
	return Py_None;
}

PyObject *
CORBA_PyClass___invoke (PyObject *_,
			PyObject *args,
			PyObject *keys)
{
	PyObject *self, *o_args, *o_keys;
	char *opname;
	CORBA_PyInstance_Glue *glue;
	CORBA_OperationDescription *opd;

	if (!PyArg_ParseTuple(args, "OsO!O!", &self, &opname, &PyTuple_Type,
	                      &o_args, &PyDict_Type, &o_keys)) 
		return NULL;

	glue = g_hash_table_lookup(object_instance_glue, self);
	d_assert(glue != NULL);
	
	opd = find_operation(glue->class_glue, opname);
	return _stub_func(glue->obj, o_args, opd);
}

PyObject *
CORBA_PyClass__getattr__ (PyObject *_,
			  PyObject *args,
			  PyObject *k)
{
	PyObject *self;
	char *name;
	CORBA_AttributeDescription *ad;
	CORBA_PyInstance_Glue *glue;

	if (!PyArg_ParseTuple(args, "Os", &self, &name)) 
		return NULL;

	glue = g_hash_table_lookup(object_instance_glue, self);
	d_assert(glue != NULL);

	if (glue->class_glue) {
		ad = find_attribute(glue->class_glue, name);
		if (ad)
			return get_attribute(glue, ad);
	}

	PyErr_Format(PyExc_AttributeError, "%s", name);
	
	return 0;
}

PyObject *CORBA_PyClass__setattr__(PyObject *_, PyObject *args, PyObject *k)
{
	PyObject *self, *value;
	char *name;
	CORBA_PyInstance_Glue *glue;

	if (!PyArg_ParseTuple(args, "OsO", &self, &name, &value)) 
		return NULL;

	glue = g_hash_table_lookup(object_instance_glue, self);
	d_assert(glue != NULL);

	if (glue->class_glue) {
		CORBA_AttributeDescription *ad = find_attribute(glue->class_glue, name);
		if (ad)
			return set_attribute(glue, ad, value);
	}

	/* Return with an error this attribute isn't defined
	 * XXX: maybe we should let users define local attributes?
	 */
	raise_system_exception(OPExc_INTERNAL, 0, CORBA_COMPLETED_NO,
	                       "attribute %s unknown", name);
	return 0;
}

PyObject *CORBA_PyClass___narrow(PyObject *_, PyObject *args, PyObject *k)
{
	PyObject *self, *target_class;
	CORBA_PyInstance_Glue *glue;
	CORBA_PyClass_Glue *target_class_glue;
	char *repo_id;

	if (!PyArg_ParseTuple(args, "OO!", &self, &PyClass_Type, &target_class))
		return NULL;
	glue = g_hash_table_lookup(object_instance_glue, self);
	if (!glue)
		return raise_system_exception(OPExc_INTERNAL, 0, CORBA_COMPLETED_NO,
		             "Self is not a valid CORBA Object reference");

	repo_id = g_hash_table_lookup(stub_repo_ids, target_class);
	target_class_glue = g_hash_table_lookup(object_glue, repo_id);

	return CORBA_Object_to_PyObject_with_type(glue->obj,
	                                          target_class_glue, 
	                                          CORBA_FALSE);
}

PyObject *
CORBA_PyClass___is_a (PyObject *_, PyObject *args, PyObject *k)
{
	PyObject *self;
	CORBA_PyInstance_Glue *glue;
	char *repo_id;
	CORBA_boolean result;

	if (!PyArg_ParseTuple(args, "Os", &self, &repo_id)) {
		return NULL;
	}
	
	glue = g_hash_table_lookup(object_instance_glue, self);
	if (!glue) {
		return raise_system_exception(OPExc_INTERNAL, 0, CORBA_COMPLETED_NO,
		             "Self is not a valid CORBA Object reference");
	}
	
	result = CORBA_Object_is_a(glue->obj, repo_id, &glue->ev);
	if (!check_corba_ex(&glue->ev)) {
		return NULL;
	}

	return Py_BuildValue("h", result);
}

PyObject *
CORBA_PyClass___is_equivalent (PyObject *_,
			       PyObject *args, 
			       PyObject *k)
{
	PyObject *self, *other;
	CORBA_PyInstance_Glue *glue, *other_glue;
	CORBA_boolean result;
	if (!PyArg_ParseTuple(args, "OO", &self, &other)) {
		return NULL;
	}
	

	glue = g_hash_table_lookup(object_instance_glue, self);
	if (!glue) {
		return raise_system_exception(OPExc_INTERNAL, 0, CORBA_COMPLETED_NO,
		             "Self is not a valid CORBA Object reference");
	}
	
	other_glue = g_hash_table_lookup(object_instance_glue, other);
	if (!other_glue) {
		return raise_system_exception(OPExc_BAD_PARAM, 0, CORBA_COMPLETED_NO,
		             "Parameter 1 is not a valid CORBA Object reference");
	}
	
	result = CORBA_Object_is_equivalent(glue->obj, other_glue->obj, &glue->ev);
	if (!check_corba_ex(&glue->ev)) {
		return NULL;
	}

	return Py_BuildValue("h", result);
}


PyObject *CORBA_PyClass___hash(PyObject *_, PyObject *args, PyObject *k)
{
	PyObject *self;
	CORBA_PyInstance_Glue *glue;
	CORBA_unsigned_long result, max;

	if (!PyArg_ParseTuple(args, "Ol", &self, &max)) {
		return NULL;
	}
	
	glue = g_hash_table_lookup(object_instance_glue, self);
	if (!glue) {
		return raise_system_exception(OPExc_INTERNAL, 0, CORBA_COMPLETED_NO,
		             "Self is not a valid CORBA Object reference");
	}
	
	result = CORBA_Object_hash(glue->obj, max, &glue->ev);
	if (!check_corba_ex(&glue->ev)) {
		return NULL;
	}

	return Py_BuildValue("l", result);
}

PyObject *CORBA_PyClass___non_existent(PyObject *_, PyObject *args, PyObject *k)
{
	PyObject *self;
	CORBA_PyInstance_Glue *glue;
	CORBA_boolean result;

	if (!PyArg_ParseTuple(args, "O", &self)) {
		return NULL;
	}
	
	glue = g_hash_table_lookup(object_instance_glue, self);
	if (!glue) {
		return raise_system_exception(OPExc_INTERNAL, 0, CORBA_COMPLETED_NO,
		             "Self is not a valid CORBA Object reference");
	}
	
	result = CORBA_Object_non_existent(glue->obj, &glue->ev);
	if (!check_corba_ex(&glue->ev)) {
		return NULL;
	}

	return Py_BuildValue("h", result);
}

/* Local Variables: */
/* c-file-style: "python" */
/* vim: set noet ai fo=tcq sts=5 ts=5 sw=5 wrapmargin=8: */
/* End: */
