/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Johan Dahlin
 *
 */

#ifndef __UTILS_H__
#define __UTILS_H__

#include "corba-object.h"
#include "exceptions.h"

/* Define this if you want global debug */
/* #define DEBUG */

#ifdef DEBUG
#define __DEBUG_LEVEL__ 5
#else
#define __DEBUG_LEVEL__ 0
#endif

#define is_corba_pyobject(obj) \
	(g_hash_table_lookup(object_instance_glue, obj) != 0)
//	(obj->ob_type->tp_dealloc == (destructor)CORBA_PyObject__dealloc)

#define dupe_tc(a) (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)a, NULL)
#define d_error(f, a...) g_log(G_LOG_DOMAIN, G_LOG_LEVEL_ERROR, f, ##a)

#define d_message(l, f, a...) G_STMT_START { \
 if (__DEBUG_LEVEL__ >= l) \
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, f, ##a); \
	}G_STMT_END

#define d_warning(l, f, a...) G_STMT_START { \
 if (__DEBUG_LEVEL__ >= l) \
		g_log(G_LOG_DOMAIN, G_LOG_LEVEL_WARNING, f, ##a); \
	}G_STMT_END

#ifndef DEBUG
	#define d_assert_noval(expr) G_STMT_START { \
		if (!(expr)) { \
			raise_system_exception(OPExc_INTERNAL, 0, CORBA_COMPLETED_MAYBE, \
			"file %s: line %d (%s): assertion failed: (%s)", \
			__FILE__, __LINE__, __PRETTY_FUNCTION__, #expr); \
	      return; \
		}  } G_STMT_END

	#define d_assert_val(expr, val) G_STMT_START { \
		if (!(expr)) { \
			raise_system_exception(OPExc_INTERNAL, 0, CORBA_COMPLETED_MAYBE, \
			"file %s: line %d (%s): assertion failed: (%s)", \
			__FILE__, __LINE__, __PRETTY_FUNCTION__, #expr); \
	      return val; \
		}  } G_STMT_END

	#define d_assert(expr) d_assert_val(expr, NULL);
#else
	#define d_assert(expr) g_assert(expr)
	#define d_assert_val(expr, val) g_assert(expr)
	#define d_assert_noval(expr) g_assert(expr)
#endif /* DEBUG */

#endif /* __UTILS_H__ */
