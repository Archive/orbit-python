/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */
#include "marshal.h"

#include "corba-object.h"
#include "exceptions.h"
#include "orbit-python-types.h"
#include "types.h"
#include "util.h"

#define buf_putn giop_send_buffer_append

// Make a TypeCode for system exceptions, borrowed from CORBA::ORBit
static const char *Status_SubNames[] = {
	"COMPLETED_YES", "COMPLETED_NO", "COMPLETED_MAYBE"
};

/* This is the old, struct. But i'll keep it here until i know it works */
#if 0 
static struct CORBA_TypeCode_struct Status_TypeCode = {
	{},                      /*. struct ORBit_RootObject_struct parent */          
	CORBA_tk_enum,           /*. CORBA_unsigned_long kind */
	NULL,                    /*. char *name */
	NULL,                    /*. char *repo_id */
	0,                       /*. CORBA_unsigned_long length */
	3,                       /*. CORBA_unsigned_long sub_parts */
	(char**)Status_SubNames, /*. char **subnames */
	NULL,                    /*. CORBA_TypeCode *subtypes */  
        NULL,                    /* CORBA_any *sublabels */        
        CORBA_OBJECT_NIL,        /* CORBA_TypeCode discriminator */
	-1,                      /*. CORBA_unsigned_long recurse_depth */ 
	0,                       /*. CORBA_long default_index */ 
	0,                       /*. CORBA_unsigned_short digits */ 
	0,                       /*. CORBA_short scale */
	0,                       /*. CORBA_short c_align */
};
#endif

static struct CORBA_TypeCode_struct Status_TypeCode = {
        {},                      /* struct ORBit_RootObject_struct parent */
        CORBA_tk_enum,           /* CORBA_unsigned_long  kind */          
	0,                       /* CORBA_unsigned_long  flags */         
        0,                       /* CORBA_short          c_length */      
        0,                       /* CORBA_short          c_align */       
        0,                       /* CORBA_unsigned_long  length */        
        3,                       /* CORBA_unsigned_long  sub_parts */     
        NULL,                    /* CORBA_TypeCode      *subtypes */
	CORBA_OBJECT_NIL,        /* CORBA_TypeCode       discriminator */
        NULL,                    /* char                *name */
        NULL,                    /* char                *repo_id */
        (char**)Status_SubNames, /* char               **subnames */      
        0,                       /* CORBA_long           default_index */ 
        -1,                      /* CORBA_unsigned_long  recurse_depth */ 
        0,                       /* CORBA_unsigned_short digits */        
        0,                       /* CORBA_short scale; */
};

static struct CORBA_TypeCode_struct Minor_TypeCode = {
	{}, CORBA_tk_ulong, 0, 0, 0, 0, 0
};

static const char *SystemExcept_SubNames[] = { "-minor", "-status" };
static CORBA_TypeCode SystemExcept_SubTypes[] = {
	&Minor_TypeCode, &Status_TypeCode
};

#if 0
static struct CORBA_TypeCode_struct SystemExcept_TypeCode = {
	{}, CORBA_tk_except, NULL, NULL, 0, 2, (char**)SystemExcept_SubNames,
	SystemExcept_SubTypes
};
#endif

static struct CORBA_TypeCode_struct SystemExcept_TypeCode = {
	{},
	CORBA_tk_except,
	0,
	0,
	0,
	0,
	2,
	SystemExcept_SubTypes,
	CORBA_OBJECT_NIL,
	NULL,
	NULL,
	(char**)SystemExcept_SubNames,
};

static CORBA_boolean marshal_short    (PyObject *arg, GIOPSendBuffer *buf);
static CORBA_boolean marshal_boolean  (PyObject *arg, GIOPSendBuffer *buf);
static CORBA_boolean marshal_string   (PyObject *arg, GIOPSendBuffer *buf);
static CORBA_boolean marshal_wstring  (PyObject *arg, GIOPSendBuffer *buf);

static CORBA_boolean marshal_object   (PyObject *arg, GIOPSendBuffer *buf);
static CORBA_boolean marshal_struct   (PyObject *arg, GIOPSendBuffer *buf,
				       CORBA_TypeCode tc);
static CORBA_boolean marshal_array    (PyObject *arg, GIOPSendBuffer *buf,
				       CORBA_TypeCode tc);
static CORBA_boolean marshal_sequence (PyObject *arg, GIOPSendBuffer *buf,
				       CORBA_TypeCode tc);
static CORBA_boolean marshal_enum     (PyObject *arg, GIOPSendBuffer *buf,
				       CORBA_TypeCode tc);
static CORBA_boolean marshal_union    (PyObject *arg, GIOPSendBuffer *buf,
				       CORBA_TypeCode tc);
static CORBA_boolean marshal_long     (PyObject *arg, GIOPSendBuffer *buf);
static CORBA_boolean marshal_char     (PyObject *arg, GIOPSendBuffer *buf);
static CORBA_boolean marshal_octet    (PyObject *arg, GIOPSendBuffer *buf);
static CORBA_boolean marshal_float    (PyObject *arg, GIOPSendBuffer *buf);
static CORBA_boolean marshal_double   (PyObject *arg, GIOPSendBuffer *buf);
static CORBA_boolean marshal_longlong (PyObject *arg, GIOPSendBuffer *buf);
static CORBA_boolean marshal_typecode (PyObject *arg, GIOPSendBuffer *buf);
static CORBA_boolean marshal_any      (PyObject *arg, GIOPSendBuffer *buf);

void
ORBit_Python_init_marshal (void)
{
	SystemExcept_SubTypes[0] = (CORBA_TypeCode)TC_CORBA_unsigned_long;
}

CORBA_boolean
marshal_arg (PyObject *arg,
	     GIOPSendBuffer *buf,
	     CORBA_TypeCode tc)
{
	void *glue;
	if (!arg) {
		d_warning(0, "marshal_arg: (arg == NULL)");
		return CORBA_FALSE;
	}
	d_message(2, "marshal_arg: tc->kind = %d, tc->name = %s, pyarg = %s",
	          tc->kind, tc->name, arg->ob_type->tp_name);
	switch (tc->kind) {
		case CORBA_tk_null:
		case CORBA_tk_void:
			return CORBA_TRUE;
		
		case CORBA_tk_string:
			return marshal_string(arg, buf);
#if PY_MAJOR_VERSION >= 2
		case CORBA_tk_wstring:
			return marshal_wstring(arg, buf);
#endif /* PY_MAJOR_VERSION >= 2 */
		case CORBA_tk_short:
		case CORBA_tk_ushort:
			return marshal_short(arg, buf);
		case CORBA_tk_boolean:
			return marshal_boolean(arg, buf);
		case CORBA_tk_long:
		case CORBA_tk_ulong:
			return marshal_long(arg, buf);
		case CORBA_tk_char:
			return marshal_char(arg, buf);
		case CORBA_tk_octet:
			return marshal_octet(arg, buf);
		case CORBA_tk_float:
			return marshal_float(arg, buf);
		case CORBA_tk_double:
			return marshal_double(arg, buf);
		case CORBA_tk_longlong:
		case CORBA_tk_ulonglong:
			return marshal_longlong(arg, buf);

		case CORBA_tk_struct:
			return marshal_struct(arg, buf, tc);
		case CORBA_tk_array:
			return marshal_array(arg, buf, tc);
		case CORBA_tk_sequence:
			return marshal_sequence(arg, buf, tc);
		case CORBA_tk_enum:
			return marshal_enum(arg, buf, tc);
		case CORBA_tk_union:
			return marshal_union(arg, buf, tc);
		case CORBA_tk_alias:
			return marshal_arg(arg, buf, tc->subtypes[0]);
		case CORBA_tk_any:
			return marshal_any(arg, buf);
		case CORBA_tk_TypeCode:
			return marshal_typecode(arg, buf);

#if PY_MAJOR_VERSION < 2
                case CORBA_tk_wstring:
#endif /* PY_MAJOR_VERSION < 2 */
		case CORBA_tk_wchar:
		case CORBA_tk_Principal:
			d_warning(0, "Can't marshal unsupported typecode: %s", 
			          (char *)tc->kind);
			return CORBA_FALSE;
#ifndef OLD
	        case CORBA_tk_objref:
			glue = g_hash_table_lookup (object_glue, tc->repo_id);
			if (glue || !strcmp (tc->repo_id, "IDL:CORBA/Object:1.0")) {
			        return marshal_object (arg, buf);
			} else {
				PyErr_Format(PyExc_TypeError, "Failed to marshal: %s (%s)",
					     tc->name, tc->repo_id);
			}
#endif		
		default:
			break;

	}
#ifdef OLD	
	// Is this an object reference we know about?
	glue = g_hash_table_lookup(object_glue, tc->repo_id);
	if (glue || !strcmp(tc->repo_id, ""))
		return marshal_object(arg, buf);
	else
		PyErr_Format(PyExc_TypeError, "Failed to marshal: %s (%s)",
		                tc->name, tc->repo_id);
#endif	
	return CORBA_FALSE;
}

static CORBA_boolean
marshal_short (PyObject *arg, GIOPSendBuffer *buf)
{
	CORBA_short v;
	if (!PyInt_Check(arg)) {
		raise_system_exception(OPExc_MARSHAL, 0,
		                       CORBA_COMPLETED_MAYBE, 
		                       "Expected integer, got %s", 
		                       arg->ob_type->tp_name);
		return CORBA_FALSE;
	}
	v = (CORBA_short)PyInt_AsLong(arg);
	buf_putn(buf, &v, sizeof(v));
	return CORBA_TRUE;
}

static CORBA_boolean marshal_boolean(PyObject *arg, GIOPSendBuffer *buf)
{
	CORBA_boolean v;
	if (!PyInt_Check(arg)) {
		raise_system_exception(OPExc_MARSHAL, 0,
		                       CORBA_COMPLETED_MAYBE, 
		                       "Expected integer, got %s", 
		                       arg->ob_type->tp_name);
		return CORBA_FALSE;
	}
	if (!PyArg_Parse(arg, "h", &v))
		return CORBA_FALSE;
	buf_putn(buf, &v, sizeof(v));
	return CORBA_TRUE;
}

// FIXME: check typecode for length
static CORBA_boolean marshal_string(PyObject *arg, GIOPSendBuffer *buf)
{
	const char *s;
	CORBA_unsigned_long len;

	if (!PyString_Check(arg)) {
		raise_system_exception(OPExc_MARSHAL, 0,
		                       CORBA_COMPLETED_MAYBE, 
		                       "Expected string, got %s", 
		                       arg->ob_type->tp_name);
		return CORBA_FALSE;
	}
	if (!PyArg_Parse(arg, "s", &s))
		return CORBA_FALSE;
	d_message(3, "marshal_string: contents = %s", s);

	len = strlen(s) + 1;
	buf_putn(buf, &len, sizeof(len));
	giop_send_buffer_append(buf, s, len);

	return CORBA_TRUE;
}

#if PY_MAJOR_VERSION >= 2
// FIXME: check typecode for length
// FIXME: check interoperability with other ORBs!
#warning Support for wide strings is highly experimental!
static CORBA_boolean marshal_wstring(PyObject *arg, GIOPSendBuffer *buf)
{
	Py_UNICODE *s;
	CORBA_unsigned_long len;
	
	if (!PyUnicode_Check(arg)) {
		raise_system_exception(OPExc_MARSHAL, 0,
		                       CORBA_COMPLETED_MAYBE, 
		                       "Expected Unicode, got %s", 
		                       arg->ob_type->tp_name);
		return CORBA_FALSE;
	}
	if (!PyArg_Parse(arg, "u#", &s, &len))
		return CORBA_FALSE;
	
	len = len + 1;
	buf_putn(buf, &len, sizeof(len));
	giop_send_buffer_append (buf, s, len * sizeof(Py_UNICODE));
	return CORBA_TRUE;
}
#endif /* PY_MAJOR_VERSION >= 2 */

CORBA_exception_type marshal_exception(PyObject *type, PyObject *data,
                                       GIOPSendBuffer *buf, CORBA_TypeCode tc,
                                       CORBA_OperationDescription *opd)
{
	PyObject *repo_object, *base, *bases;
	char *repo_id;
	CORBA_unsigned_long i, len;
	CORBA_boolean do_free = CORBA_FALSE;
	CORBA_exception_type ret = CORBA_NO_EXCEPTION;

	g_return_val_if_fail (data != 0 && type != 0 && buf != 0,
	                      CORBA_NO_EXCEPTION);

	repo_object = PyObject_GetAttrString(type, "__repo_id");
	if (!repo_object) {
		// don't know this exception (Python exception?), so
		// marshal CORBA.UNKNOWN
		PyObject *args;
		PyErr_Print();
		PyErr_Clear();
		type = OPExc_UNKNOWN;
		args = PyTuple_New(2);

		PyTuple_SetItem(args, 0, PyLong_FromLong(0));
		PyTuple_SetItem(args, 1, PyLong_FromLong(CORBA_COMPLETED_MAYBE));
		data = PyInstance_New(type, args, NULL);
		Py_DECREF(args);
	
		repo_object = PyObject_GetAttrString(type, "__repo_id");
		do_free = CORBA_TRUE;
	}
	PyArg_Parse(repo_object, "s", &repo_id);
	Py_DECREF(repo_object);
	bases = PyObject_GetAttrString(type,"__bases__");
	base = PyTuple_GetItem(bases, 0);

	if (base == OPExc_UserException) {

		if (!tc && opd)
			for (i = 0; i < opd->exceptions._length; i++) {
				if (!strcmp(opd->exceptions._buffer[i].id, repo_id)) {
					tc = opd->exceptions._buffer[i].type;
					break;
				}
			}

		if (!tc) {
			d_warning(0, "Unknown exception received");
			raise_system_exception(OPExc_UNKNOWN, 0, CORBA_COMPLETED_MAYBE, NULL);
			return CORBA_NO_EXCEPTION;
		}

		len = strlen(repo_id) + 1;
		buf_putn(buf, &len, sizeof(len));
		giop_send_buffer_append (buf, repo_id, len);
	
		if (tc->sub_parts) {

			if (!PyInstance_Check(data)) {
				d_warning(0, "Exception data (%s) must be an instance",
				data->ob_type->tp_name);
				goto bail;
			}
			for (i = 0; i < tc->sub_parts; i++) {
				PyObject *val;
				CORBA_boolean result;

				val = PyObject_GetAttrString(data, (char *)tc->subnames[i]);
				if (!val) { // FIXME: throw an exception
					d_warning(0, "Missing exception member %s", tc->subnames[i]);
					goto bail;
				}
				result = marshal_arg(val, buf, tc->subtypes[i]);
				Py_DECREF(val);
				if (!result)
					goto bail;
			}
		}
		ret = CORBA_USER_EXCEPTION;
		goto bail;
	}
	else if (base == OPExc_SystemException || type == OPExc_SystemException) {
		CORBA_long minor, completed;
		PyObject *minor_obj, *completed_obj;

		if (!PyInstance_Check(data)) {
			d_warning(0, "Exception data (%s) must be an instance",
			data->ob_type->tp_name);
			goto bail;
		}
		tc = &SystemExcept_TypeCode;
		len = strlen(repo_id) + 1;
		buf_putn(buf, &len, sizeof(len));
		giop_send_buffer_append (buf, repo_id, len);

		minor_obj = PyObject_GetAttrString(data, "minor");
		completed_obj = PyObject_GetAttrString(data, "completed");
		PyArg_Parse(minor_obj, "l", &minor);
		PyArg_Parse(completed_obj, "l", &completed);
		Py_DECREF(minor_obj);
		Py_DECREF(completed_obj);

		buf_putn(buf, &minor, sizeof(minor));
		buf_putn(buf, &completed, sizeof(completed));
		ret = CORBA_SYSTEM_EXCEPTION;
	}
bail:
	if (do_free) {
		Py_DECREF(data);
	}
	Py_DECREF(bases);
	return ret;
}

static CORBA_boolean marshal_object(PyObject *arg, GIOPSendBuffer *buf)
{
	CORBA_Object obj = CORBA_OBJECT_NIL;
	CORBA_PyInstance_Glue *glue;
	d_message(3, "marshal_object: entered, arg = %x", (guint)arg);
	if (arg != Py_None) {
		glue = g_hash_table_lookup(object_instance_glue, arg);
		if (!glue) {
			raise_system_exception(OPExc_MARSHAL, 0, 
			                       CORBA_COMPLETED_NO, 
			                       "Cannot marshal unknown object");
			return CORBA_FALSE;
		}
		obj = glue->obj;
	}
		
	ORBit_marshal_object(buf, obj);
	d_message(3, "marshal_object: exited");
	return CORBA_TRUE;
}

static CORBA_boolean marshal_struct(PyObject *arg, GIOPSendBuffer *buf,
                             CORBA_TypeCode tc)
{
	CORBA_unsigned_long i;
	if (!PyInstance_Check(arg)) {
		raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_NO, 
		                       "expected instance");
		return CORBA_FALSE;
	}
	d_message(3, "marshal_struct: %d sub parts", tc->sub_parts);
	for (i = 0; i < tc->sub_parts; i++) {
		CORBA_boolean result;
		PyObject *val = PyObject_GetAttrString(arg, (char *)tc->subnames[i]);
		// FIXME: throw an exception here
		if (!val) {
			d_warning(0, "Missing struct member %s", tc->subnames[i]);
			return CORBA_FALSE;
		}
		result = marshal_arg(val, buf, tc->subtypes[i]);
		Py_DECREF(val);
		if (!result)
			return CORBA_FALSE;
	}
	return CORBA_TRUE;
}

static CORBA_boolean marshal_array(PyObject *arg, GIOPSendBuffer *buf,
                            CORBA_TypeCode tc)
{
	PyObject *tuple;
	CORBA_unsigned_long i;
	if (PyList_Check(arg))
		tuple = PyList_AsTuple(arg);
	else if (PyTuple_Check(arg))
		tuple = arg;
	else { // FIXME: raise an exception here
		d_warning(0, "Array type must be either list or tuple");
		return CORBA_FALSE;
	}
	if (PyTuple_Size(tuple) != tc->length) { // FIXME: raise exception
		d_warning(0, "Sequence length must be length %d", tc->length);
		return CORBA_FALSE;
	}
	for (i = 0; i < tc->length; i++)
		if (!marshal_arg(PyTuple_GetItem(tuple, i), buf, tc->subtypes[0]))
			return CORBA_FALSE; 
	return CORBA_TRUE;
}


static CORBA_boolean marshal_sequence(PyObject *arg, GIOPSendBuffer *buf,
                               CORBA_TypeCode tc)
{
	CORBA_unsigned_long  len;
	gchar               *string;
	CORBA_unsigned_long  i;

	if (!PySequence_Check (arg)) {
		raise_system_exception (OPExc_MARSHAL, 0, 
		                        CORBA_COMPLETED_NO, 
		                        "Value not a sequence");
		return CORBA_FALSE;
	}
	
	/* FIXME: raise an exception */
	if (tc->length && PySequence_Length (arg) > tc->length) {
		d_warning (0, "Sequence length exceeds bounds");
		return CORBA_TRUE;
	}
	len = PySequence_Length (arg);
	buf_putn (buf, &len, sizeof (len));
	d_message(3, "marshal_sequence: length = %d", len);
	
	if (PyString_Check (arg)) {
		string = PyString_AsString (arg);
		giop_send_buffer_append (buf, string, len);
		return CORBA_TRUE;
	}

	for (i = 0; i < len; i++) {
		PyObject *si;
		CORBA_boolean ret;

		si = PySequence_GetItem(arg, i);
		ret = marshal_arg (si, buf, tc->subtypes[0]);
		Py_DECREF(si);
		if (!ret)
			return CORBA_FALSE;
	}
		
	return CORBA_TRUE;
}

static CORBA_boolean marshal_enum(PyObject *arg, GIOPSendBuffer *buf,
                           CORBA_TypeCode tc)
{
	CORBA_unsigned_long v;
	// FIXME: raise exception
	if (!PyInt_Check(arg)) {
		d_message(0, "Enum element must be an integer value");
		return CORBA_FALSE;
	}
	v = PyInt_AsLong(arg);
	buf_putn(buf, &v, sizeof(v));
	return CORBA_TRUE;
}

static CORBA_boolean marshal_union(PyObject *arg, GIOPSendBuffer *buf,
                           CORBA_TypeCode tc)
{
	PyObject *v = PyObject_GetAttrString(arg, "v"),
	         *d = PyObject_GetAttrString(arg, "d");
	CORBA_long arm;
	CORBA_boolean result;

	if (!v || !d || v == Py_None || d == Py_None) {
		// FIXME: raise exception
		d_warning(0, "Both value and discriminator must be set!");
		return CORBA_FALSE;
	}
	arm = find_union_arm(tc, d);
	if (PyErr_Occurred())  // exception from find_union_arm
		return CORBA_FALSE;

	if (arm < 0) {
		// FIXME: raise exception
		d_warning(0, "Unknown discriminator in union");
		return CORBA_FALSE;
	}
	
	result = marshal_arg(d, buf, tc->discriminator);
	Py_DECREF(d);
	if (!result) {
		Py_DECREF(v);
		return CORBA_FALSE;
	}

	result = marshal_arg(v, buf, tc->subtypes[arm]);
	Py_DECREF(v);
	return result;
}

	
static CORBA_boolean marshal_long(PyObject *arg, GIOPSendBuffer *buf)
{
	CORBA_long v;
	if (!PyInt_Check(arg) && !PyLong_Check(arg)) {
		raise_system_exception(OPExc_MARSHAL, 0,
                            CORBA_COMPLETED_MAYBE, 
		                    "Expected long, got %s", 
		                    arg->ob_type->tp_name);
		return CORBA_FALSE;
	}
	if (!PyArg_Parse(arg, "l", &v))
		return CORBA_FALSE;
	buf_putn(buf, &v, sizeof(v));
	return CORBA_TRUE;
}

static CORBA_boolean marshal_char(PyObject *arg, GIOPSendBuffer *buf)
{
	CORBA_char v;
	if (!PyString_Check(arg)) {
		raise_system_exception(OPExc_MARSHAL, 0,
		                       CORBA_COMPLETED_MAYBE, 
		                       "Expected string, got %s", 
		                       arg->ob_type->tp_name);
		return CORBA_FALSE;
	}
	if (!PyArg_Parse(arg, "c", &v))
		return CORBA_FALSE;
	buf_putn(buf, &v, 1);
	return CORBA_TRUE;
}

	
static CORBA_boolean marshal_octet(PyObject *arg, GIOPSendBuffer *buf)
{
	CORBA_octet  v;
	gchar       *s;
	
	if (!PyInt_Check (arg) && 
	    (PyString_Check (arg) && PyString_Size (arg) != 1)) {
		raise_system_exception(OPExc_MARSHAL, 0,
		                       CORBA_COMPLETED_MAYBE, 
		                       "Expected integer or a string of length 1, "
		                       "got %s", 
		                       arg->ob_type->tp_name);
		return CORBA_FALSE;
	}

	if (PyInt_Check (arg)) {
		v = PyInt_AsLong (arg);
	} else if (PyString_Check (arg)) {
		s = PyString_AsString (arg);
		v = s[0];
	}
	buf_putn (buf, &v, sizeof (v));
	
	return CORBA_TRUE;
}

static CORBA_boolean marshal_float(PyObject *arg, GIOPSendBuffer *buf)
{
	CORBA_float v;
	if (PyInt_Check(arg) || PyLong_Check(arg)) {
		v = (CORBA_float)PyInt_AsLong(arg);
		if (PyErr_Occurred())
			return CORBA_FALSE;
	}
	else if (PyFloat_Check(arg)) {
		if (!PyArg_Parse(arg, "f", &v))
			return CORBA_FALSE;
	}
	else {
		raise_system_exception(OPExc_MARSHAL, 0,
		                       CORBA_COMPLETED_MAYBE, 
		                       "Expected float or int, got %s", 
		                       arg->ob_type->tp_name);
		return CORBA_FALSE;
	}
	buf_putn(buf, &v, sizeof(v));
	return CORBA_TRUE;
}

static CORBA_boolean marshal_double(PyObject *arg, GIOPSendBuffer *buf)
{
	CORBA_double v;

	if (PyInt_Check(arg))
		v = (CORBA_double)PyInt_AsLong(arg);
	else if (PyLong_Check(arg))
		v = (CORBA_double)PyLong_AsDouble(arg);
	else if (PyFloat_Check(arg))
		v = (CORBA_double)PyFloat_AsDouble(arg);
	else 
		raise_system_exception(OPExc_MARSHAL, 0,
		                       CORBA_COMPLETED_MAYBE, 
		                       "Expected float, got %s", 
		                       arg->ob_type->tp_name);
	if (PyErr_Occurred())
		return CORBA_FALSE;

	buf_putn(buf, &v, sizeof(v));
	return CORBA_TRUE;
}

static CORBA_boolean marshal_longlong(PyObject *arg, GIOPSendBuffer *buf)
{
	CORBA_long_long v;
	if (!PyInt_Check(arg)) {
		raise_system_exception(OPExc_MARSHAL, 0,
		                       CORBA_COMPLETED_MAYBE, 
		                       "Expected integer, got %s", 
		                       arg->ob_type->tp_name);
		return CORBA_FALSE;
	}
	if (!PyArg_Parse(arg, "l", &v))
		return CORBA_FALSE;
	buf_putn(buf, &v, sizeof(v));
	return CORBA_TRUE;
}

static CORBA_boolean marshal_any(PyObject *arg, GIOPSendBuffer *buf)
{
	CORBA_TypeCode_PyObject *tco;
	CORBA_TypeCode tc;

	if (!CORBA_Any_PyObject_Check(arg)) {
		raise_system_exception(OPExc_MARSHAL, 0,
		                       CORBA_COMPLETED_MAYBE, 
		                       "Expected any, got %s", 
		                       arg->ob_type->tp_name);
		return CORBA_FALSE;
	}
	tco = (CORBA_TypeCode_PyObject *)((CORBA_Any_PyObject *)arg)->tc_object;
	tc = tco->tc;
	ORBit_encode_CORBA_TypeCode(tc, buf);
	return marshal_arg(((CORBA_Any_PyObject *)arg)->value, buf, tc);
}

static CORBA_boolean marshal_typecode(PyObject *arg, GIOPSendBuffer *buf)
{
	CORBA_TypeCode tc;

	if (!CORBA_Any_PyObject_Check(arg)) {
		raise_system_exception(OPExc_MARSHAL, 0,
		                       CORBA_COMPLETED_MAYBE, 
		                       "Expected tc, got %s", 
		                       arg->ob_type->tp_name);
		return CORBA_FALSE;
	}
	tc = ((CORBA_TypeCode_PyObject *)arg)->tc;
	ORBit_encode_CORBA_TypeCode(tc, buf);
   /* Below commented out 08/24/01 (tack)
    * We shouldn't be doing this. 
	buf_putn(buf, &tc, sizeof(tc));
    */
	
	return CORBA_TRUE;
}

/* Local Variables: */
/* c-file-style: "python" */
/* vim: set noet ai fo=tcq sts=4 ts=4 sw=4 wrapmargin=8: */
/* End: */
