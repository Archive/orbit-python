/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com> 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include "exceptions.h"
#include "util.h"

static PyObject * SystemExcept_PyClass__init (PyObject * _,
					      PyObject * args, 
					      PyObject * keys);

static PyObject * SystemExcept_PyClass__str  (PyObject * _,
					      PyObject * args, 
					      PyObject * keys);

static PyObject * create_exception           (PyObject * ModuleDict,
					      char *     name,
					      PyObject * base);
	
GHashTable * exceptions;
PyObject * OPExc_Exception;
PyObject * OPExc_SystemException;
PyObject * OPExc_UserException;
PyObject * OPExc_UNKNOWN;
PyObject * OPExc_BAD_PARAM;
PyObject * OPExc_NO_MEMORY;
PyObject * OPExc_IMP_LIMIT;
PyObject * OPExc_COMM_FAILURE;
PyObject * OPExc_INV_OBJREF;
PyObject * OPExc_OBJECT_NOT_EXIST;
PyObject * OPExc_NO_PERMISSION;
PyObject * OPExc_INTERNAL;
PyObject * OPExc_MARSHAL;
PyObject * OPExc_INITIALIZE;
PyObject * OPExc_NO_IMPLEMENT;
PyObject * OPExc_BAD_TYPECODE;
PyObject * OPExc_BAD_OPERATION;
PyObject * OPExc_NO_RESOURCES;
PyObject * OPExc_NO_RESPONSE;
PyObject * OPExc_PERSIST_STORE;
PyObject * OPExc_BAD_INV_ORDER;
PyObject * OPExc_TRANSIENT;
PyObject * OPExc_FREE_MEM;
PyObject * OPExc_INV_IDENT;
PyObject * OPExc_INV_FLAG;
PyObject * OPExc_INTF_REPOS;
PyObject * OPExc_BAD_CONTEXT;
PyObject * OPExc_OBJ_ADAPTER;
PyObject * OPExc_DATA_CONVERSION;
PyObject * OPExc_TRANSACTION_REQUIRED;
PyObject * OPExc_TRANSACTION_ROLLEDBACK;
PyObject * OPExc_INVALID_TRANSACTION;
PyObject * OPExc_WRONG_TRANSACTION;

void
ORBit_Python_init_exceptions (PyObject *ModuleDict)
{
	PyObject *sysexc;
	PyMethodDef *def;
	PyObject *func, *meth;

	exceptions = g_hash_table_new (g_str_hash, g_str_equal);
	
	sysexc                       = OPExc_SystemException;
	OPExc_Exception              = create_exception (ModuleDict, "Exception", NULL);
	OPExc_SystemException        = create_exception (ModuleDict, "SystemException", OPExc_Exception);
	OPExc_UserException          = create_exception (ModuleDict, "UserException", OPExc_Exception);
	OPExc_UNKNOWN                = create_exception (ModuleDict, "UNKNOWN", sysexc);
	OPExc_BAD_PARAM              = create_exception (ModuleDict, "BAD_PARAM", sysexc);
	OPExc_NO_MEMORY              = create_exception (ModuleDict, "NO_MEMORY", sysexc);
	OPExc_IMP_LIMIT              = create_exception (ModuleDict, "IMP_LIMIT", sysexc);
	OPExc_COMM_FAILURE           = create_exception (ModuleDict, "COMM_FAILURE", sysexc);
	OPExc_INV_OBJREF             = create_exception (ModuleDict, "INV_OBJREF", sysexc);
	OPExc_OBJECT_NOT_EXIST       = create_exception (ModuleDict, "OBJECT_NOT_EXIST", sysexc);
	OPExc_NO_PERMISSION          = create_exception (ModuleDict, "NO_PERMISSION", sysexc);
	OPExc_INTERNAL               = create_exception (ModuleDict, "INTERNAL", sysexc);
	OPExc_MARSHAL                = create_exception (ModuleDict, "MARSHAL", sysexc);
	OPExc_INITIALIZE             = create_exception (ModuleDict, "INITIALIZE", sysexc);
	OPExc_BAD_TYPECODE           = create_exception (ModuleDict, "BAD_TYPECODE", sysexc);
	OPExc_BAD_OPERATION          = create_exception (ModuleDict, "BAD_OPERATION", sysexc);
	OPExc_NO_RESOURCES           = create_exception (ModuleDict, "NO_RESOURCES", sysexc);
	OPExc_NO_RESPONSE            = create_exception (ModuleDict, "NO_REPONSE", sysexc);
	OPExc_PERSIST_STORE          = create_exception (ModuleDict, "PERSIST_STORE", sysexc);
	OPExc_BAD_INV_ORDER          = create_exception (ModuleDict, "BAD_INV_ORDER", sysexc);
	OPExc_TRANSIENT              = create_exception (ModuleDict, "TRANSIENT", sysexc);
	OPExc_FREE_MEM               = create_exception (ModuleDict, "FREE_MEM", sysexc);
	OPExc_INV_IDENT              = create_exception (ModuleDict, "INV_IDENT", sysexc);
	OPExc_INV_FLAG               = create_exception (ModuleDict, "INV_FLAG", sysexc);
	OPExc_INTF_REPOS             = create_exception (ModuleDict, "INTF_REPOS", sysexc);
	OPExc_BAD_CONTEXT            = create_exception (ModuleDict, "BAD_CONTEXT", sysexc);
	OPExc_OBJ_ADAPTER            = create_exception (ModuleDict, "OBJ_ADAPTER", sysexc);
	OPExc_DATA_CONVERSION        = create_exception (ModuleDict, "DATA_CONVERSION", sysexc);
	OPExc_TRANSACTION_REQUIRED   = create_exception (ModuleDict, "TRANSACTION_REQUIRED", sysexc);
	OPExc_TRANSACTION_ROLLEDBACK = create_exception (ModuleDict, "TRANSACTION_ROLLEDBACK", sysexc);
	OPExc_INVALID_TRANSACTION    = create_exception (ModuleDict, "INVALID_TRANSACTION", sysexc);
	OPExc_WRONG_TRANSACTION      = create_exception (ModuleDict, "WRONG_TRANSACTION", sysexc);

	def = g_new(PyMethodDef, 1);
	def->ml_name  = g_strdup("__init__");
	def->ml_meth  = (PyCFunction)SystemExcept_PyClass__init;
	def->ml_flags = METH_VARARGS | METH_KEYWORDS;
	
	func = PyCFunction_New (def, OPExc_SystemException);
	meth = PyMethod_New (func, NULL, OPExc_SystemException);
	PyObject_SetAttrString (OPExc_SystemException, "__init__", meth);

	def = g_new (PyMethodDef, 1);
	def->ml_name  = g_strdup ("__str__");
	def->ml_meth  = (PyCFunction)SystemExcept_PyClass__str;
	def->ml_flags = METH_VARARGS | METH_KEYWORDS;
	
	func = PyCFunction_New (def, OPExc_SystemException);
	meth = PyMethod_New (func, NULL, OPExc_SystemException);
	PyObject_SetAttrString (OPExc_SystemException, "__str__", meth);
}

CORBA_boolean
check_corba_ex (CORBA_Environment *ev)
{
	if (ev->_major != CORBA_NO_EXCEPTION) {
		PyObject *exc;
		d_message(1, "CORBA exception raised: %s", CORBA_exception_id(ev));
		exc = g_hash_table_lookup(exceptions, CORBA_exception_id(ev));
		if (!exc) {
			raise_system_exception(OPExc_UNKNOWN, 0, CORBA_COMPLETED_MAYBE, NULL);
			return CORBA_FALSE;
		}
		raise_system_exception(exc, 0, CORBA_COMPLETED_MAYBE, NULL);
		return CORBA_FALSE;
	}
	
	return CORBA_TRUE;
}

void
raise_user_exception (char *repo_id, PyObject *o)
{	
	PyObject *error = NULL;
	
	if (!o) {
		Py_INCREF(Py_None);
		o = Py_None;
	}
	
	if (repo_id) {
		error = (PyObject *)g_hash_table_lookup(exceptions, repo_id);
	}
	
	if (error) {
		PyErr_SetObject(error, o);
	} else {
		raise_system_exception(OPExc_UNKNOWN, 0, CORBA_COMPLETED_MAYBE, NULL);
	}
}

void *
raise_system_exception (PyObject *              exc,
			CORBA_unsigned_long     minor,
			CORBA_completion_status status,
			char *                  info,
			...)
{
	PyObject *data;
	PyObject *instance;
	char      buffer[500];
	
	if (info) {
		va_list args;
		va_start(args, info);
		vsprintf(buffer, info, args);
		info = buffer;
	}

	data = PyTuple_New(2);
	PyTuple_SetItem(data, 0, PyLong_FromLong(minor));
	PyTuple_SetItem(data, 1, PyLong_FromLong(status));
	
	instance = PyInstance_New(exc, data, NULL);
	if (!instance)
		return NULL;

	if (info) {
		PyObject *infoobj = PyString_FromString(info);
		PyObject_SetAttrString(instance, "_info", infoobj);
		Py_DECREF(infoobj);
	}
	
	PyObject_SetAttrString(instance, "args", data);
	PyErr_SetObject(exc, instance);
	Py_DECREF(instance);
	Py_DECREF(data);

	/* return NULL simply so callers can do: return raise_system_exception() */
	return NULL;
}
	
PyObject *
UserExcept_PyClass__init (PyObject *_,
			  PyObject *args,
			  PyObject *keys)
{

	PyObject *self = PyTuple_GetItem(args, 0);
	if (self && keys) {
		int pos = 0;
		PyObject *key, *value;
		while (PyDict_Next(keys, &pos, &key, &value))
			PyObject_SetAttr(self, key, value);
	}

	Py_INCREF(Py_None);
	return Py_None;
}

PyObject *
UserExcept_PyClass__str (PyObject *_,
			 PyObject *args,
			 PyObject *keys)
{
	PyObject *self = PyTuple_GetItem(args, 0);
	PyObject *dict = PyObject_GetAttrString(self, "__dict__");
	PyObject *tuple, *ret, *fmt;
	int size = PyDict_Size(dict);

	if (size == 0) {
		Py_DECREF(dict);
		return PyString_FromString("User exception with no members");
	}

	tuple = PyTuple_New(1);
	PyTuple_SetItem(tuple, 0, dict);
	fmt = PyString_FromString("User exception members: %s");
	ret = PyString_Format(fmt, tuple);
	Py_DECREF(fmt);
	Py_DECREF(tuple);
	Py_DECREF(dict);
	return ret;
}

static PyObject *
SystemExcept_PyClass__init (PyObject *_,
			    PyObject *args, 
			    PyObject *keys)
{
	PyObject *self;
	int minor, completed;

	if (!PyArg_ParseTuple(args, "Oll", &self, &minor, &completed)) {
		PyErr_Print();
		return NULL;
	}
	PyObject_SetAttrString(self, "minor", PyTuple_GetItem(args, 1));
	PyObject_SetAttrString(self, "completed", PyTuple_GetItem(args, 2));
	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *
SystemExcept_PyClass__str (PyObject *_, PyObject *args, 
			   PyObject *keys)
{
	PyObject *self = PyTuple_GetItem(args, 0);

	int minor, completed;
	PyObject *minor_obj, *completed_obj;
	char *txt, str[1000];

	minor_obj = PyObject_GetAttrString(self, "minor");
	completed_obj = PyObject_GetAttrString(self, "completed");
	
	PyArg_Parse(minor_obj, "l", &minor);
	PyArg_Parse(completed_obj, "l", &completed);

	switch (completed) {
		case 0: txt = "CORBA_COMPLETED_YES"; break;
		case 1: txt = "CORBA_COMPLETED_NO"; break;
		case 2: txt = "CORBA_COMPLETED_MAYBE"; break;
		default: txt = "[Invalid CORBA_COMPLETED code]"; break;
	}

	if (PyObject_HasAttrString(self, "_info")) {
		PyObject *info_obj = PyObject_GetAttrString(self, "_info");
		char *info = PyString_AsString(info_obj);
		Py_DECREF(info_obj);
		snprintf(str, 1000, "Minor: %d, Completed: %s.\nInfo: %s.", 
		         minor, txt, info);
	}
	else
		snprintf(str, 1000, "Minor: %d, Completed: %s.", minor, txt);
	Py_DECREF(minor_obj);
	Py_DECREF(completed_obj);
	return PyString_FromString(str);
}

static PyObject *
create_exception (PyObject *ModuleDict, char *name, PyObject *base)
{
	char *full_name = g_strconcat("CORBA.", name, NULL),
	     *repo_id = g_strconcat("IDL:omg.org/CORBA/", name, ":1.0", NULL);
	
	PyObject *e = PyErr_NewException(full_name, base, NULL);
	PyObject_SetAttrString(e, "__repo_id", PyString_FromString(repo_id));
	PyDict_SetItemString(ModuleDict, name, e);

	g_hash_table_insert(exceptions, repo_id, e);
	repo_id = g_strconcat("IDL:CORBA/", name, ":1.0", NULL);
	g_hash_table_insert(exceptions, repo_id, e);

	g_free(full_name);
	// repo_id still used by exceptions hash
	return e;
}
	
/* Local Variables: */
/* c-file-style: "python" */
/* End: */
