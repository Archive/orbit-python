/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Johan Dahlin
 *
 */

#ifndef __MARSHAL_H__
#define __MARSHAL_H__

#include "orbit-python.h"

void                 ORBit_Python_init_marshal (void);

CORBA_boolean        marshal_arg               (PyObject *                   arg,
						GIOPSendBuffer *             buf,
						CORBA_TypeCode               tc);

CORBA_exception_type marshal_exception         (PyObject *                   type,
						PyObject *                   data,
						GIOPSendBuffer *             buf,
						CORBA_TypeCode               tc,
						CORBA_OperationDescription * opd);
void ORBit_Python_init_marshal(void);

#endif /* __MARSHAL_H__ */
