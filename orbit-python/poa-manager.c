/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000,2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2001,2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 *	Wraps POAManager
 *	Methods handled:
 *		activate
 *
 */

#include "poa-manager.h"
#include "util.h"

static PyObject * POAManager_PyObject__getattr  (POAManager_PyObject * self,
						 char *                name);

static void       POAManager_PyObject__dealloc  (POAManager_PyObject * self);

static int        POAManager_PyObject__setattr  (POAManager_PyObject * self,
						 char *                name,
						 PyObject *            obj);

static PyObject * POAManager_PyObject__activate (POAManager_PyObject * self, 
						 PyObject *            args);

PyMethodDef POAManager_PyObject_methods[] = {
	{ "activate", (PyCFunction)POAManager_PyObject__activate, METH_VARARGS },
	{ NULL, NULL }
};

PyTypeObject POAManager_PyObject_Type = {
	PyObject_HEAD_INIT(NULL)
	0,                                         /*ob_size*/
	"PortableServer.POAManager",               /*tp_name*/
	sizeof(POAManager_PyObject),               /*tp_basicsize*/
	0,                                         /*tp_itemsize*/
	
	/* methods */
	(destructor)POAManager_PyObject__dealloc,  /*tp_dealloc*/
	0,                                         /*tp_print*/
	(getattrfunc)POAManager_PyObject__getattr, /*tp_getattr*/
	(setattrfunc)POAManager_PyObject__setattr, /*tp_setattr*/
	0,                                         /*tp_compare*/
	0,                                         /*tp_repr*/
	0,                                         /*tp_as_number*/
	0,                                         /*tp_as_sequence*/
	0,                                         /*tp_as_mapping*/
	0,                                         /*tp_hash*/
};

POAManager_PyObject *
POAManager_Object_to_PyObject (PortableServer_POAManager o)
{
	POAManager_PyObject *self;
	self = PyObject_NEW (POAManager_PyObject,
			     &POAManager_PyObject_Type);
	
	if (!self) {
		return NULL;
	}
	
	CORBA_exception_init(&self->ev);
	self->obj = o;
	return self;
}

static PyObject *
POAManager_PyObject__activate (POAManager_PyObject * self, 
			       PyObject *            args)
{
	d_message (5, "%s: entered", __FUNCTION__);
	
	if (!PyArg_ParseTuple(args, ":POAManager.activate")) {
		return NULL;
	}
	
	PortableServer_POAManager_activate (self->obj, &self->ev);
	if (!check_corba_ex (&self->ev)) {
		return NULL;
	}
	
	Py_INCREF(Py_None);
	return Py_None;
}

static void
POAManager_PyObject__dealloc (POAManager_PyObject * self)
{
	PyMem_DEL (self);
}

static PyObject *
POAManager_PyObject__getattr (POAManager_PyObject * self,
			      char *                name)
{
	return Py_FindMethod (POAManager_PyObject_methods, (PyObject *)self, name);
}

static int
POAManager_PyObject__setattr (POAManager_PyObject * self,
			      char *                name,
			      PyObject *            v)
{
	return -1;
}

/* Local Variables: */
/* c-file-style: "python" */
/* End: */
