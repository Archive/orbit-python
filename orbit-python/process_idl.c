/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000-2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2001-2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry 
 */

#include "process_idl.h"

#include "util.h"

#include <glib.h>

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>

#define is_keyword(x, n) \
    (!strncmp(p, x, n)  && \
    (p > line ? isspace(*(p-1)) : 1) && \
    (p + n < line + strlen(line) ? isspace(*(p+n)) : 1 ))

#define d(x)

typedef struct _IDLFile IDLFile;
typedef struct _IDLModule IDLModule;

struct _IDLFile {
	char     *name;
	GSList   *includes;
	GSList   *included_by;
	GSList   *defines_modules;
	GSList   *defines;
	gboolean  loaded;          /* true if this file has been loaded */
};
	
struct _IDLModule {
	char      *name;
	int        scope_level; 
	char       is_interface; /* this is really an interface, not a module */
	GSList    *files;        /* files that define this module -- list of IDLFile */
	GSList    *modules;      /* list of sub modules -- list if IDLModule */
	IDLModule *parent;
};
	
static IDLModule  *global_module = NULL;
static GHashTable *idl_files = NULL;

static IDLModule *
find_module (const char *name,
	     IDLModule  *top,
	     gboolean    do_create)
{
	GSList *p = top->modules;
	IDLModule *newmod = 0;

	d(g_message ("%s: entered (name=%s)",
		     __FUNCTION__, name));
	
	while (p) {
		if (!strcmp( ((IDLModule *)p->data)->name, name))
			return p->data;
		p = p->next;
	}

	if (do_create) {
		newmod = g_new0(IDLModule, 1);
		newmod->name = g_strdup(name);
		newmod->parent = top;
		top->modules = g_slist_append(top->modules, newmod);
	}
	
	return newmod;
}

static IDLFile *
find_file (const char *filename,
	   gboolean    do_create)
{
	IDLFile *file;

	file = g_hash_table_lookup (idl_files, filename);
	
	if (!file && do_create) {
		file = g_new0 (IDLFile, 1);
		file->name = g_strdup (filename);
		g_hash_table_insert (idl_files, file->name, file);
	}
	
	return file;
}
	

static void
add_file_to_module (IDLModule *module, char *filename)
{
	GSList *p;
	IDLFile *file, *curfile;

	file = find_file (filename, 1);

	for (p = module->files; p; p = p->next) {
		IDLFile *curfile = (IDLFile*)p->data;
		
		if (!strcmp (curfile->name, filename)) {
			return;
		}
	}
	
	module->files = g_slist_append (module->files, file);
}
	

static char *
construct_full_path(char *_fullpath, char *file)
{
	char *fullpath = g_strdup(_fullpath),
	     *p = fullpath + strlen(fullpath), *newpath;
	while (p >= fullpath && *p != '/')
		p--;
	*p = 0;

	newpath = g_strconcat(fullpath, "/", file, NULL);
	g_free(fullpath);
	return newpath;
}
	
static void
preprocess_idl_file (char *file)
{
	FILE *fp;
	char line[512], *p, *q, 
	     name[100], *name_ptr = 0;
	gboolean in_comment = FALSE, grab_name = FALSE, grab_include = FALSE,
	         grab_interface = FALSE;
	IDLModule *module = global_module;
	IDLFile *idl_file = find_file(file, 1);
	int scope_level = 0;

	d_message (3, "%s: opening file %s",
		   __FUNCTION__, file);
	
	if ((fp = fopen(file, "r")) == NULL) {
		return;
	}

	for (;;) {
		fgets(line, 512, fp);
		line[strlen(line) - 1] = 0;
		if (feof(fp)) {
			break;
		}
		
		/* first filter out '//' comments */
		if ((p = strstr(line, "//")) != NULL) 
			*p = 0;
		
		/* handle * / close comment */
		if (in_comment) {
			if ((p = strstr(line, "*/")) == NULL) /* comment doesn't end */
				continue;
			strcpy(line, p + 2);
			in_comment = 0;
		}

		/* handle / * open comment */
		if ((p = strstr(line, "/*")) != NULL) {
			q = strstr(p, "*/");
			*p = 0;
			
			/* comment spans multiple lines */
			if (!q) {
				in_comment = TRUE;
			/* comment on one line */
			} else {
				strcat(line, q + 2);
			}
		}
		/* Done with comments */

		/* remove string literals (since they can contain keywords)
		 * FIXME: this only handles one, need to loop
		 */
		if ((p = strstr(line, "\"")) != NULL) {
			if (!strstr(line, "#include")) {
				*p++ = 0;
				if ((q = strstr(p, "\"")) != NULL)
					strcat(line, q + 1);
			}
		}

		/* now work through the line */
		p = line;

		/* Strip leading whitespace */
		while (isspace(*p)) p++;

		/* Check for defines that need to exist to properly import this file.
		 * Yes, this is a kludge.  A god awful shameful kludge.  If you want
		 * to do it properly, by all means ... :)
		 * This essentially does:
		 *    /\! \s* define \s* \( (\S+?) \) /x
		 */
		if (!strncmp(p, "#if", 3) && strstr(p, "__ORBIT_IDL__")) {
			char *end;
			/* Okay, search for the '!' */
			#define strip_whitespace() \
				while (*p && isspace(*p)) p++; if (!p) continue
			
			p = strstr (p, "!");
			if (!p) {
				continue;
			}
			
			p++;
			strip_whitespace();
			
			/* "defined" should follow
			 * FIXME: it doesn't have to follow, it could be later.
			 */
			
			if (strncmp(p, "defined", 7)) {
				continue;
			}
			
			p+= 7;
			
			/* strip whitespace */
			strip_whitespace();
			
			if (*p != '(') {
				continue;
			}
			
			p++;
			strip_whitespace();
			end = p;
			
			/* now record until whitespace or close paren */
			while (*end && !isspace(*end) && *end != ')') {
				end++;
			}
			
			if (*end) {
				*end = 0;
			}
			
			idl_file->defines = g_slist_append(idl_file->defines, g_strdup(p));
			continue;
		}

		while (*p) {
			if (*p == ';' && grab_name)
				grab_name = FALSE;
			if (*p == '{') {
				if (grab_name) {
					/* Could be a derived interface -- strip out the parent */
					char *ptr = strstr(name, ":");
					if (ptr)
						*ptr = 0;

					idl_file->defines_modules = g_slist_append(
							idl_file->defines_modules, g_strdup(name));
					module = find_module(name, module, 1);
					module->scope_level = scope_level + 1;
					module->is_interface = grab_interface ? 1 : 0;
					add_file_to_module(module, file);
					grab_interface = grab_name = FALSE;
				}
				scope_level++;
			}
			else if (*p == '}') {
				if (module->scope_level == scope_level && module) {
					module = module->parent;
				}
				scope_level--;
			}
			else if ((*p == '>' || *p == '"') && grab_include) {
				if (*p != '"' || *name != 0) {
					char *fp;
					IDLFile *inc;

					grab_include = FALSE;
					q = name;
					while (isspace(*q) || *q == '"' || *q == '<') q++;
					fp = construct_full_path(file, q);
					inc = find_file(fp, 1);
					idl_file->includes = g_slist_append(idl_file->includes, fp);
					inc->included_by = g_slist_append(inc->included_by,
					                                  g_strdup(file));
				}
			}
			else if (is_keyword("module", 6)) {
				memset(name, 0, sizeof(name));
				name_ptr = name;
				grab_name = TRUE;
				p += 5;
			}

			else if (is_keyword("interface", 9)) {
				if (scope_level == 0)
					add_file_to_module(global_module, file);
				memset(name, 0, sizeof(name));
				name_ptr = name;
				grab_name = TRUE;
				grab_interface = TRUE;
				p += 8;
			}
			else if (is_keyword("#include", 8)) {
				memset(name, 0, sizeof(name));
				name_ptr = name;
				grab_include = TRUE;
				p += 7;
			}
			else if (scope_level == 0 && (
			         is_keyword("struct", 6) ||
			         is_keyword("enum", 4) ||
			         is_keyword("union", 5) ||
			         is_keyword("const", 5) )) {
				add_file_to_module(global_module, file);
			}
			else if ((grab_name || grab_include) && !isspace(*p))
				*name_ptr++ = *p;
			p++;
		}
	}
	fclose(fp);

}

void process_dir(char *path)
{
	DIR *dir;
	struct dirent *entry;

	if ((dir = opendir(path)) == NULL)
		return;

	while ((entry = readdir(dir))) {
		if (!strncmp(entry->d_name + strlen(entry->d_name) - 4, ".idl", 4)) {
			char *fname = g_strconcat(path, "/", entry->d_name, NULL);
			preprocess_idl_file(fname);
			g_free(fname);
		}
	}
	closedir(dir);

}

void
process_idl_paths (char *pathstr)
{
	char *tmp = g_strdup(pathstr), *p, *last = tmp;

	global_module = g_new0(IDLModule, 1);
	idl_files = g_hash_table_new(g_str_hash, g_str_equal);

	while ((p = strstr(last, ":")) != NULL) {
		*p = 0;
		if (last[strlen(last) - 1] == '/')
			last[strlen(last) - 1] = 0;
		process_dir(last);
		last = p + 1;
	}
	if (*last) {
		if (last[strlen(last) - 1] == '/')
			last[strlen(last) - 1] = 0;
		process_dir(last);
	}

}

/********************************************************/

static void
hash_keys_to_list (gpointer key, gpointer value, gpointer list)
{
	*(GSList **)list = g_slist_append(*(GSList **)list, key);
}

static void
hash_values_to_list (gpointer key, gpointer value, gpointer list)
{
	*(GSList **)list = g_slist_append(*(GSList **)list, value);
}

static GSList *
hash_table_as_list (GHashTable *hash, char key)
{
	GSList *list = 0;
	if (key) {
		g_hash_table_foreach(hash, hash_keys_to_list, &list);
	} else {
		g_hash_table_foreach(hash, hash_values_to_list, &list);
	}
	
	return list;
}

/*
 * Let the set of IDLs for a module be L.
 * Let the module we're importing be M.
 *
 * Step 1:
 * If A includes a set S, S intersects with L, and A is not in L,
 * then remove S from L and add A if A contains M or A defines no module.
 *
 * Step 2: 
 * If A includes B, and A,B are in L, then remove B.
 */
static void
narrow_idl_file_list (const char *module,
		      GHashTable *hash)
{
	GSList   *idl_files_list;
	GSList   *list;
	GSList   *p;
	IDLFile  *curfile;
	gboolean  skip;

	d(g_message ("%s: entered. (module=%s)", __FUNCTION__, module));
	
	idl_files_list = hash_table_as_list (idl_files, 0);
	
	/* Step 1 */
	for (list = idl_files_list; list;
	     list = list->next) {
		curfile = (IDLFile *)list->data;
		
		/* curfile is A
		 * Skip A if it's in L
		 */
		if (g_hash_table_lookup (hash, curfile->name)) {
			d(g_message ("%s: skipping %s; in L",
				     __FUNCTION__, curfile->name));
			continue;
		}
		
		/* Skip A if it does not define M */
		if (!curfile->defines_modules) {
			d(g_message ("%s: skipping %s; not defined",
				     __FUNCTION__, curfile->name));
			continue;
		}
		
		skip = TRUE;
		for (p = curfile->defines_modules; p; p = p->next) {
			if (!strcmp (p->data, module)) {
				skip = FALSE;
				break;
			}
		}

		if (skip) {
			d(g_message ("%s: skipping %s; not in defines",
				     __FUNCTION__, curfile->name));		
			continue;
		}

		/* Check to see if S intersects with L.  If it does, flag will be 1 */
		skip = TRUE;
		for (p = curfile->includes; p; p = p->next) {
			if (g_hash_table_lookup (hash, p->data)) {
				skip = FALSE;
				break;
			}
		}
		
		if (skip) {
			d(g_message ("%s: skipping %s; not in includes",
				     __FUNCTION__, curfile->name));
			continue;
		}

                /* Now we loop again through those files and remove them */
		for (p = curfile->includes; p; p = p->next) {
			g_hash_table_remove (hash, p->data);
		}
			
		/* Now add A to L only if it hasn't already been loaded */
		if (!curfile->loaded) {
			d(g_message ("%s: adding %s", __FUNCTION__, curfile->name));
			g_hash_table_insert (hash, curfile->name, (gpointer)1);
		}

	}
#if 0
	/* Step 2 */
	for (list = idl_files_list; list; list = list->next) {
		curfile = (IDLFile *)list->data;

		for (p = curfile->includes; p; p = p->next) {
			if (g_hash_table_lookup(hash, p->data) &&
			    g_hash_table_lookup(hash, curfile->name)) {
				d(g_message ("%s: removing %s",
					     __FUNCTION__, curfile->name));
				g_hash_table_remove(hash, p->data);
			}
		}
	}
#endif
	g_slist_free (idl_files_list);
}
	

static GSList *
get_module_file_list (IDLModule  *module,
		      GHashTable *files)
{
	GSList *p;
	
	for (p = module->files; p; p = p->next) {
		IDLFile *file = (IDLFile *)p->data;
		if (!file->loaded) {
			d(g_message ("%s: adding %s",
				     __FUNCTION__, file->name));
			g_hash_table_insert (files, file->name, (gpointer)1);
		}
	}
	
	for (p = module->modules; p; p = p->next) {
		get_module_file_list ((IDLModule *)p->data, files);
	}
}

static IDLModule *
find_module_from_path (IDLModule  *top,
		       char       *_name, 
		       char        from_global,
		       char      **failed)
{
	char *name = g_strdup (_name), *q;
	GSList *p;

	d(g_message ("%s: entered.", __FUNCTION__));
	
	q = strstr (name, ".");
	if (q) {
		*q = 0;
	}

	for (p = top->modules; p; p = p->next) {
		IDLModule *curmod = (IDLModule *)p->data;
		
		/* Skip global interfaces -- the user must import from _GlobalIDL */
		if (curmod->is_interface &&
		    top == global_module &&
		    !from_global) {
			continue;
		}
		
		if (!strcmp (name, "*")) {
			g_free (name);
			return top;
		}
		
		if (!strcmp (curmod->name, name)) {
			/* Import the whole module if a specific interface was requested */
			if (curmod->is_interface && top != global_module) {
				g_free(name);
				return top;
			}
			
			/* found, recurse if there's more */
			if (q) {
				IDLModule *m;
				m = find_module_from_path(curmod, q + 1, from_global, failed);
				g_free(name);
				return m;
			}
			
			g_free(name);
			return curmod;
		}
	}
	
	if (failed) {
		*failed = top != global_module ? g_strdup(_name) : 0;
	}
	
	g_free (name);
	return 0;
}
	
static void
_get_defines_for_file (const char  *filename,
		       GHashTable **hash,
		       GHashTable **parsed)
{
	IDLFile *idlfile;
	GSList *p, *list;

	d(g_message ("%s: entered. (filename=%s)", __FUNCTION__, filename));
	
	idlfile = find_file (filename, 0);
	if (!idlfile)
		return;
	
	list = g_hash_table_lookup (*parsed, filename);
	if (!list) {
		list = g_slist_alloc ();
	}
	
	/* defines */
	p = idlfile->defines;
	while (p) {
		g_hash_table_insert(*hash, p->data, (gpointer)1);
		p = p->next;
	}
	
	/* includes */
	for (p = idlfile->includes; p; p = p->next) {
		if (g_slist_find (list, p->data) != NULL) {
			d(g_message ("%s: %s is already parsed", filename, p->data));
			continue;
		} else {
			list = g_slist_append (list, p->data);
			g_hash_table_insert (*parsed, g_strdup (filename), list);
			d(g_message ("%s: %s is now parsed", filename, p->data));
		}
		
		_get_defines_for_file(p->data, hash, parsed);
	}
	
}

GSList *
get_idl_list_for_module (char      *name,
			 gboolean   from_global,
			 char     **failed)
{
	IDLModule *module;
	GHashTable *files = g_hash_table_new(g_str_hash, g_str_equal);
	GSList *list = 0;
	
	d(g_message ("%s: entered. (name=%s)",
		     __FUNCTION__, name));

	module = find_module_from_path(global_module, name, from_global, failed);
	if (module) {
		GSList *p;
		
		get_module_file_list (module, files);
		narrow_idl_file_list (name, files);
		list = hash_table_as_list(files, 1);

		for (p = list; p; p = p->next) {
			d(g_message ("%s: returning %s", __FUNCTION__, (char*)p->data));
		}

	}
	g_hash_table_destroy(files);

	/* If list is NULL it means we don't load any files for this module
	 * (probably because they were already loaded).  So we set the failed
	 * message to NULL and return NULL, in which case the caller will
	 * just try to import the module.
	 */
	if (module && !list && failed)
		*failed = 0;
	
	return list;
}

GSList *
get_global_idl_files (void)
{
	GSList *ret = 0;
	GSList *p = global_module->files;
	
	while (p) {
		IDLFile *file = (IDLFile *)p->data;
		ret = g_slist_append(ret, file->name);
		p = p->next;
	}
	
	return ret;
}

GSList *
get_defines_for_file (const char *filename)
{
	GHashTable *hash;
	GHashTable *includes;
	GSList *ret;
	
	d(g_message ("%s: entered. (filename=%s)", __FUNCTION__, filename));
	
	hash     = g_hash_table_new (g_str_hash, g_str_equal);
	includes = g_hash_table_new (g_str_hash, g_str_equal);
	
	_get_defines_for_file (filename, &hash, &includes);
	
	ret = hash_table_as_list (hash, 1);
	g_hash_table_destroy (hash);
	
	return ret;
}

void
set_file_as_loaded (char        *filename,
		    GHashTable **loaded)
{
	IDLFile *file;
	GSList  *p;
	GSList  *list;	
	
	d(g_message ("%s: entered. (filename=%s)", __FUNCTION__, filename));
	
	file = g_hash_table_lookup(idl_files, filename);
	if (!file) {
		d(g_message ("I haven't parsed %s yet!", filename));
		return;
	}
	
	if (!loaded) {
		d(g_message ("Creating new hash table for %s", filename));
		*loaded = g_hash_table_new (g_str_hash, g_str_equal);
	}
	
	list = g_hash_table_lookup (*loaded, filename);
	if (!list) {
		list = g_slist_alloc ();
	}
	
	file->loaded = TRUE;
	for (p = file->includes; p; p = p->next) {
		if (g_slist_find (list, p->data) != NULL) {
			d(g_message ("%s: %s is already marked as loaded", filename, p->data));
			continue;
		} else {
			d(g_message ("%s: %s is now marked loaded.", filename, p->data));
			list = g_slist_append (list, p->data);
			g_hash_table_insert (*loaded, filename, list);
		}
		set_file_as_loaded(p->data, loaded);
	}
}
