/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000,2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2001,2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 *	Wraps POA
 *	Methods handled:
 *           activate_object
 *           activate_object_with_id
 *           deactivate_object
 *           servant_to_reference
 *           reference_to_servant
 *
 */

#include "poa-object.h"

#include "corba-object.h"
#include "corba-orb.h"
#include "exceptions.h"
#include "server.h"
#include "util.h"

static void       POA_PyObject__dealloc                 (POA_PyObject * self);

static PyObject * POA_PyObject__getattr                 (POA_PyObject * self,
							 char *         name);

static int        POA_PyObject__setattr                 (POA_PyObject * self,
							 char *         name,
							 PyObject *     obj);


static PyObject * POA_PyObject__activate_object         (POA_PyObject * self,
							 PyObject *     args);

static PyObject * POA_PyObject__activate_object_with_id (POA_PyObject * self,
							 PyObject *     args);

static PyObject * POA_PyObject__get_the_POAManager      (POA_PyObject * self,
							 PyObject *     args);

static PyObject * POA_PyObject__reference_to_servant    (POA_PyObject * self,
						         PyObject *     args);

GHashTable *poa_objects;

/* PortableServer.Servant initialized from PortableServermodule.c */
PyObject *servant_base;


PyMethodDef POA_PyObject_methods[] = {
	/* public */
	{ "servant_to_reference",    (PyCFunction)POA_PyObject__servant_to_reference,    METH_VARARGS },
	{ "deactivate_object",       (PyCFunction)POA_PyObject__deactivate_object,       METH_VARARGS },

	/* private */
        { "activate_object",         (PyCFunction)POA_PyObject__activate_object,         METH_VARARGS },
	{ "activate_object_with_id", (PyCFunction)POA_PyObject__activate_object_with_id, METH_VARARGS },
	{ "reference_to_servant",    (PyCFunction)POA_PyObject__reference_to_servant,    METH_VARARGS },
	{ "_get_the_POAManager",     (PyCFunction)POA_PyObject__get_the_POAManager,      METH_VARARGS },
	{ NULL, NULL }
};

PyTypeObject POA_PyObject_Type = {
	PyObject_HEAD_INIT(NULL)
	0,                                 /*ob_size*/
	"PortableServer.POA",              /*tp_name*/
	sizeof(POA_PyObject),              /*tp_basicsize*/
	0,                                 /*tp_itemsize*/
	
	/* methods */
	(destructor)POA_PyObject__dealloc,  /*tp_dealloc*/
	0,                                  /*tp_print*/
	(getattrfunc)POA_PyObject__getattr, /*tp_getattr*/
	(setattrfunc)POA_PyObject__setattr, /*tp_setattr*/
	0,                                  /*tp_compare*/
	0,                                  /*tp_repr*/
	0,                                  /*tp_as_number*/
	0,                                  /*tp_as_sequence*/
	0,                                  /*tp_as_mapping*/
	0,                                  /*tp_hash*/
};

/* Public */

POA_PyObject *
POA_Object_to_PyObject (PortableServer_POA object)
{
	POA_PyObject *self;
	CORBA_ORB_PyObject *orb;
		
	self = g_hash_table_lookup (poa_objects, object);
	if (self) {
		Py_INCREF(self);
		return self;
	}

	orb = g_hash_table_lookup (orb_objects, object->orb);
	if (!orb) {
		return raise_system_exception (OPExc_BAD_PARAM, 0, CORBA_COMPLETED_NO,
					       "POA object belongs to unknown ORB");
	}
	
	self = PyObject_NEW (POA_PyObject, &POA_PyObject_Type);
	if (!self) {
		return NULL;
	}
	
	CORBA_exception_init (&self->ev);
	self->obj = object;
	self->orb = orb;
	Py_INCREF (orb);

	g_hash_table_insert (orb_objects, object, self);
	
	return self;
}

PyObject *
POA_PyObject__servant_to_reference (POA_PyObject * self,
				    PyObject *     args)
{
	CORBA_Object              ref;
	CORBA_Object              ref2;
	PyObject *                servant;
	Servant_PyInstance_Glue * servant_glue;

	d_message (5, "%s: entered", __FUNCTION__);
	
	if (!PyArg_ParseTuple(args, "O:servant_to_reference", &servant)) {
		return NULL;
	}

	servant_glue = g_hash_table_lookup(servant_instance_glue, servant);
	
	/* ORBit will implicitly activate for us, but we need to prevent it
	 * from doing that because our activate_object does some additional
	 * stuff.
	 * (XXX: should handle MULTIPLE_ID, but ORBit doesn't appear to support
	 * it.  We don't support setting POA policies yet anyway.)
	 */
	if (self->obj->p_implicit_activation == PortableServer_IMPLICIT_ACTIVATION &&
		 (!servant_glue || !servant_glue->activated)) {
			PyObject *res = POA_PyObject__activate_object(self, args);
			if (!res) {
				return NULL;
			}
			Py_DECREF(res);
			servant_glue = g_hash_table_lookup(servant_instance_glue, servant);
	
	}
		
	if (!servant_glue) {
		return raise_system_exception (OPExc_BAD_PARAM, 0, CORBA_COMPLETED_NO,
					       "object not an activated servant");
	}
	
	if (!servant_glue->activated) {
		return raise_system_exception (OPExc_BAD_PARAM, 0, CORBA_COMPLETED_NO,
					       "servant must be activated");
	}

	/* ref = CORBA_OBJECT_NIL */
	ref = PortableServer_POA_servant_to_reference (self->obj,
						       servant_glue,
						       &self->ev);
	
	ref2 = CORBA_Object_duplicate (ref, &self->ev);
	
	if (!check_corba_ex (&self->ev)) {
		return NULL;
	}
	
	if (ref == CORBA_OBJECT_NIL) {
		Py_INCREF(Py_None);
		return Py_None;
	}
	
	/* finally, take care of returning reference -- creating it if it
	 * doesn't exist
	 */
	return (PyObject *)CORBA_Object_to_PyObject(ref2);
}

PyObject *
POA_PyObject__deactivate_object (POA_PyObject * self,
				 PyObject *     args)
{
	PyObject *instance;
	Servant_PyInstance_Glue *instance_glue;

	g_return_if_fail (self != NULL);

	if (!PyArg_ParseTuple(args, "O:deactivate_object", &instance)) {
		return NULL;
	}

	instance_glue = g_hash_table_lookup(servant_instance_glue, instance);
	
	/* FIXME: should raise ObjectNotActive */
	if (!instance_glue) {
		return raise_system_exception(OPExc_BAD_INV_ORDER, 0, CORBA_COMPLETED_NO,
                                    "object not an activated servant");
	}
	
	if (!instance_glue->activated) {
		return raise_system_exception(OPExc_BAD_INV_ORDER, 0, CORBA_COMPLETED_NO,
                                    "servant not activated");
	}
	
	d_message(1, "Deactivating %x", (guint)instance);
	PortableServer_POA_deactivate_object(self->obj, instance_glue->oid, 
		                                     &self->ev);
	if (!check_corba_ex(&self->ev))
		return NULL;

	instance_glue->activated = CORBA_FALSE;
	Py_DECREF(instance_glue->impl);
	Py_DECREF(instance_glue->servant);
	Py_DECREF(self);

	ORBIT_SERVANT_SET_CLASSINFO ((PortableServer_ServantBase *)instance_glue,
                                     NULL);

	CORBA_free(instance_glue->oid);

	d_message(1, "POA_PyObject__deactivate_object: deactivated %x", 
	          (guint)instance);
	Py_INCREF(Py_None);
	return Py_None;
}

/* Private */

static PyObject *
POA_PyObject__activate_object (POA_PyObject * self,
			       PyObject *     args)
{
	PortableServer_ObjectId *id;
	PyObject                *instance; /* servant */
	Servant_PyClass_Glue    *class_glue;
	Servant_PyInstance_Glue *instance_glue;

	d_message (5, "%s: entered", __FUNCTION__);
	g_return_if_fail (self != NULL);

	if (!PyArg_ParseTuple (args, "O:activate_object", &instance)) {
		return NULL;
	}

	d_message (1, "activating %x", (guint)instance);

	/* see if the instance_glue for this instance exists already
	 * (i.e. if delegation was used)
	 */
	instance_glue = g_hash_table_lookup (servant_instance_glue, instance);
	if (!instance_glue) {
		class_glue = (Servant_PyClass_Glue *)
		                  get_class_glue_from_instance(instance);
		if (!class_glue) {
			return raise_system_exception (OPExc_BAD_PARAM,
						       0,
						       CORBA_COMPLETED_NO,
						       "object not a servant");
		}
		
		instance_glue = ORBit_Python_init_pserver (class_glue, instance);
		if (!instance_glue) {
			g_message ("Error, instance_glue is NULL");
		}
	}
	else if (instance_glue->activated == CORBA_TRUE) {
		return raise_system_exception (OPExc_BAD_INV_ORDER, 0, CORBA_COMPLETED_NO, 
		                              "servant already activated");
	}
	
	d_message (4, "i'm going to activate right now");
	id = PortableServer_POA_activate_object (self->obj,
						 instance_glue,
						 &self->ev);
	
	if (!check_corba_ex (&self->ev)) {
		return NULL;
	}

	Py_INCREF (instance_glue->impl);
	instance_glue->activated = CORBA_TRUE;
	instance_glue->poa = self;
	Py_INCREF (self);
	instance_glue->oid = id;
	Py_INCREF (instance_glue->servant);
	
	return Py_BuildValue("s#", id->_buffer, id->_length);

}

static PyObject *
POA_PyObject__activate_object_with_id (POA_PyObject * self,
				       PyObject *     args)
{
	PyObject *instance;
	Servant_PyClass_Glue *class_glue;
	Servant_PyInstance_Glue *instance_glue;
	PortableServer_ObjectId *id;

	g_return_if_fail (self != NULL);

	id = (PortableServer_ObjectId *)CORBA_sequence_CORBA_octet__alloc();
	if (!PyArg_ParseTuple(args, "s#O:activate_object_with_id",
			      &id->_buffer, &id->_length, &instance)) {
		ORBit_free(id);
		return NULL;
	}
	
	id->_length++; /* oid length includes NULL */

	/* see if the instance_glue for this instance exists already
	 * (i.e. if delegation was used)
	 */
	instance_glue = g_hash_table_lookup(servant_instance_glue, instance);
	if (!instance_glue) {
		class_glue = (Servant_PyClass_Glue *)
		                  get_class_glue_from_instance(instance);
		if (!class_glue) {
			return raise_system_exception (OPExc_BAD_PARAM, 0, CORBA_COMPLETED_NO,
						       "object not a servant");
		}
		
		instance_glue = ORBit_Python_init_pserver (class_glue, instance);
	} else if (instance_glue->activated == CORBA_TRUE) {
		return raise_system_exception(OPExc_BAD_INV_ORDER, 0, CORBA_COMPLETED_NO, 
		                              "servant already activated");
	}

	PortableServer_POA_activate_object_with_id (self->obj,
						    id,
						    instance_glue,
	                                           &self->ev);
	ORBit_free(id);

	if (!check_corba_ex (&self->ev)) {
		return NULL;
	}

	Py_INCREF(instance_glue->impl);
	instance_glue->activated = CORBA_TRUE;
	instance_glue->poa = self;
	Py_INCREF(self);
	
	instance_glue->oid = id;
	Py_INCREF(instance_glue->servant);

	Py_INCREF(Py_None);
	return Py_None;
}


static PyObject *
POA_PyObject__reference_to_servant (POA_PyObject * self,
				    PyObject *     args)
{
	CORBA_Object obj;
	Servant_PyInstance_Glue *instance_glue;
	PyObject *ref;
	CORBA_PyInstance_Glue *objglue;

	g_return_if_fail (self != NULL);

	if (!PyArg_ParseTuple(args, "O", &ref))
		return NULL;

	objglue = g_hash_table_lookup(object_instance_glue, ref);
	if (!objglue)
		return raise_system_exception(OPExc_BAD_PARAM, 0, CORBA_COMPLETED_NO,
                                    "parameter 1 not a CORBA object");

	obj = (CORBA_Object)PortableServer_POA_reference_to_servant(self->obj, 
			              objglue->obj, &self->ev);
	if (!check_corba_ex(&self->ev))
		return NULL;
	instance_glue = (Servant_PyInstance_Glue *)obj;
	if (!instance_glue) {
		Py_INCREF(Py_None);
		return Py_None;
	}
	Py_INCREF(instance_glue->servant);
	return instance_glue->servant;
}

static PyObject *
POA_PyObject__get_the_POAManager(POA_PyObject *self, PyObject *args)
{
	PortableServer_POAManager m;

	g_return_if_fail (self != NULL);

	m = PortableServer_POA__get_the_POAManager(self->obj, &self->ev);
	if (!check_corba_ex(&self->ev))
		return NULL;
	if (m == CORBA_OBJECT_NIL) {
		Py_INCREF(Py_None);
		return Py_None;
	}
	return (PyObject *)POAManager_Object_to_PyObject(m);
}

static void
POA_PyObject__dealloc(POA_PyObject *self)
{
	g_return_if_fail (self != NULL);

	d_message(1, "POA_PyObject__dealloc: entered");

	Py_DECREF(self->orb);
	CORBA_Object_release((CORBA_Object)self->obj, &self->ev);
	CORBA_exception_free(&self->ev);
	PyMem_DEL(self);
}


static PyObject *
POA_PyObject__getattr (POA_PyObject *self, char *name)
{
	g_return_if_fail (self != NULL);

	if (!strcmp(name, "the_POAManager")) {
		name = "_get_the_POAManager";
	}

	return Py_FindMethod(POA_PyObject_methods, (PyObject *)self, name);
}

static int
POA_PyObject__setattr (POA_PyObject * self,
		       char *         name,
		       PyObject *     v)
{
	g_return_if_fail (self != NULL);
	
	return -1;
}

/* Local Variables: */
/* c-file-style: "python" */
/* End: */
