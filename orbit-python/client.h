/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Johan Dahlin
 *
 */

#ifndef __CLIENT_H__
#define __CLIENT_H__

#include "orbit-python-types.h"

GPtrArray *      marshal_call   (CORBA_Object                  obj,
				 GIOPConnection *              con,
				 GIOPMessageQueueEntry *       mqe,
				 CORBA_unsigned_long           req_id,
				 CORBA_OperationDescription *  opd,
				 PyObject *                    args);

GIOPConnection * demarshal_call (CORBA_Object                  obj,
				 GIOPConnection **             con,
				 GIOPMessageQueueEntry *       mqe,
				 CORBA_unsigned_long           req_id,
				 CORBA_OperationDescription *  opd,
				 PyObject *                    args,
				 GPtrArray *                   return_types,
				 PyObject **                   tuple);

PyObject *       _stub_func     (CORBA_Object                  obj,
				 PyObject *                    args,
				 CORBA_OperationDescription *  opd);

PyObject *       get_attribute  (CORBA_PyInstance_Glue *       glue, 
				 CORBA_AttributeDescription *  ad);

PyObject *       set_attribute  (CORBA_PyInstance_Glue *       glue, 
				 CORBA_AttributeDescription *  ad,
				 PyObject *                    value);

#endif /* __CLIENT_H__ */

