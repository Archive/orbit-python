/*  -* - Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -* - * /
/* 
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 * 
 * Author: Johan Dahlin
 * 
 */

#ifndef __EXCEPTIONS_H__
#define __EXCEPTIONS_H__

#include <Python.h>
#include <orbit/orbit-types.h>
#include <orbit/orb-core/corba-environment.h>
#include <glib.h>

PyObject *     UserExcept_PyClass__init     (PyObject * _,
					     PyObject * args,
					     PyObject * keys);

PyObject *     UserExcept_PyClass__str      (PyObject * _,
					     PyObject * args,
					     PyObject * keys);

CORBA_boolean  check_corba_ex               (CORBA_Environment *);

void           raise_user_exception         (char *,
					     PyObject *);

void *         raise_system_exception       (PyObject *,
					     CORBA_unsigned_long, 
					     CORBA_completion_status,
					     char *,
					     ...);

void           ORBit_Python_init_exceptions (PyObject *ModuleDict);

extern GHashTable * exceptions;
extern PyObject * OPExc_Exception;
extern PyObject * OPExc_SystemException;
extern PyObject * OPExc_UserException;
extern PyObject * OPExc_UNKNOWN;
extern PyObject * OPExc_BAD_PARAM;
extern PyObject * OPExc_NO_MEMORY;
extern PyObject * OPExc_IMP_LIMIT;
extern PyObject * OPExc_COMM_FAILURE;
extern PyObject * OPExc_INV_OBJREF;
extern PyObject * OPExc_OBJECT_NOT_EXIST;
extern PyObject * OPExc_NO_PERMISSION;
extern PyObject * OPExc_INTERNAL;
extern PyObject * OPExc_MARSHAL;
extern PyObject * OPExc_INITIALIZE;
extern PyObject * OPExc_NO_IMPLEMENT;
extern PyObject * OPExc_BAD_TYPECODE;
extern PyObject * OPExc_BAD_OPERATION;
extern PyObject * OPExc_NO_RESOURCES;
extern PyObject * OPExc_NO_RESPONSE;
extern PyObject * OPExc_PERSIST_STORE;
extern PyObject * OPExc_BAD_INV_ORDER;
extern PyObject * OPExc_TRANSIENT;
extern PyObject * OPExc_FREE_MEM;
extern PyObject * OPExc_INV_IDENT;
extern PyObject * OPExc_INV_FLAG;
extern PyObject * OPExc_INTF_REPOS;
extern PyObject * OPExc_BAD_CONTEXT;
extern PyObject * OPExc_OBJ_ADAPTER;
extern PyObject * OPExc_DATA_CONVERSION;
extern PyObject * OPExc_TRANSACTION_REQUIRED;
extern PyObject * OPExc_TRANSACTION_ROLLEDBACK;
extern PyObject * OPExc_INVALID_TRANSACTION;
extern PyObject * OPExc_WRONG_TRANSACTION;

#endif /* #define __EXCEPTIONS_H__ */



