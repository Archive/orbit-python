/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Johan Dahlin
 *
 */

#ifndef __SERVER_H__
#define __SERVER_H__

#include "orbit-python-types.h"

/* hash on Python Class Object addresses; store Servant_PyClass_Glue */
extern GHashTable *servant_class_glue;
/* hash on Python Instance Object addresses; store Servant_PyInterface_Glue */
extern GHashTable *servant_instance_glue;

void                         ORBit_Python_init_server     (void);
Servant_PyInstance_Glue *    ORBit_Python_init_pserver    (Servant_PyClass_Glue *,
							   PyObject *);

CORBA_AttributeDescription * find_attribute               (CORBA_PyClass_Glue *glue,
							   char *n);

CORBA_OperationDescription * find_operation               (CORBA_PyClass_Glue *glue,
							   char *n);

ORBitSkeleton                get_skel                     (Servant_PyInstance_Glue *self,
							   GIOPRecvBuffer *buf,
							   gpointer *impl);

Servant_PyClass_Glue *       get_class_glue_from_instance  (PyObject *);

PyObject *                   Servant_PyClass__init        (PyObject *_,
							   PyObject *,
							   PyObject *);

PyObject *                   Servant_PyClass__del         (PyObject *_,
							   PyObject *,
							   PyObject *);

PyObject *                   Servant_PyClass__this        (PyObject *_,
							   PyObject *,
							   PyObject *);

PyObject *                   Servant_PyClass__default_POA (PyObject *_,
							   PyObject *,
							   PyObject *);


#endif /* __SERVER_H__ */

