/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 *
 *	Wraps CORBA_ORB
 *	Methods handled:
 *		object_to_string
 *		string_to_object
 *		run
 */

#include "corba-orb.h"

#include "corba-object.h"
#include "exceptions.h"
#include "orbit-python-types.h"
#include "poa-object.h"

#include <signal.h>

/* Private methods */
static void              CORBA_ORB_PyObject__dealloc                    (CORBA_ORB_PyObject *self);

static PyObject *        CORBA_ORB_PyObject__object_to_string           (CORBA_ORB_PyObject *self, 
									 PyObject           *args);

static PyObject *        CORBA_ORB_PyObject__string_to_object           (CORBA_ORB_PyObject *self, 
									 PyObject           *args);

static PyObject *        CORBA_ORB_PyObject__shutdown                   (CORBA_ORB_PyObject *self,
									 PyObject           *args);

static PyObject *        CORBA_ORB_PyObject__run                        (CORBA_ORB_PyObject *self,
									 PyObject           *args);

static int               CORBA_ORB_PyObject__setattr                    (CORBA_ORB_PyObject *self,
									 char               *attr,
									 PyObject           *obj);

static PyObject *        CORBA_ORB_PyObject__getattr                    (CORBA_ORB_PyObject *self,
									 char               *attr);

static PyMethodDef
CORBA_ORB_PyObject_methods[] = {
	{ "object_to_string", (PyCFunction)CORBA_ORB_PyObject__object_to_string, 1 },
	{ "string_to_object", (PyCFunction)CORBA_ORB_PyObject__string_to_object, 1 },
	{ "resolve_initial_references", 
	                      (PyCFunction)CORBA_ORB_PyObject__resolve_initial_references, 1 },
	{ "run",              (PyCFunction)CORBA_ORB_PyObject__run,              1},
	{ "shutdown",         (PyCFunction)CORBA_ORB_PyObject__shutdown,         1},
	{ NULL, NULL }
};

POA_PyObject *root_poa;
GHashTable *orb_objects;

PyTypeObject CORBA_ORB_PyObject_Type = {
	PyObject_HEAD_INIT(NULL)
	0,                                        /*ob_size*/
	"CORBA.ORB",                              /*tp_name*/
	sizeof(CORBA_ORB_PyObject),               /*tp_basicsize*/
	0,                                        /*tp_itemsize*/
	
	/* methods */
	(destructor)CORBA_ORB_PyObject__dealloc,  /*tp_dealloc*/
	0,                                        /*tp_print*/
	(getattrfunc)CORBA_ORB_PyObject__getattr, /*tp_getattr*/
	(setattrfunc)CORBA_ORB_PyObject__setattr, /*tp_setattr*/
	0,                                        /*tp_compare*/
	0,                                        /*tp_repr*/
	0,                                        /*tp_as_number*/
	0,                                        /*tp_as_sequence*/
	0,                                        /*tp_as_mapping*/
	0,                                        /*tp_hash*/
};

/* Public functions */

CORBA_ORB_PyObject *
CORBA_ORB_PyObject__new (CORBA_ORB orb)
{
	CORBA_ORB_PyObject *self;

	self = g_hash_table_lookup (orb_objects, orb);
	if (self) {
		Py_INCREF(self);
		return self;
	}

	self = PyObject_NEW (CORBA_ORB_PyObject,
			     &CORBA_ORB_PyObject_Type);
	if (!self) {
		return NULL;
	}

	CORBA_exception_init (&self->ev);

	self->obj = orb;
	g_hash_table_insert (orb_objects, orb, self);
	
	return self;
}

PyObject *
CORBA_ORB_PyObject__resolve_initial_references (CORBA_ORB_PyObject *self,
						PyObject           *args)
{
	CORBA_Object res;
	char *s;

	if (!PyArg_ParseTuple (args, "s:resolve_initial_references", &s)) {
		return NULL;
	}

	/* workaround for ORBit bug
	 * http://bugzilla.gnome.org/show_bug.cgi?id=55726
	 * FIXME: should raise CORBA.ORB.InvalidName
	 */
	
	if (strcmp (s, "RootPOA") &&
	    strcmp (s, "NameService") &&
	    strcmp (s, "ImplementationRepository") && 
	    strcmp (s, "InterfaceRepository")) {
		return raise_system_exception (OPExc_BAD_PARAM,
					       0,
					       CORBA_COMPLETED_NO, 
		                              "Invalid name");
	}

	res = CORBA_ORB_resolve_initial_references (self->obj,
						    s,
						    &self->ev);
	if (!check_corba_ex (&self->ev)) {
		return NULL;
	}
	
	if (!strcmp (s, "RootPOA")) {
		if (!root_poa) {
			root_poa = POA_Object_to_PyObject ((PortableServer_POA)res);
		}
		
		Py_INCREF (root_poa);
		return (PyObject *)root_poa;
	}
	
	return ((PyObject *)CORBA_Object_to_PyObject (res));
}

/* Private methods */

static void
CORBA_ORB_PyObject__dealloc (CORBA_ORB_PyObject *self)
{
	CORBA_Object_release ((CORBA_Object)self->obj, &self->ev);
	CORBA_exception_free (&self->ev);
	g_hash_table_remove (orb_objects, self->obj);
	PyMem_DEL (self);
}

static PyObject *
CORBA_ORB_PyObject__object_to_string (CORBA_ORB_PyObject *self, 
				      PyObject           *args)
{
	PyObject              *obj;
	PyObject              *strobj = NULL;
	CORBA_PyInstance_Glue *glue;
	CORBA_Object           corba_obj;
	CORBA_char            *s;

	if (!PyArg_ParseTuple (args, "O:object_to_string", &obj)) {
		return NULL;
	}

	/* check to see if it's a known objref */
	glue = g_hash_table_lookup (object_instance_glue, obj);
	if (!glue) {
		raise_system_exception (OPExc_BAD_PARAM,
					0,
					CORBA_COMPLETED_NO,
					NULL);
		return NULL;
	}
	
	corba_obj = glue->obj;

	s = CORBA_ORB_object_to_string (self->obj,
					corba_obj,
					&self->ev);
	
	if (check_corba_ex (&self->ev)) {
		strobj = PyString_FromString (s);
	}
	
	if (s) {
 		CORBA_free (s);
	}

	return strobj;
}

static PyObject *
CORBA_ORB_PyObject__string_to_object (CORBA_ORB_PyObject *self, 
				      PyObject           *args)
{
	CORBA_Object obj;
	char *s, *p;
	
	if (!PyArg_ParseTuple(args, "s:string_to_object", &s)) {
		return NULL;
	}
	
	/* Remove traililng whitespace from IOR */
	p = s + strlen (s) - 1;
	while (isspace (*p)) {
		*p-- = 0;
	}

	obj = CORBA_ORB_string_to_object (self->obj, s, &self->ev);
	
	if (!check_corba_ex (&self->ev)) {
		return NULL;
	}
	
	if (obj == CORBA_OBJECT_NIL) {
		Py_INCREF (Py_None);
		return Py_None;
	}
	
	return ((PyObject *)CORBA_Object_to_PyObject (obj));
}

static PyObject *
CORBA_ORB_PyObject__run (CORBA_ORB_PyObject *self,
			 PyObject           *args)
{
	void (*oldsig)(int);

	if (!PyArg_ParseTuple (args, ":run")) {
		return NULL;
	}

	/* Make ctrl-c work while the ORB is running. */
	oldsig = signal (SIGINT, SIG_DFL);

	CORBA_ORB_run (self->obj, &self->ev);
	
	signal (SIGINT, oldsig);
	
	if (!check_corba_ex (&self->ev)) {
		return NULL;
	}

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *
CORBA_ORB_PyObject__shutdown (CORBA_ORB_PyObject *self,
			      PyObject           *args)
{
	gboolean wait;
	
	if (!PyArg_ParseTuple (args, "h:shutdown", &wait)) {
		return NULL;
	}
	
	/* FIXME: probably don't allow this to be called unless the ORB is really
	 * running?  Seems to cause segfaults.
	 */
	
	CORBA_ORB_shutdown (self->obj,
			    (CORBA_boolean)wait,
			    &self->ev);
	
	if (!check_corba_ex (&self->ev)) {
		return NULL;
	}
	
	Py_INCREF (Py_None);
	return Py_None;
}

static PyObject *
CORBA_ORB_PyObject__getattr (CORBA_ORB_PyObject *self,
			     char               *name)
{
	return Py_FindMethod (CORBA_ORB_PyObject_methods,
			      (PyObject *)self,
			      name);
}

static int
CORBA_ORB_PyObject__setattr (CORBA_ORB_PyObject *self,
			     char               *name,
			     PyObject           *v)
{
	return -1;
}

/* Local Variables: */
/* c-file-style: "python" */
/* End: */
