/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000,2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2001,2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include "server.h"

#include "corba-object.h"
#include "corba-orb.h"
#include "demarshal.h"
#include "exceptions.h"
#include "orbit-python-types.h"
#include "poa-object.h"
#include "marshal.h"
#include "util.h"

GHashTable *servant_class_glue, *servant_instance_glue;

/* private functions */
static void                 operation_skel            (Servant_PyInstance_Glue *    servant,
						       GIOPRecvBuffer *             buf,
						       CORBA_Environment *          ev,
						       gpointer                     impl);

static CORBA_exception_type marshal_current_exception (GIOPSendBuffer *             sb,
						       CORBA_OperationDescription * opd);

static void                 hash_get_value            (gpointer key,
						       gpointer value,
						       gpointer data);

void
ORBit_Python_init_server (void)
{
	servant_instance_glue = g_hash_table_new(g_direct_hash, g_direct_equal);
	servant_class_glue = g_hash_table_new(g_direct_hash, g_direct_equal);
}

CORBA_OperationDescription *
find_operation (CORBA_PyClass_Glue * glue,
		char *               n)
{
	CORBA_unsigned_long i;
	InterfaceDef_Full * d;

	d_message (4, "%s (glue=0x%x, n=\"%s\")",
		   __FUNCTION__, (int)glue, n);
	
	for (i = 0; i < glue->desc->operations._length; i++) {
		if (!strcmp(n, glue->desc->operations._buffer[i].name)) {
			d_message (4, "find_operation returns %s", n);
			return &glue->desc->operations._buffer[i];
		}
	}
	
	/* Look through base interfaces for this operation */
	d = glue->desc;
	for (i = 0; i < d->base_interfaces._length; i++) {
		char *repo_id = d->base_interfaces._buffer[i];
		glue = (CORBA_PyClass_Glue *) g_hash_table_lookup(object_glue, repo_id);
		if (glue) {
			CORBA_OperationDescription *res = find_operation(glue, n);
			if (res)
				return res;
		}
	}
	return NULL;
}

CORBA_AttributeDescription *
find_attribute (CORBA_PyClass_Glue *glue, char *n)
{
	CORBA_unsigned_long i;
	InterfaceDef_Full * d;

	d_message (5, "%s (glue=0x%x, n=\"%s\")",
		   __FUNCTION__, (int)glue, n);
	
	for (i = 0; i < glue->desc->attributes._length; i++) {
		if (!strcmp(n, glue->desc->attributes._buffer[i].name))
			return &glue->desc->attributes._buffer[i];
	}
	
	/* Look through base interfaces for this attribute */
	d = glue->desc;
	for (i = 0; i < d->base_interfaces._length; i++) {
		char *repo_id = d->base_interfaces._buffer[i];
		glue = (CORBA_PyClass_Glue *)g_hash_table_lookup(object_glue, repo_id);
		if (glue) {
			CORBA_AttributeDescription *res = find_attribute(glue, n);
			if (res)
				return res;
		}
	}
	return NULL;
}

static CORBA_exception_type
marshal_current_exception (GIOPSendBuffer *sb,
			   CORBA_OperationDescription *opd)
{
	PyObject *type, *val, *tb;
	CORBA_exception_type ex_type;

	PyErr_Fetch(&type, &val, &tb);
	ex_type = marshal_exception(type, val, sb, NULL, opd);

	if (!PyObject_HasAttrString(type, "__repo_id")) {
		PyErr_Restore(type, val, tb);
	} else {
		/* TODO: print system exceptions */
		Py_XDECREF(type);
		Py_XDECREF(val);
		Py_XDECREF(tb);
	}
	
	/* FIXME: handle version 0 & 1 */
	if (sb->msg.header.version[1] == 2) {
		sb->msg.u.reply_1_2.reply_status = (CORBA_unsigned_long)ex_type;
	}

	return ex_type;
}

static void
operation_skel (Servant_PyInstance_Glue *servant,
		GIOPRecvBuffer *buf,
		CORBA_Environment *ev,
		gpointer impl)
{
	GIOPSendBuffer *sb = NULL;
	CORBA_OperationDescription *opd = (CORBA_OperationDescription *)impl;
	CORBA_unsigned_long i, cur = 0;
	PyObject *result = NULL, *tuple, *dict, *meth = NULL;
	CORBA_boolean is_attr_op = CORBA_FALSE;
	CORBA_ORB_PyObject *orb;
	int num_return_values = 0;

	d_assert_noval(servant != NULL);
	
	orb = servant->poa->orb;

	/* Figure out how many parameters we're passing to the python function */
	for (i = 0, cur = 0; i < opd->parameters._length; i++) {
		if (opd->parameters._buffer[i].mode != CORBA_PARAM_OUT) {
			cur++;
		}
		
		/* While we're here, determine number of OUT and INOUT params for
		 * the return value
		 */
		if (opd->parameters._buffer[i].mode != CORBA_PARAM_IN) {
			num_return_values++;
		}
	}
	
	if (opd->result->kind != CORBA_tk_void) {
		num_return_values++;
	}

	/* Construct the parameter list to pass to the python method */
	tuple = PyTuple_New(cur);
	for (i = 0, cur = 0; i < opd->parameters._length; i++) {
		if (opd->parameters._buffer[i].mode != CORBA_PARAM_OUT) {
			PyObject *arg;
			arg = demarshal_arg(buf, opd->parameters._buffer[i].type, orb->obj);
			PyTuple_SetItem(tuple, cur++, arg);
		}
	}
	
	/* Find instance's attributes */
	dict = PyObject_GetAttrString(servant->impl,"__dict__");
	if (PyObject_HasAttrString (servant->impl, opd->name)) {
		meth = PyObject_GetAttrString(servant->impl, opd->name);
	}
	
	if (!meth || !PyCallable_Check (meth)) {
		/* Fetch an attribute */
		if (!strncmp(opd->name, "_get_", 5)) {
			d_message(3, "operation_skel: Handling _get_attribute");
			is_attr_op = CORBA_TRUE;

			/* At this point we know the servant hasn't implemented
			 * _get_<attrname>, so check to see if __getattr__ is implemented,
			 * and call that.
			 */
			if (PyObject_HasAttrString(servant->impl, "__getattr__")) {
				PyObject *t;
				d_message(3, "operation_skel: Calling servant __getattr__");
				
				/* Construct a new temporary tuple to pass to __getattr__ */
				t = PyTuple_New(1);
				PyTuple_SetItem(t, 0, PyString_FromString(&opd->name[5]));
				result = PyObject_CallMethod(servant->impl, "__getattr__", "O", t);
				
				/* free the temp tuple */
				Py_DECREF(t);
			} else {
				/* __getattr__ isn't implemented, so check the dictionary */
				result = PyDict_GetItemString(dict, &opd->name[5]);
				if (result)
					Py_INCREF(result);
			}

			if (!result) {
				raise_system_exception(OPExc_INTERNAL, 0, CORBA_COMPLETED_NO,
				                       "Attribute not yet defined");
			}

		/* set an attribute */
		} else if (!strncmp(opd->name, "_set_", 5)) {
			PyObject *value;
			d_message(3, "operation_skel: Handling _set_attribute");
			is_attr_op = CORBA_TRUE;
			value = PyTuple_GetItem(tuple, 0);

			/* At this point we know the servant hasn't implemented
			 * _set_<attrname>, so check to see if __setattr__ is implemented,
			 * and call that.
			 */
			if (PyObject_HasAttrString(servant->impl, "__setattr__")) {
				PyObject *t;
				d_message(3, "operation_skel: Calling servant __setattr__");
				/* Construct a new temporary tuple to pass to __setattr__ */
				t = PyTuple_New(2);
				PyTuple_SetItem(t, 0, PyString_FromString(&opd->name[5]));
				Py_INCREF(value);
				PyTuple_SetItem(t, 1, value);
				result = PyObject_CallMethod(servant->impl, "__setattr__", "O", t);
				Py_DECREF(result);
				/* free the temp tuple */
				Py_DECREF(t);
			} else {
				/* __setattr__ isn't implemented, so check the dictionary */
				d_message(5, "Setting attribute %s to %x", &opd->name[5],
				             (guint)value);
				PyDict_SetItemString(dict, &opd->name[5], value);
			}

			Py_INCREF(Py_None);
			result = Py_None;
		/* The servant does not implement the requested operation */
		} else {
			raise_system_exception(OPExc_BAD_OPERATION, 0, CORBA_COMPLETED_NO,
			                       "Servant does not implement operation");
		}
	} else {
		/* Normal, existing method call
		 * (possibly an overloaded _get_* / _set_*)
		 */
		d_message(3, "operation_skel: Invoking PyObject_CallMethod");
		result = PyObject_CallMethod(servant->impl, opd->name, "O", tuple);
		Py_DECREF(meth);
	}
	
	/* Free the tuple used for the operation's argument list */
	Py_DECREF(tuple);
	
	/* We don't bail if PyErr_Occurred because we need to marshal the
	 * exception to the client
	 */
	if (opd->mode == CORBA_OP_ONEWAY && !PyErr_Occurred())
		goto bail;

	/* reset_sendbuf allocates a new GIOPSendBuffer and frees the existing
	 * send buffer (if one is allocated).  This is useful if we start
	 * marshalling return values to the client but run into an exception
	 * along the way.
	 */
	#define reset_sendbuf()                                                            \
		if (sb) {                                                                  \
		    giop_send_buffer_unuse (sb);                                           \
		}                                                                          \
		sb = giop_send_buffer_use_reply (buf->giop_version,                        \
						 giop_recv_buffer_get_request_id (buf),    \
						 (CORBA_unsigned_long)CORBA_NO_EXCEPTION)
	
	reset_sendbuf();
	/* Check to see if the method raised an exception */
	if (PyErr_Occurred()) {
		marshal_current_exception(sb, opd);
		goto send;
	}

	/* Verify the case where the IDL operation takes either 0 or 1 return
	 * values.  If it takes more than 1 value, we make sure the result
	 * object is at least a tuple or list but we don't validate the
	 * number of elements in this case because things get hairy (i.e.
	 * depends on context)
	 */
	if ( !PyTuple_Check(result) && !PyList_Check(result) && 
	      num_return_values > 1)  {
		raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_MAYBE,
		                       "Operation did not return expected number of "
		                       "arguments (%d)", num_return_values);
	}
		
	/* Marshal the IDL operation return value */
	cur = 0;
	if (opd->result->kind != CORBA_tk_void) {
		PyObject *arg;
		CORBA_boolean success;

		if (num_return_values == 1) {
			arg = result;
			Py_INCREF(arg);
		} else
			arg = PySequence_GetItem(result, cur++);
		success = marshal_arg(arg, sb, opd->result);
		Py_DECREF(arg);
		if (!success) {
			g_message("Error marshalling return value -- bad type?");
			reset_sendbuf();
			marshal_current_exception(sb, opd);
			goto send;
		}
	}

	/* Marshal the out/inout parameters for the operation
	 *   cur is used for the index of the result tuple
	 *   i is used for the index of the operation description
	 */
	for (i = 0; i < opd->parameters._length; i++) {
		PyObject *arg;
		CORBA_boolean success;
		CORBA_ParameterDescription *pd = &opd->parameters._buffer[i];
		if (pd->mode != CORBA_PARAM_OUT && pd->mode != CORBA_PARAM_INOUT)
			continue;

		if (num_return_values == 1) {
			arg = result;
			Py_INCREF(arg); /* take a new reference because we DECREF it later */
		}
		else if (cur == PySequence_Length(result)) {
			raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_MAYBE,
		   	                    "Operation did not return expected number of "
		      	                 "arguments (%d)", num_return_values);
			reset_sendbuf();
			marshal_current_exception(sb, opd);
			goto send;
		} else
			arg = PySequence_GetItem(result, cur++);

		success = marshal_arg(arg, sb, pd->type);
		Py_DECREF(arg);
		if (!success) {
			g_message("Error marshalling out/inout param -- bad type?");
			reset_sendbuf();
			marshal_current_exception(sb, opd);
			goto send;
		}
	}

send:
	giop_send_buffer_write(sb, buf->connection, FALSE);
		
bail:

	Py_XDECREF(dict);

	if (PyErr_Occurred()) {
		PyErr_Print();
		PyErr_Clear();
	}

	if (sb) {
		giop_send_buffer_unuse(sb);
		sb = NULL;
	}
	if (result) {
		Py_DECREF(result);
	}
}
      

ORBitSkeleton
get_skel (Servant_PyInstance_Glue *self,
	  GIOPRecvBuffer *buf,
	  gpointer *impl)
{
	gchar *opname;
	CORBA_OperationDescription *opd;
	
	g_return_val_if_fail (buf != NULL, NULL);

	opname = giop_recv_buffer_get_opname(buf);

	g_return_val_if_fail (opname != NULL, NULL);
	
	opd = find_operation(self->class_glue->interface_glue, opname);
	if (!opd) {
		char *attr;
		CORBA_AttributeDescription *ad;
		/* Fetch the description for this attribute */
		attr = &opname[5];
		ad = find_attribute(self->class_glue->interface_glue, attr);
		/* We abort if this attribute doesn't exist. */
		if (!ad) {
			return (ORBitSkeleton)NULL;
		}
	}
	*impl = opd;
	
	return (ORBitSkeleton)operation_skel;
}

Servant_PyInstance_Glue *
ORBit_Python_init_pserver (Servant_PyClass_Glue *class_glue, 
			   PyObject *instance)
{
	Servant_PyInstance_Glue *instance_glue;
	CORBA_Environment ev;
	PortableServer_ServantBase__epv *base_epv, *vepv;

	instance_glue = g_new0(Servant_PyInstance_Glue, 1);
	instance_glue->activated = CORBA_FALSE;
		
	/* initialize to NULL */
	base_epv = g_new0(PortableServer_ServantBase__epv, 1);
	instance_glue->vepv  = g_new0(PortableServer_ServantBase__vepv, 1);
	vepv = (PortableServer_ServantBase__epv *)instance_glue->vepv;
	vepv->_private = base_epv;

	CORBA_exception_init(&ev);

	PortableServer_ServantBase__init ((PortableServer_ServantBase *)instance_glue, &ev);

	if (!check_corba_ex(&ev)) {
		g_message ("Exception in PortableServer_ServantBase__init, %s", ev._id);
		return NULL;
	}
	
	CORBA_exception_free(&ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		d_error("Exception while initializing servant");
	}

	ORBIT_SERVANT_SET_CLASSINFO ((PortableServer_ServantBase *)instance_glue,
                                     class_glue->class_info);

	instance_glue->impl = instance;
	instance_glue->servant = instance;
	instance_glue->class_glue = class_glue;
	g_hash_table_insert(servant_instance_glue, instance, instance_glue);

	d_message(1, "ORBit_Python_init_pserver: created new servant at %x", 
	          (guint)instance_glue);
	return instance_glue;
}

Servant_PyClass_Glue *
get_class_glue_from_class (PyObject *class_object)
{
	Servant_PyClass_Glue *class_glue = NULL;
	PyObject *bases;
	int i;

	class_glue = g_hash_table_lookup(servant_class_glue, class_object);
	if (class_glue) {
		return class_glue;
	}
	
	if (!PyObject_HasAttrString(class_object, "__bases__")) {
		return NULL;
	}
	
	bases = PyObject_GetAttrString(class_object,"__bases__");
	for (i = 0; i < PyTuple_Size(bases); i++) {
		PyObject *cl = PyTuple_GetItem(bases, i);
		class_glue = get_class_glue_from_class(cl);
		if (class_glue) {
			break;
		}
	}
	
	Py_XDECREF(bases);
	return class_glue;
}

Servant_PyClass_Glue *
get_class_glue_from_instance (PyObject *instance)
{
	PyObject *class_object;
	Servant_PyClass_Glue *class_glue = NULL;

	if (!PyObject_HasAttrString(instance, "__class__")) {
		g_message ("Error, the object in class_glue does not have a __class__ attribute");
		return NULL;
	}
	class_object = PyObject_GetAttrString(instance, "__class__");
	class_glue = get_class_glue_from_class(class_object);
/*	assert (class_glue == NULL); */
	
	Py_XDECREF(class_object);
	return class_glue;
}

PyObject *
Servant_PyClass__init(PyObject *_, PyObject *args, PyObject *keys)
{
	if (PyTuple_Size(args) > 1) {
		PyObject *self = PyTuple_GetItem(args, 0);
		PyObject *impl = PyTuple_GetItem(args, 1);
		Servant_PyClass_Glue *class_glue;
		d_message(1, "Servant using delegation with %x %x", 
		              (guint)self, (guint)impl);
		class_glue = get_class_glue_from_instance(self);
		if (class_glue) {
			Servant_PyInstance_Glue *iglue = ORBit_Python_init_pserver(class_glue,
			                                                           self);
			iglue->impl = impl;
			Py_INCREF(iglue->impl);
		} else {
			d_warning(0, "Couldn't find class glue to init server delegate");
		}
	}
	Py_INCREF(Py_None);
	return Py_None;
}

PyObject *
Servant_PyClass__del(PyObject *_, PyObject *args, PyObject *keys)
{
	CORBA_Environment ev;
	PyObject *self = PyTuple_GetItem(args, 0);
	Servant_PyInstance_Glue *instance_glue;
	d_message(1, "internal Servant class destructor: %x", (guint)self);

	instance_glue = g_hash_table_lookup(servant_instance_glue, self);
	if (!instance_glue) {
		goto bail; /* object wasn't activated */
	}
	
	if (instance_glue->activated) {
		PyObject *args = PyTuple_New(1), *res;
		Py_INCREF(self);
		PyTuple_SetItem(args, 0, (PyObject *)self);
		res = (PyObject *)POA_PyObject__deactivate_object(instance_glue->poa, 
		                                                  args);
		Py_DECREF(res);
		Py_DECREF(args);
	}
	
	/* delegation used */
	if (self != instance_glue->impl) { 
		Py_DECREF(instance_glue->impl);
	}

	CORBA_exception_init(&ev);
	PortableServer_ServantBase__fini(
	                   (PortableServer_ServantBase *)instance_glue, &ev);
	CORBA_exception_free(&ev);
	g_hash_table_remove(servant_instance_glue, self);
	g_free( ((PortableServer_ServantBase__epv *)instance_glue->vepv)->_private);
	g_free(instance_glue->vepv);
	g_free(instance_glue);
bail:
	Py_INCREF(Py_None);
	return Py_None;
}

PyObject *
Servant_PyClass__this (PyObject *cl,
		       PyObject *args,
		       PyObject *keys)
{
	PyObject *self;
	PyObject *poa, *ref;

	d_message (5, "%s: entered", __FUNCTION__);
	
	self = PyTuple_GetItem(args, 0);
	poa = PyObject_CallMethod(self, "_default_POA", "", NULL);
	if (!poa) {
		g_error ("Couldn't call POA :(");
		return NULL; /* _default_POA raised exception */
	}
	
	ref = POA_PyObject__servant_to_reference ((POA_PyObject *)poa, args);
	
	Py_DECREF(poa);
	return ref;
}

static void
hash_get_value (gpointer key,
		gpointer value,
		gpointer data)
{
	*(CORBA_ORB_PyObject **)data = (CORBA_ORB_PyObject *)value;
}

PyObject *
Servant_PyClass__default_POA (PyObject *cl,
			      PyObject *args, 
			      PyObject *keys)
{
	if (!root_poa) {
		CORBA_ORB_PyObject *orb = NULL;
		PyObject *poa, *resolve_args;

		/* We need to get the RootPOA.  We'll pick any ol' ORB which
		 * should be able to get it for us.
		 */
		g_hash_table_foreach(orb_objects, hash_get_value, &orb);
		if (!orb) {
			/* No ORB was initialized. */
			raise_system_exception(OPExc_BAD_INV_ORDER, 0, CORBA_COMPLETED_NO,
			                       "ORB not initialized");
			return NULL;
		}
		resolve_args = Py_BuildValue("(s)", "RootPOA");
		poa = CORBA_ORB_PyObject__resolve_initial_references(orb, resolve_args);
		Py_DECREF(resolve_args);

		/* resolve_initial_references raised exception */
		if (!poa) { 
			g_message ("%s returning NULL", __FUNCTION__);
			return NULL;
		}
		Py_DECREF(poa);

		/* pedantic, should never happen! */
		d_assert(root_poa != NULL); 
	}

	Py_INCREF(root_poa);
	return (PyObject *)root_poa;
}

static int compare_object_interface(CORBA_TypeCode tc, PyObject *arg)
{
	CORBA_PyObject_Glue *glue;

	return 1;
	
	/* Allow nil objects. */
	if (arg == Py_None) {
		return 1;
	}

	/* Fail if arg is not a CORBA PyObject. */
	if (!is_corba_pyobject(arg)) {
		return 0;
	}
	
	/* Accept any object if the required value is a generic objref. */
	if (!strcmp(tc->repo_id, "")) {
		return 1;
	}

	d_message(2, "compare_object_interface entered for arg = %s",
		  ((CORBA_PyObject *)arg)->repo_id);

	glue = g_hash_table_lookup(object_glue, ((CORBA_PyObject *)arg)->repo_id);
	
	return compare_glue_interface(tc, glue);
}

int compare_glue_interface(CORBA_TypeCode tc, CORBA_PyObject_Glue *glue)
{
	CORBA_unsigned_long i;
	InterfaceDef_Full * d;

	d_message(2, "compare_glue_interface entered");

	/* This section is not needed if we only call this function somehow like
	 *     if (tc->kind == CORBA_tk_objref
	 *         && !compare_object_interface(opd->result, arg)) { ... }
	 * I leave it as comments, though, in case someone wishes to call it
	 * without testing the expected CORBA_TypeCode->kind

	 if (tc->kind != CORBA_tk_objref) {
	 d_message(2, "Returning 0 -- not supposed to be an object ref <%d>",
	 tc->kind);
	 return 0;
	 }
	*/

	d = glue->desc;

	d_message(5, "Expecting %s, trying %s", tc->repo_id, d->id);

	if(!strcmp(d->id, tc->repo_id)) {
		d_message(5, "Returning 1 -- OK, matches");
		return 1;
	}

	for (i = 0; i < d->base_interfaces._length; i++) {
		CORBA_PyObject_Glue *glue2;
		d_message(5, "Examining %s, expecting %s",
			  d->base_interfaces._buffer[i],
			  tc->repo_id);
		if(!strcmp(d->base_interfaces._buffer[i], tc->repo_id)) {
			d_message(5, "Returning 1 -- some base class matches");
			return 1;
		}
 		glue2 = (CORBA_PyObject_Glue *)g_hash_table_lookup(object_glue,
 								   d->base_interfaces._buffer[i]);
		if(compare_glue_interface(tc, glue2)) {
			return 1;
		}
	}

	d_message(5, "Returning 0 -- not matched");
	return 0;
}


/* Local Variables: */
/* c-file-style: "python" */
/* End: */
