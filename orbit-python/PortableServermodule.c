/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000,2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2001,2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include <Python.h>

PyMethodDef PortableServer_methods[] = {
		  { NULL, NULL }
};

void
initPortableServer (void)
{
	PyObject *m;
	PyObject *servant_base; /* PortableServer.Servant */

	PyImport_ImportModule ("CORBA");
	m = Py_InitModule ("PortableServer", PortableServer_methods);

	servant_base = PyClass_New (NULL,
				    PyDict_New (),
				    PyString_FromString ("Servant"));
	
	PyObject_SetAttrString (servant_base,
				"__module__",
				PyString_FromString("PortableServer"));
	
	PyObject_SetAttrString (m, "Servant", servant_base);

}

/* Local Variables: */
/* c-file-style: "python" */
/* End: */

