/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Johan Dahlin
 *
 */

#ifndef __CORBA_ORB_H__
#define __CORBA_ORB_H__

#include "orbit-python.h"

extern CORBA_ORB_PyObject * orb_instance;
extern POA_PyObject *root_poa;
extern GHashTable *orb_objects;

CORBA_ORB_PyObject * CORBA_ORB_PyObject__new                        (CORBA_ORB);

PyObject *           CORBA_ORB_PyObject__resolve_initial_references (CORBA_ORB_PyObject *self, 
								     PyObject *obj);

#endif /* __CORBA_ORB_H__ */
