/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com> 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include "demarshal.h"

#include "corba-any.h"
#include "corba-object.h"
#include "corba-typecode.h"
#include "exceptions.h"
#include "util.h"

#define  advance_buf(b, l)         \
{                                  \
        char *z = (char *)b->cur;  \
        z += l;                    \
        b->cur = z;                \
}                 

static CORBA_boolean buf_getn            (GIOPRecvBuffer *             buf,
					  void *                       dest,
					  size_t                       n);
static PyObject *    demarshal_short     (GIOPRecvBuffer *             buf);
static PyObject *    demarshal_boolean   (GIOPRecvBuffer *             buf);
static PyObject *    demarshal_string    (GIOPRecvBuffer *             buf,
					  CORBA_TypeCode               tc);
static PyObject *    demarshal_wstring   (GIOPRecvBuffer *             buf,
					  CORBA_TypeCode               tc);
static PyObject *    demarshal_object    (GIOPRecvBuffer *             buf,
					  CORBA_ORB                    orb);
static PyObject *    demarshal_struct    (GIOPRecvBuffer *             buf,
					  CORBA_TypeCode               tc, 
					  CORBA_ORB                    orb);
static PyObject *    demarshal_array     (GIOPRecvBuffer *             buf,
					  CORBA_TypeCode               tc,
					  CORBA_ORB                    orb);
static PyObject *    demarshal_sequence  (GIOPRecvBuffer *             buf,
					  CORBA_TypeCode               tc, 
					  CORBA_ORB                    orb);
static PyObject *    demarshal_enum      (GIOPRecvBuffer *             buf,
					  CORBA_TypeCode               tc);
static PyObject *    demarshal_union     (GIOPRecvBuffer *            buf,
					  CORBA_TypeCode               tc,
					  CORBA_ORB                    orb);
static PyObject *    demarshal_long      (GIOPRecvBuffer *             buf);
static PyObject *    demarshal_char      (GIOPRecvBuffer *             buf);
static PyObject *    demarshal_octet     (GIOPRecvBuffer *             buf);
static PyObject *    demarshal_float     (GIOPRecvBuffer *             buf);
static PyObject *    demarshal_double    (GIOPRecvBuffer *             buf);
static PyObject *    demarshal_longlong  (GIOPRecvBuffer *             buf);	
static PyObject *    demarshal_any       (GIOPRecvBuffer *             buf,
					  CORBA_TypeCode               tc,
					  CORBA_ORB                    orb);
static PyObject *    demarshal_typecode  (GIOPRecvBuffer *             buf);

PyObject *
demarshal_arg (GIOPRecvBuffer *buf,
	       CORBA_TypeCode  tc,
	       CORBA_ORB       orb)
{
	d_message (2, "demarshal_arg: tc->kind = %d, tc->name = %s", 
		   tc->kind, tc->name);
	
	switch (tc->kind) {
		case CORBA_tk_null:
			Py_INCREF(Py_None);
			return Py_None;
		case CORBA_tk_void:
			return NULL;
		case CORBA_tk_string:
			return demarshal_string(buf, tc);
#if PY_MAJOR_VERSION >= 2
		case CORBA_tk_wstring:
			return demarshal_wstring(buf, tc);
#endif /* PY_MAJOR_VERSION >= 2 */
		case CORBA_tk_short:
		case CORBA_tk_ushort:
			return demarshal_short(buf);
		case CORBA_tk_boolean:
			return demarshal_boolean(buf);
		case CORBA_tk_long:
		case CORBA_tk_ulong:
			return demarshal_long(buf);
		case CORBA_tk_char:
			return demarshal_char(buf);
		case CORBA_tk_octet:
			return demarshal_octet(buf);
		case CORBA_tk_float:
			return demarshal_float(buf);
		case CORBA_tk_double:
			return demarshal_double(buf);
		case CORBA_tk_longlong:
		case CORBA_tk_ulonglong:
			return demarshal_longlong(buf);

		case CORBA_tk_struct:
			return demarshal_struct(buf, tc, orb);
		case CORBA_tk_array:
			return demarshal_array(buf, tc, orb);
		case CORBA_tk_sequence:
			return demarshal_sequence(buf, tc, orb);
		case CORBA_tk_enum:
			return demarshal_enum(buf, tc);
		case CORBA_tk_union:
			return demarshal_union(buf, tc, orb);
		case CORBA_tk_alias:
			return demarshal_arg(buf, tc->subtypes[0], orb);
		case CORBA_tk_any:
			return demarshal_any(buf, tc, orb);
		case CORBA_tk_TypeCode:
			return demarshal_typecode(buf);
		case CORBA_tk_objref:
			return demarshal_object(buf, orb);

		case CORBA_tk_wchar:
#if PY_MAJOR_VERSION < 2
		case CORBA_tk_wstring:
#endif /* PY_MAJOR_VERSION < 2 */
		case CORBA_tk_Principal:
		default:
			d_warning(0, "Can't demarshal unsupported typecode: %d", tc->kind);
			return NULL;

	}
}

void
demarshal_exception(GIOPRecvBuffer *buf, CORBA_TypeCode tc,
		    CORBA_exception_type type,
		    CORBA_OperationDescription *opd, CORBA_ORB orb)
{
	CORBA_unsigned_long len, i;
	char *repo_id = 0;
	PyObject *data = NULL, *cl;

	d_message(4, "demarshal_exception: entered");
	g_return_if_fail (type != CORBA_NO_EXCEPTION );
	
	if (!buf_getn(buf, &len, sizeof(len)))
		goto skip;
	if (*((char *)buf->cur + len - 1) != '\0')
		goto skip;

	repo_id = (char *)buf->cur;
	advance_buf(buf, len);
	if (type == CORBA_USER_EXCEPTION) {
		if (opd)
			for (i = 0; i < opd->exceptions._length; i++) 
				if (!strcmp(opd->exceptions._buffer[i].id, repo_id)) {
					tc = opd->exceptions._buffer[i].type;
					break;
				}
		if (!tc) {
			raise_system_exception(OPExc_UNKNOWN, 0, 
			                       CORBA_COMPLETED_MAYBE, "Unkown exception: %s",
			                       repo_id);
			return;
		}
	}

	if (!tc) { // system exception
		CORBA_long minor, completed;
		PyObject *ex = g_hash_table_lookup(exceptions, repo_id);
		if (!buf_getn(buf, &minor, sizeof(minor))) { }
		if (!buf_getn(buf, &completed, sizeof(completed))) { }
		// FIXME: validate ex
		raise_system_exception(ex, minor, completed, NULL);
		return;
	}
	// Demarshal the exception values
	cl = (PyObject *)g_hash_table_lookup(exceptions, repo_id);
	data = PyInstance_New(cl, NULL, NULL);

	for (i = 0; i < tc->sub_parts; i++) {
		PyObject *o = demarshal_arg(buf, tc->subtypes[i], orb);
		if (!o) goto bail;
		PyObject_SetAttrString(data, (char *)tc->subnames[i], o);
	}
	// Raise the exception
skip:
	raise_user_exception(repo_id, data);
bail:
	Py_XDECREF(data);
}

static CORBA_boolean
buf_getn (GIOPRecvBuffer *buf, void *dest, size_t n)
{
	static int i = 0;
	if (buf->left_to_read) {
		d_warning(0, "incomplete message received");
		return CORBA_FALSE;
	}
	
	memcpy (dest, (gpointer)buf->cur, n);
	buf->cur = ((guchar *)buf->cur) + n;
	
	i++;
	
	return CORBA_TRUE;
}

static PyObject *
demarshal_short (GIOPRecvBuffer *buf)
{
	CORBA_short v;

	d_message(4, "demarshal_short: entered");

	if (buf_getn(buf, &v, sizeof(v)))
		return PyInt_FromLong((CORBA_long)v);
	raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_YES, NULL);
	return NULL;
}

static PyObject *
demarshal_boolean(GIOPRecvBuffer *buf)
{
	CORBA_boolean v;

	d_message(4, "demarshal_boolean: entered");

	if (buf_getn(buf, &v, sizeof(v)))
		return Py_BuildValue("h", v);
	raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_YES, NULL);
	return NULL;
}

static PyObject *
demarshal_string(GIOPRecvBuffer *buf, CORBA_TypeCode tc)
{
	char *s;
	CORBA_unsigned_long len = 0;
	PyObject *o;
	
	d_message(4, "demarshal_string: entered");
	if (!buf_getn(buf, &len, sizeof(len)))
		return NULL;
	if (tc->length != 0 && len - 1 > tc->length) {
		raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_YES,
		                       "String received is too long!");
		return NULL;
	}


	s = g_new(char, len);
	memmove(s, buf->cur, len);
	advance_buf(buf, len);
	
	s[len - 1] = '\0';
	o = PyString_FromString(s);
	d_message(3, "demarshal_string: contents = %s", s);
	g_free(s);
	return o;
}

#if PY_MAJOR_VERSION >= 2
// FIXME: check interoperability with other ORBs!
#warning Support for wide strings is highly experimental!
static PyObject *
demarshal_wstring(GIOPRecvBuffer *buf, CORBA_TypeCode tc)
{
	Py_UNICODE *s;
	CORBA_unsigned_long len;
	PyObject *o;

	d_message(4, "demarshal_wstring: entered");

	if (!buf_getn(buf, &len, sizeof(len)))
		return NULL;
	if (tc->length != 0 && len - 1 > tc->length) {
		raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_YES,
		                       "String received is too long!");
		return NULL;
	}

	s = g_new(Py_UNICODE, len);
	memmove(s, buf->cur, len * sizeof(Py_UNICODE));
	advance_buf(buf, len);

	s[len - 1] = 0;
	o = PyUnicode_FromUnicode(s, len-1);
	g_free(s);
	return o;
}
#endif /* PY_MAJOR_VERSION >= 2 */

static PyObject *
demarshal_object(GIOPRecvBuffer *buf, CORBA_ORB orb)
{
	CORBA_Object retval;
	
	d_message(4, "demarshal_object: entered");

#ifdef OLD
	retval = ORBit_demarshal_object(buf, orb);
#else
	if (ORBit_demarshal_object (&retval, buf, orb)) {
		raise_system_exception (OPExc_MARSHAL,
					0, 
					CORBA_COMPLETED_NO,
					NULL);

		retval = CORBA_OBJECT_NIL;
	}
#endif	
	return (PyObject *)CORBA_Object_to_PyObject (retval);
}

static PyObject *
demarshal_struct(GIOPRecvBuffer *buf, CORBA_TypeCode tc, 
		 CORBA_ORB orb)
{
	PyObject *cl = (PyObject *)g_hash_table_lookup(object_glue, tc->repo_id),
	         *data;
	CORBA_unsigned_long i;

	d_message(4, "demarshal_struct: entered");

	if (!cl)
		return raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_NO,
			                           "Asked to demarshal unknown struct (%s)", 
		                              tc->repo_id);
	d_message(3, "demarshal_struct: %d sub parts", tc->sub_parts);
	data = PyInstance_New(cl, NULL, NULL);
	for (i = 0; i < tc->sub_parts; i++) {
		PyObject *o = demarshal_arg(buf, tc->subtypes[i], orb);
		if (!o) goto bail;
		PyObject_SetAttrString(data, (char *)tc->subnames[i], o);
		Py_DECREF(o);
	}
	return data;
bail:
	Py_DECREF(data);
	return NULL;
}
	
static PyObject *
demarshal_array(GIOPRecvBuffer *buf, CORBA_TypeCode tc, CORBA_ORB orb)
{
	PyObject *tuple = PyTuple_New(tc->length);
	CORBA_unsigned_long i;

	d_message(4, "demarshal_array: entered");

	for (i = 0; i < tc->length; i++) {
		PyObject *o = demarshal_arg(buf, tc->subtypes[0], orb);
		if (!o) goto bail;
		PyTuple_SetItem(tuple, i, o);
	}
	return tuple;
bail:
	Py_DECREF(tuple);
	return NULL;
}

static PyObject *
demarshal_sequence(GIOPRecvBuffer *buf, CORBA_TypeCode tc, 
		   CORBA_ORB orb)
{
	CORBA_unsigned_long  len;
	PyObject            *seq;
	CORBA_unsigned_long  i;
	PyObject            *o;
	gchar               *string;
	
	d_message(4, "demarshal_sequence: entered");
	if (!buf_getn (buf, &len, sizeof (len))) {
		return raise_system_exception (OPExc_MARSHAL, 0, CORBA_COMPLETED_NO, NULL);
	}
	
	d_message(3, "demarshal_sequence: length = %d", len);
	if (tc->subtypes[0]->kind == CORBA_tk_char ||
	    tc->subtypes[0]->kind == CORBA_tk_octet) {
		string = g_new0 (char, len + 1);
		memmove (string, buf->cur, len);
		advance_buf (buf, len);
		
		seq = PyString_FromString (string);
		g_free (string);
	} else {
		seq = PyTuple_New (len);
		for (i = 0; i < len; i++) {
			o = demarshal_arg (buf, tc->subtypes[0], orb);
			if (!o) {
				Py_DECREF (seq);
				return NULL;
			}
			PyTuple_SetItem (seq, i, o);
		}
	}

	return seq;
}

static PyObject *
demarshal_enum(GIOPRecvBuffer *buf, CORBA_TypeCode tc)
{
	CORBA_unsigned_long v;

	d_message(4, "demarshal_enum: entered");

	if (!buf_getn(buf, &v, sizeof(v)))
		return NULL;
	return PyInt_FromLong(v);
}

	
static PyObject *
demarshal_union(GIOPRecvBuffer *buf, CORBA_TypeCode tc, CORBA_ORB orb)
{
	PyObject *v, *d, *inst, *cl;
	CORBA_long arm;

	d_message(4, "demarshal_union: entered");

	cl = (PyObject *)g_hash_table_lookup(object_glue, tc->repo_id);
	if (!cl) {
		d_warning(0, "Unregistered union type: %s", tc->repo_id);
		return NULL;
	}
	d = demarshal_arg(buf, tc->discriminator, orb);
	if (!d) 
		return NULL;
	
	arm = find_union_arm(tc, d);
	if (PyErr_Occurred())  // exception from find_union_arm
		return NULL;

	if (arm >= 0) {
		PyObject *tuple;
		v = demarshal_arg(buf, tc->subtypes[arm], orb);
		if (!v)
			return NULL;
		tuple = Py_BuildValue("OO", d, v);	
		Py_DECREF(d);
		Py_DECREF(v);
		inst = PyInstance_New(cl, tuple, NULL);
		Py_DECREF(tuple);
		return inst;
	}

	// Couldn't find arm
	Py_DECREF(d);
	Py_INCREF(Py_None);
	return Py_None;
}


	
static PyObject *
demarshal_long(GIOPRecvBuffer *buf)
{
	CORBA_long v;

	d_message(4, "demarshal_long: entered");

	if (buf_getn(buf, &v, sizeof(v)))
		return Py_BuildValue("l", v);
	raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_YES, NULL);
	return NULL;
}


	
static PyObject *
demarshal_char(GIOPRecvBuffer *buf)
{
	CORBA_char v;

	d_message(4, "demarshal_char: entered");

	if (buf_getn(buf, &v, sizeof(v)))
		return Py_BuildValue("c", v);
	raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_YES, NULL);
	return NULL;
}

	
static PyObject *
demarshal_octet(GIOPRecvBuffer *buf)
{
	CORBA_octet v;

	d_message(4, "demarshal_octet: entered");

	if (buf_getn(buf, &v, sizeof(CORBA_char)))
		return Py_BuildValue("b", v);
	raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_YES, NULL);
	return NULL;
}



	
static PyObject *
demarshal_float(GIOPRecvBuffer *buf)
{
	CORBA_float v;

	d_message(4, "demarshal_float: entered");

	if (buf_getn(buf, &v, sizeof(v)))
		return Py_BuildValue("f", v);
	raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_YES, NULL);
	return NULL;
}
	
static PyObject *
demarshal_double(GIOPRecvBuffer *buf)
{
	CORBA_double v;

	d_message(4, "demarshal_double: entered");

	if (buf_getn(buf, &v, sizeof(v)))
		return Py_BuildValue("d", v);
	raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_YES, NULL);
	return NULL;
}

static PyObject *
demarshal_longlong(GIOPRecvBuffer *buf)
{
	CORBA_long_long v;

	d_message(4, "demarshal_longlong: entered");

	if (buf_getn(buf, &v, sizeof(v)))
		return Py_BuildValue("l", v);
	raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_YES, NULL);
	return NULL;
}

static PyObject *
demarshal_any(GIOPRecvBuffer *buf, CORBA_TypeCode tc, CORBA_ORB orb)
{
	CORBA_TypeCode res_tc;
	CORBA_TypeCode_PyObject *to;
	CORBA_Any_PyObject *ao;
	PyObject *val;

	d_message(4, "demarshal_any: entered");

	ORBit_decode_CORBA_TypeCode(&res_tc, buf);
	val = demarshal_arg(buf, res_tc, orb);
	if (!val) {
		raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_YES, NULL);
		CORBA_Object_release((CORBA_Object)res_tc, NULL);
		return NULL;
	}
	to = CORBA_TypeCode_PyObject__new(res_tc);
	CORBA_Object_release((CORBA_Object)res_tc, NULL);

	ao = CORBA_Any_PyObject__new(to, val);
	Py_DECREF(to);
	Py_DECREF(val);
	d_message(7, "demarshal_any: returning %x", (guint)ao);
	return (PyObject *)ao;
}

static PyObject *
demarshal_typecode(GIOPRecvBuffer *buf)
{
	CORBA_TypeCode res_tc;
	CORBA_TypeCode_PyObject *to;
	
	d_message(4, "demarshal_TypeCode: entered");
	
	ORBit_decode_CORBA_TypeCode(&res_tc, buf);	
	to = CORBA_TypeCode_PyObject__new(res_tc);
	CORBA_Object_release((CORBA_Object)res_tc, NULL);
	
	return (PyObject*)to;
}

/* Local Variables: */
/* c-file-style: "python" */
/* End: */
