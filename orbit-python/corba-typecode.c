/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com> 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include "corba-typecode.h"
#include "idl.h"
#include "util.h"

static void       CORBA_TypeCode_PyObject__dealloc (CORBA_TypeCode_PyObject *self);

static PyObject * CORBA_TypeCode_PyObject__getattr (CORBA_TypeCode_PyObject *self, 
						    char *name);

static int        CORBA_TypeCode_PyObject__setattr (CORBA_TypeCode_PyObject *self,
						    char *name, 
						    PyObject *obj);

static PyObject * CORBA_TypeCode_PyObject__repr    (CORBA_TypeCode_PyObject *self);

/* Generic CORBA.Object */
PyTypeObject CORBA_TypeCode_PyObject_Type = {
	PyObject_HEAD_INIT(NULL)
	0,                                             /*ob_size*/
	"CORBA.TypeCode",                              /*tp_name*/
	sizeof(CORBA_TypeCode_PyObject),               /*tp_basicsize*/
	0,                                             /*tp_itemsize*/
	
	/* methods */
	(destructor)CORBA_TypeCode_PyObject__dealloc,  /*tp_dealloc*/
	0,                                             /*tp_print*/
	(getattrfunc)CORBA_TypeCode_PyObject__getattr, /*tp_getattr*/
	(setattrfunc)CORBA_TypeCode_PyObject__setattr, /*tp_setattr*/
	0,                                             /*tp_compare*/
	(reprfunc)CORBA_TypeCode_PyObject__repr,       /*tp_repr*/
	0,                                             /*tp_as_number*/
	0,                                             /*tp_as_sequence*/
	0,                                             /*tp_as_mapping*/
	0,                                             /*tp_hash*/
};

CORBA_TypeCode_PyObject *
CORBA_TypeCode_PyObject__new (CORBA_TypeCode tc)
{
	CORBA_TypeCode_PyObject *self;

	self = PyObject_NEW (CORBA_TypeCode_PyObject,
			     &CORBA_TypeCode_PyObject_Type);
	
	if (self) {
		self->tc = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)tc, NULL);
		self->repo_id = find_repo_id_from_typecode (tc);
		if (self->repo_id) {
			self->repo_id = g_strdup(self->repo_id);
		}
	}
		
	return self;
}


static void
CORBA_TypeCode_PyObject__dealloc (CORBA_TypeCode_PyObject *self)
{	
	if (self->repo_id) {
		g_free (self->repo_id);
	}
	
	CORBA_Object_release ((CORBA_Object)self->tc, NULL);
	PyMem_DEL (self);
}

static PyObject *
CORBA_TypeCode_PyObject__getattr (CORBA_TypeCode_PyObject *self, 
				  char *name)
{
	return NULL;
}

static int
CORBA_TypeCode_PyObject__setattr (CORBA_TypeCode_PyObject *self, 
				  char *name,
				  PyObject *v)
{
	return -1;
}

static PyObject *
CORBA_TypeCode_PyObject__repr (CORBA_TypeCode_PyObject *self)
{
	gchar    *repo_id;
	PyObject *retval;
	
	d_assert (self->repo_id != NULL);
	
	repo_id = g_strdup_printf ("<CORBA.TypeCode object at %x of type '%s'>",
		                   (int)self,
			           self->repo_id);
	
	retval = PyString_FromString (repo_id);
	g_free (repo_id);
	return retval;
}
