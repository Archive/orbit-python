/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Johan Dahlin
 *
 */

#ifndef __ORBIT_PYTHON_H__
#define __ORBIT_PYTHON_H__

#include <Python.h>
#include <orbit/orbit.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Public structs */
typedef struct {
	PyObject_HEAD
	CORBA_Object       obj;
	CORBA_Environment  ev;
	GSList            *last_method;
	char              *repo_id;
} CORBA_PyObject;

typedef struct {
	PyObject_HEAD
	CORBA_ORB         obj;
	CORBA_Environment ev;
} CORBA_ORB_PyObject;

typedef struct {
	PyObject_HEAD
	CORBA_TypeCode  tc;	
	char           *repo_id;
} CORBA_TypeCode_PyObject;

typedef struct {
	PyObject_HEAD
	PyObject                *value;
	CORBA_TypeCode_PyObject *tc_object;
} CORBA_Any_PyObject;

typedef struct {
	PyObject_HEAD
	CORBA_Object   obj;
} CORBA_fixed_PyObject;

typedef struct {
	PyObject_HEAD
	CORBA_ORB_PyObject *orb;
	PortableServer_POA  obj;
	CORBA_Environment   ev;
} POA_PyObject;

typedef struct {
	PyObject_HEAD
	PortableServer_POAManager obj;
	CORBA_Environment         ev;
} POAManager_PyObject;

typedef struct _ORBitPython_FunctionStruct 
{
	char *orbit_python_version;
	
	PyObject *(*obj_new)(CORBA_Object);
	CORBA_boolean (*obj_check)(PyObject *);
	CORBA_Object (*obj_get)(PyObject *);
		
	PyTypeObject *orb_type;
	CORBA_ORB_PyObject *(*orb_new)(CORBA_ORB);

	PyTypeObject *any_type;
	CORBA_Any_PyObject *(*any_new)(CORBA_any *);
	CORBA_Any_PyObject *(*any_new_val)(CORBA_TypeCode_PyObject *, PyObject *);
	CORBA_any *(*any_get)(CORBA_Any_PyObject *);

	PyTypeObject *tc_type;
	CORBA_TypeCode_PyObject *(*tc_new)(CORBA_TypeCode);
   
	PyTypeObject *poa_type;
	POA_PyObject *(*poa_new)(PortableServer_POA object);
        
	PyTypeObject *poam_type;
	POAManager_PyObject *(*poam_new)(PortableServer_POAManager o);

} _ORBitPython_FunctionStruct;


#ifdef INSIDE_ORBIT_PYTHON
extern PyTypeObject CORBA_PyObject_Type;
extern PyTypeObject CORBA_ORB_PyObject_Type;
extern PyTypeObject CORBA_TypeCode_PyObject_Type;
extern PyTypeObject CORBA_Any_PyObject_Type;
extern PyTypeObject CORBA_fixed_PyObject_Type;
extern PyTypeObject POAManager_PyObject_Type;
extern PyTypeObject POA_PyObject_Type;

#define CORBA_ORB_PyObject_Check(v) ((v)->ob_type == &CORBA_ORB_PyObject_Type)
#define CORBA_TypeCode_PyObject_Check(v) \
		((v)->ob_type == &CORBA_TypeCode_PyObject_Type)
#define CORBA_Any_PyObject_Check(v) \
		((v)->ob_type == &CORBA_Any_PyObject_Type)
#define CORBA_fixed_PyObject_Check(v) \
		((v)->ob_type == &CORBA_fixed_PyObject_Type)
#define POA_PyObject_Check(v) ((v)->ob_type == &POA_PyObject_Type)
#define POAManager_PyObject_Check(v) ((v)->ob_type == &POAManager_PyObject_Type)

#else
struct _ORBitPython_FunctionStruct *_ORBitPython_API;

#define PyORBit_Object              PyObject
#define PyORBit_Object_New          (_ORBitPython_API->obj_new)
#define PyORBit_Object_Check        (_ORBitPython_API->obj_check)
#define PyORBit_Object_Get          (_ORBitPython_API->obj_get)

#define PyORBit_ORB                 CORBA_ORB_PyObject
#define PyORBit_ORB_New             (_ORBitPython_API->orb_new)
#define PyORBit_ORB_Check(v)        ((v)->ob_type == _ORBitPython_API->orb_type)
#define PyORBit_ORB_Type            *(_ORBitPython_API->orb_type)
#define PyORBit_ORB_Get(o)          (((CORBA_ORB_PyObject*)(o))->obj)

#define PyORBit_Any                 CORBA_Any_PyObject
#define PyORBit_Any_New             (_ORBitPython_API->any_new)
#define PyORBit_Any_New_with_value  (_ORBitPython_API->any_new_val)
#define PyORBit_Any_Check(v)        ((v)->ob_type == _ORBitPython_API->any_type)
#define PyORBit_Any_Type            *(_ORBitPython_API->any_type)
#define PyORBit_Any_Get             (_ORBitPython_API->any_get)

#define PyORBit_TypeCode            CORBA_TypeCode_PyObject
#define PyORBit_TypeCode_New        (_ORBitPython_API->tc_new)
#define PyORBit_TypeCode_Check(v)   ((v)->ob_type == _ORBitPython_API->tc_type)
#define PyORBit_TypeCode_Type       *(_ORBitPython_API->tc_type)
#define PyORBit_TypeCode_Get(o)     (((CORBA_TypeCode_PyObject*)(o))->tc)
 
#define PyORBit_POA                 POA_PyObject  
#define PyORBit_POA_New             (_ORBitPython_API->poa_new)
#define PyORBit_POA_Check(v)        ((v)->ob_type == _ORBitPython_API->poa_type)
#define PyORBit_POA_Type            *(_ORBitPython_API->poa_type)
#define PyORBit_POA_Get(o)          (((POA_PyObject*)(o))->obj)
 
#define PyORBit_POAManager          POAManager_PyObject  
#define PyORBit_POAManager_New      (_ORBitPython_API->poam_new)
#define PyORBit_POAManager_Check(v) ((v)->ob_type == _ORBitPython_API->poam_type)
#define PyORBit_POAManager_Type     *(_ORBitPython_API->poam_type)
#define PyORBit_POAManager_Get(o)   (((POAManager_PyObject*)(o))->obj)   

#define init_orbit_python() \
{ \
	PyObject *orbpy = PyImport_ImportModule("CORBA"); \
	if (orbpy != NULL) { \
		PyObject *module_dict = PyModule_GetDict(orbpy); \
		PyObject *cobj = PyDict_GetItemString(module_dict, "_ORBitPython_API"); \
		if (PyCObject_Check(cobj)) \
			_ORBitPython_API = PyCObject_AsVoidPtr(cobj); \
		else \
			Py_FatalError("Could not find _ORBitPython_API object"); \
	} else \
		Py_FatalError("Could not import CORBA"); \
}
#endif /* INSIDE_ORBIT_PYTHON */

#ifdef __cplusplus
}
#endif

#endif /* __ORBIT_PYTHON_H__ */

/* Local Variables: */
/* c-file-style: "python" */
/* End: */
