/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001-2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Johan Dahlin
 *
 */

#ifndef __PROCESS_IDL_H__
#define __PROCESS_IDL_H__

#include <glib/ghash.h>
#include <glib/gslist.h>

GSList *   get_idl_list_for_module (char       *name,
				    gboolean    from_global,
				    char      **failed);

void       set_file_as_loaded      (char        *filename,
				    GHashTable **loaded);

GSList *   get_defines_for_file    (const char *filename);

GSList *   get_global_idl_files    (void);

void       process_idl_paths       (char       *pathstr);

#endif /* __PROCESS_IDL_H__ */
