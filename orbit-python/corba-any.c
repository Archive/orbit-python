/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com> 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include "corba-any.h"

static void       CORBA_Any_PyObject__dealloc (CORBA_Any_PyObject *self);

static PyObject * CORBA_Any_PyObject__getattr (CORBA_Any_PyObject *self,
					       char               *name);

static int        CORBA_Any_PyObject__setattr (CORBA_Any_PyObject *self,
					       char               *name,
					       PyObject           *obj);

static PyObject * CORBA_Any_PyObject__repr    (CORBA_Any_PyObject *self);

/* Generic CORBA.Object */
PyTypeObject
CORBA_Any_PyObject_Type = {
	PyObject_HEAD_INIT(NULL)
	0,                                        /*ob_size*/
	"CORBA.Any",                              /*tp_name*/
	sizeof(CORBA_Any_PyObject),               /*tp_basicsize*/
	0,                                        /*tp_itemsize*/
	
	/* methods */
	(destructor)CORBA_Any_PyObject__dealloc,  /*tp_dealloc*/
	0,                                        /*tp_print*/
	(getattrfunc)CORBA_Any_PyObject__getattr, /*tp_getattr*/
	(setattrfunc)CORBA_Any_PyObject__setattr, /*tp_setattr*/
	0,                                        /*tp_compare*/
	(reprfunc)CORBA_Any_PyObject__repr,       /*tp_repr*/
	0,                                        /*tp_as_number*/
	0,                                        /*tp_as_sequence*/
	0,                                        /*tp_as_mapping*/
	0,                                        /*tp_hash*/
};

CORBA_Any_PyObject *
CORBA_Any_PyObject__new (CORBA_TypeCode_PyObject *tc,
			 PyObject *value)
{
	CORBA_Any_PyObject *self;
	self = PyObject_NEW(CORBA_Any_PyObject, &CORBA_Any_PyObject_Type);
	if (self) {
		Py_INCREF(tc);
		Py_INCREF(value);
		self->tc_object = tc;
		self->value = value;
	}
	return self;
}


static void
CORBA_Any_PyObject__dealloc(CORBA_Any_PyObject *self)
{
	Py_DECREF(self->value);
	Py_DECREF(self->tc_object);
	PyMem_DEL(self);
}

static PyObject *
CORBA_Any_PyObject__getattr(CORBA_Any_PyObject *self, char *name)
{
	if (!strcmp(name, "tc")) {
          Py_INCREF(self->tc_object);
          return (PyObject *)self->tc_object;
        }
	else if (!strcmp(name, "value")) {
          Py_INCREF(self->value);
          return self->value;
        }
	return NULL;
}

static int
CORBA_Any_PyObject__setattr (CORBA_Any_PyObject *self, 
			     char               *name,
			     PyObject           *v)
{
	return -1;
}

static PyObject *
CORBA_Any_PyObject__repr(CORBA_Any_PyObject *self)
{
	PyObject *tc_rep_obj, *val_rep_obj, *ret;
	char *tc_repr = "Unknown",  *val_repr = "Unknown", *s;

	if ((tc_rep_obj = PyObject_Repr((PyObject *)self->tc_object)))
		tc_repr = PyString_AsString(tc_rep_obj);
	if ((val_rep_obj = PyObject_Repr(self->value)))
		val_repr = PyString_AsString(val_rep_obj);
	s = g_strconcat("<Type ", tc_repr, " with value ", val_repr, ">", NULL);
	Py_XDECREF(tc_rep_obj);
	Py_XDECREF(val_rep_obj);
	ret = PyString_FromString(s);
	g_free(s);
	return ret;
}
