/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Johan Dahlin
 *
 */
#ifndef __ORBIT_PYTHON_TYPES_H__
#define __ORBIT_PYTHON_TYPES_H__

#include <Python.h>
#include <orbit/orbit-types.h>
#include <orbit/poa/poa.h>

#include "orbit-python.h"

typedef CORBA_InterfaceDef_FullInterfaceDescription InterfaceDef_Full;

/* glue for CORBA interfaces to Python objects (client side) */
typedef struct {
	PyTypeObject *type;
	PyObject *class_obj;
	PyMethodDef *generic_method, *narrow_method, *is_a_method,
	            *is_equivalent_method, *hash_method, *non_existent_method;
	InterfaceDef_Full *desc;
	
	PortableServer_ClassInfo *class_info;
	CORBA_unsigned_long class_id;
} CORBA_PyObject_Glue;

/* glue for CORBA interfaces to Python objects (client side) */
typedef struct {
	PyObject *class_obj;
	InterfaceDef_Full *desc;        /* idl */
	ORBit_IInterface *interface;    /* typecode */
	
	PortableServer_ClassInfo *class_info;
	CORBA_unsigned_long class_id;
	char *repo_id;
	CORBA_PyObject_Glue *oldglue; ///
} CORBA_PyClass_Glue;

/* glue for CORBA interfaces to Python objects (client side) */
typedef struct {
	/* the orb that this object is attached to */
	CORBA_ORB_PyObject *orb; 
	CORBA_PyClass_Glue *class_glue;
	CORBA_Object        obj;
	CORBA_Environment   ev;
	/* duplicated from class_glue for convenience */
	char          *repo_id; 
} CORBA_PyInstance_Glue;

/* glue for servant classes to Python servant classes */
typedef struct {
	PortableServer_ClassInfo *class_info;
	CORBA_unsigned_long class_id;
	CORBA_PyClass_Glue *interface_glue;
} Servant_PyClass_Glue;

/* glue for CORBA servants to Python instances */
typedef struct {
	void *_private;
	PortableServer_ServantBase__vepv *vepv;

	Servant_PyClass_Glue *class_glue;
	/* impl: for delegation, an instance of the implementation class
	 * for inheritance, the servant object itself
	 */
	PyObject *impl;
	/* the servant object for both delegation and inheritance
	 * So for inheritance, servant == impl
	 */
	PyObject *servant;
	CORBA_boolean activated;
	POA_PyObject *poa;
	PortableServer_ObjectId *oid;
} Servant_PyInstance_Glue;

#endif /* __ORBIT_PYTHON_TYPES_H__ */
