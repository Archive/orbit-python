/*  -* - Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -* - * /
/*
 * Copyright (C) 2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com> 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include "idl.h"

#include "CORBAmodule.h"
#include "corba-object.h"
#include "exceptions.h"
#include "orbit-python-types.h"
#include "poa-object.h"
#include "server.h"
#include "types.h"
#include "util.h"

#include <libIDL/IDL.h>

typedef struct {
	GSList *ops, *attrs;
	PyObject *poa_class;
} InterfaceData;

static void                         add_object_to_hierarchy     (IDL_tree            tree,
								 PyObject *          o,
								 IDL_tree            ident, 
								 gboolean            is_skel,
								 gboolean            is_module);
static void                         add_idl_operations_to_class (CORBA_PyClass_Glue * glue,
								 PyObject *           dict,
								 IDL_tree             tree);
static CORBA_PyClass_Glue *         register_interface          (IDL_tree             tree,
								 InterfaceDef_Full *  desc);
static void                         construct_interface         (IDL_tree             tree,
								 InterfaceData *      idata);
static CORBA_OperationDescription * do_operation                (IDL_tree             tree);
static GSList *                     do_attribute                (IDL_tree             tree);
static void                         do_exception                (IDL_tree             tree);
static void                         do_struct                   (IDL_tree             tree);
static void                         do_enum                     (IDL_tree             tree);
static void                         do_union                    (IDL_tree             tree);	
static gboolean                     tree_pre_func               (IDL_tree_func_data * tfd,
								 gpointer             user_data);
static gboolean                     tree_post_func              (IDL_tree_func_data * tfd,
								 gpointer             user_data);
static void                         _find_repo_id_func          (gpointer             key,
								 gpointer             value,
								 gpointer             id);
static gboolean                     remove_typecode             (char *               id);
static CORBA_TypeCode               alloc_typecode              (void);
static CORBA_TypeCode               get_integer_typecode        (IDL_tree             tree);
static CORBA_TypeCode               get_string_typecode         (IDL_tree             tree);
static CORBA_TypeCode               get_wstring_typecode        (IDL_tree             tree);
static CORBA_TypeCode               get_declarator_typecode     (IDL_tree             tree,
								 CORBA_TypeCode       base_tc);
static gchar *                      get_declarator_name         (IDL_tree             tree);
static CORBA_TypeCode               get_ident_typecode          (IDL_tree             tree);
static CORBA_TypeCode               get_exception_typecode      (IDL_tree             tree);
static CORBA_TypeCode               get_interface_typecode      (IDL_tree             tree);
static CORBA_TypeCode               get_forward_dcl_typecode    (IDL_tree             tree);
static CORBA_TypeCode               get_struct_typecode         (IDL_tree             tree);
static CORBA_TypeCode               get_sequence_typecode       (IDL_tree             tree);
static CORBA_TypeCode               get_enum_typecode           (IDL_tree             tree);
static int                          enumerator_index            (IDL_tree             tree);
static CORBA_TypeCode               get_union_typecode          (IDL_tree             tree);
static CORBA_TypeCode               get_float_typecode          (IDL_tree             tree);
static CORBA_TypeCode               get_module_typecode         (IDL_tree             tree);
static CORBA_TypeCode               get_typecode                (IDL_tree             tree);

GHashTable *python_keywords_hash = 0;
char *python_keywords[] = 
	{ "and", "assert", "break", "class", "continue", "def", "del", "elif",
	  "else", "except", "exec", "finally", "for", "from", "global", "if",
	  "import", "in", "is", "lambda", "not", "or", "pass", "print", "raise",
	  "return", "try", "while", NULL };

#define is_python_keyword(x) (g_hash_table_lookup(python_keywords_hash, x))

/* for get_union_typecode */
#define C_M(kind, type, value)                        \
	case kind: {                                  \
		type val;                             \
		res->sublabels[i] = (CORBA_long) val; \
		break;                                \
	}

GHashTable *type_codes;
GHashTable *stub_repo_ids;

static PyMethodDef
module_methods[] = {
	{ NULL, NULL }
};

void
ORBit_Python_init_typecodes (void)
{
	type_codes = g_hash_table_new (g_str_hash, g_str_equal);
/*	store_typecode ("IDL:CORBA/Null:1.0", dupe_tc (TC_CORBA_null));	*/
/*	store_typecode ("IDL:CORBA/Void:1.0", dupe_tc (TC_CORBA_void));	*/
	store_typecode ("IDL:CORBA/Short:1.0", dupe_tc (TC_CORBA_short));
	store_typecode ("IDL:CORBA/Long:1.0", dupe_tc (TC_CORBA_long));
	store_typecode ("IDL:CORBA/LongLong:1.0", dupe_tc (TC_CORBA_long_long));
	store_typecode ("IDL:CORBA/UShort:1.0", dupe_tc (TC_CORBA_unsigned_short));
	store_typecode ("IDL:CORBA/ULong:1.0", dupe_tc (TC_CORBA_unsigned_long));
	store_typecode ("IDL:CORBA/ULongLong:1.0", dupe_tc (TC_CORBA_unsigned_long_long));
	store_typecode ("IDL:CORBA/Float:1.0", dupe_tc (TC_CORBA_float));
	store_typecode ("IDL:CORBA/Double:1.0", dupe_tc (TC_CORBA_double));
	store_typecode ("IDL:CORBA/LongDouble:1.0", dupe_tc (TC_CORBA_long_double));
	store_typecode ("IDL:CORBA/Boolean:1.0", dupe_tc (TC_CORBA_boolean));
	store_typecode ("IDL:CORBA/Char:1.0", dupe_tc (TC_CORBA_char));
	store_typecode ("IDL:CORBA/WChar:1.0", dupe_tc (TC_CORBA_wchar));
	store_typecode ("IDL:CORBA/Octet:1.0", dupe_tc (TC_CORBA_octet));
	store_typecode ("IDL:CORBA/Any:1.0", dupe_tc (TC_CORBA_any));
	store_typecode ("IDL:CORBA/TypeCode:1.0", dupe_tc (TC_CORBA_TypeCode));
/*	store_typecode ("IDL:CORBA/Principal:1.0", dupe_tc (TC_CORBA_Principal)); */
	store_typecode ("IDL:CORBA/Object:1.0", dupe_tc (TC_CORBA_Object));
	store_typecode ("IDL:CORBA/String:1.0", dupe_tc (TC_CORBA_string));
	store_typecode ("IDL:CORBA/WString:1.0", dupe_tc (TC_CORBA_wstring));
}

CORBA_boolean
parse_idl (const char *file,
	   const char *params)
{
	IDL_tree tree;
	IDL_ns ns;
	int ret;

	if (!python_keywords_hash) {
		char **s = python_keywords;
		python_keywords_hash = g_hash_table_new(g_str_hash, g_str_equal);
		while (*s) {
			g_hash_table_insert(python_keywords_hash, *s, (gpointer)1);
			s++;
		}
	}
		
	ret = IDL_parse_filename(file, params, NULL, &tree, &ns, 
	                         IDLF_TYPECODES | IDLF_CODEFRAGS, IDL_WARNING1);

	if (ret == IDL_ERROR) {
		raise_system_exception(OPExc_UNKNOWN, 0,
		                       CORBA_COMPLETED_NO, "Error parsing IDL");
		return CORBA_FALSE;
	}
	else if (ret < 0) {
		raise_system_exception(OPExc_UNKNOWN, 0,
		                       CORBA_COMPLETED_NO, "Error parsing IDL: %s",
		                       g_strerror(errno));
		return CORBA_FALSE;
	}

	IDL_tree_walk(tree, NULL, tree_pre_func, tree_post_func, NULL);
	IDL_tree_free(tree);
	IDL_ns_free(ns);

	return CORBA_TRUE;
}	


/*
 * Given M.I: lookup M__POA and add I
 * Given M.M2.I: lookup M_POA, lookup M2 under that, and add I.
 * POA: modules -> Python module, suffixed with __POA
 *      interfaces -> classes w/ constructor for delegation
 * client: modules -> Python module
 *         interfaces -> Python type objects
 *
 * FIXME: this function has various minor leaks with GetAttrString
 */
static void
add_object_to_hierarchy (IDL_tree   tree,
			 PyObject * o,
			 IDL_tree   ident, 
			 gboolean   is_skel,
			 gboolean   is_module)
{
	char *full, *p, *q, *suffix = is_skel ? "__POA" : "";
	gboolean is_root = TRUE;
	PyObject *parent = is_skel ? global_poa_module : global_client_module;
	if (!ident) {
		ident = IDL_INTERFACE(tree).ident;
	}
	
	p = full = g_strdup(IDL_ns_ident_to_qstring(ident, ".", 0));
	while ((q = strstr(p, "."))) {
		char *attr_name;
		*q = 0;
		attr_name = g_strconcat(p, is_root ? suffix : "", NULL);
		if (is_root)
			parent = g_hash_table_lookup(is_skel ? poa_modules : client_modules,
			                              attr_name);
		else
			parent = PyObject_GetAttrString(parent, attr_name);
		if (!parent) d_error("Can't find parent!");
		g_free(attr_name);
		p = q + 1;
		is_root = FALSE;
	}
	if (is_root && is_module) {
		/* append to module to poa_modules */
		char *name = g_strconcat(p, is_skel ? "__POA" : NULL, NULL);
		GHashTable *hash = is_skel ? poa_modules : client_modules;
		g_hash_table_insert (hash, name, o);
	} else {
		if (PyClass_Check (o) && PyModule_Check (parent)) {
			PyObject_SetAttrString (o, "__module__", 
						PyObject_GetAttrString (parent, "__name__"));
		}
		PyObject_SetAttrString (parent, p, o);
	}

	g_free(full);
}

static void
add_idl_operations_to_class (CORBA_PyClass_Glue * glue,
			     PyObject *           dict,
			     IDL_tree             tree)
{
	CORBA_unsigned_long i;
	InterfaceDef_Full * d;

	for (i = 0; i < glue->desc->operations._length; i++) {
		GString *str = g_string_new("");
		gchar *opname = glue->desc->operations._buffer[i].name;
		g_string_sprintf(str, 
		     "%s%s(self, *t, **k): return self.__invoke(\"%s\", t, k)\n", 
		      is_python_keyword(opname) ? "def _" : "def ", opname, opname);
		PyRun_String(str->str, Py_single_input, PyEval_GetGlobals(), dict);
		g_string_free(str, TRUE);
	}
	
	/* Look through base interfaces for this operation */
	d = glue->desc;
	for (i = 0; i < d->base_interfaces._length; i++) {
		char *repo_id = d->base_interfaces._buffer[i];
		glue = (CORBA_PyClass_Glue *)g_hash_table_lookup(object_glue, repo_id);
		if (glue)
			add_idl_operations_to_class(glue, dict, tree);
	}
}

typedef struct {
	PortableServer_ServantBase__epv *_base_epv;
	struct {
		void *_private;
	} ORBitPython_epv;
} ORBitPython_vepv;


static CORBA_PyClass_Glue *
register_interface (IDL_tree            tree,
		    InterfaceDef_Full * desc)
{
	IDL_tree             ident;
	char *               repo_id;
	char *               modstr;
	char *               ifstr;
	PyObject *           bases;
	PyObject *           cldict;
	PyObject *           clname;
	PyObject *           cl;
	CORBA_PyClass_Glue * glue;
	ORBitPython_vepv   * fakevepv;
	static CORBA_unsigned_long ORBitPython__classid = 0;

	ident = IDL_INTERFACE (tree).ident;
	repo_id = g_strdup (IDL_IDENT_REPO_ID (ident));
	
	d_message (4, "%s: registering \"%s\"", __FUNCTION__, repo_id);
	
	modstr = g_strdup (IDL_ns_ident_to_qstring (ident, ".", 0));
	
	ifstr = modstr + strlen(modstr);
	while (ifstr > modstr && *ifstr != '.') ifstr--;
	if (*ifstr == '.') {
		*ifstr = 0;
		ifstr = g_strdup (ifstr + 1);
	} else {
		ifstr = g_strdup (modstr);
		g_free (modstr);
		modstr = g_strdup ("_GlobalIDL");
	}
		
	bases = PyTuple_New (1);
	PyTuple_SetItem (bases, 0, corba_object_class);
	
	cldict = PyDict_New ();
	clname = PyString_FromString (ifstr);
	cl = PyClass_New (bases, cldict, clname);
	PyObject_SetAttrString (cl, "__module__", PyString_FromString (modstr));
	g_free (ifstr);
	g_free (modstr);
		
	Py_XDECREF (bases);
	Py_XDECREF (clname);
	Py_XDECREF (cldict);
		
	glue = g_new (CORBA_PyClass_Glue, 1);
	glue->desc      = desc;
	glue->class_obj = cl;
	glue->repo_id   = repo_id;
	
	glue->class_info = g_new0 (PortableServer_ClassInfo, 1);
	glue->class_info->relay_call       = (ORBit_impl_finder)&get_skel;	
	glue->class_info->class_name       = g_strdup (glue->desc->id);
	glue->class_info->class_id         = &ORBitPython__classid;

	fakevepv = NULL;

	if (!glue->class_info->vepvmap) {
		glue->class_info->vepvmap = g_new0 (ORBit_VepvIdx, ORBitPython__classid + 1);
		glue->class_info->vepvmap[ORBitPython__classid] =
			(((char *) &(fakevepv->ORBitPython_epv)) -
			 ((char *) (fakevepv))) / sizeof(GFunc);
	}

	ORBit_classinfo_register (glue->class_info);

	add_idl_operations_to_class (glue, cldict, tree);
	
	return glue;
}

static void
construct_interface (IDL_tree        tree,
		     InterfaceData * idata)
{
	IDL_tree ident = IDL_INTERFACE(tree).ident;
	GSList *t;
	CORBA_unsigned_long len, i, cur, attr_ops = 0;
	IDL_tree inheritance_spec;
	CORBA_PyClass_Glue *glue;
	Servant_PyClass_Glue *class_glue;
	InterfaceDef_Full *desc;
	
	desc = g_new0(InterfaceDef_Full, 1);
	desc->name = g_strdup(IDL_IDENT(ident).str);
	desc->id = g_strdup(IDL_IDENT_REPO_ID(ident));

	/* Count the attributes. */
	len = (CORBA_unsigned_long)g_slist_length(idata->attrs);
	t = idata->attrs;
	for (i = 0; i < len; i++) {
		CORBA_AttributeDescription *ad;
		ad = (CORBA_AttributeDescription *)t->data;
		attr_ops += 1;
		if (ad->mode == CORBA_ATTR_NORMAL)
			attr_ops += 1;
		t = t->next;
	}
	/* Done with attribute counting. */

	/* Define all the methods */
	desc->operations._length = g_slist_length(idata->ops) + attr_ops;
	desc->operations._buffer = 
		CORBA_sequence_CORBA_OperationDescription_allocbuf(
			desc->operations._length + attr_ops);
	cur = 0;
	t = idata->ops;
	for (i = 0; i < g_slist_length(idata->ops); i++) {
		CORBA_OperationDescription *d = (CORBA_OperationDescription *)t->data;
		/* Set up the ops for interface desc */
		desc->operations._buffer[cur] = *d;
		g_free(t->data);
		t = t->next;
		cur++;
	}
	g_slist_free(idata->ops);
	/* Done with the methods. */

	/* Now setup all the attributes. */
	len = (CORBA_unsigned_long)g_slist_length(idata->attrs);
	desc->attributes._length = len;
	desc->attributes._buffer =
		CORBA_sequence_CORBA_AttributeDescription_allocbuf(len);
	desc->attributes._release = TRUE;

	t = idata->attrs;
	for (i = 0; i < desc->attributes._length; i++) {
		CORBA_OperationDescription *opd;
		CORBA_AttributeDescription *ad;
		
		ad = (CORBA_AttributeDescription *)t->data;
		desc->attributes._buffer[i] = *ad;
		opd = g_new0(CORBA_OperationDescription, 1);
		opd->name = g_strdup_printf("_get_%s", ad->name);
		opd->result = ad->type;
		opd->parameters._length = 0;
		opd->parameters._buffer = (CORBA_ParameterDescription *) NULL;
		desc->operations._buffer[cur] = *opd;
		cur++;
		if (ad->mode == CORBA_ATTR_NORMAL) {
			opd = g_new0(CORBA_OperationDescription, 1);
			opd->name = g_strdup_printf("_set_%s", ad->name);
			opd->result = TC_void;
			opd->parameters._length = 1;
			opd->parameters._buffer = CORBA_sequence_CORBA_ParameterDescription_allocbuf(1);
			opd->parameters._buffer[0].name = ad->name;
			opd->parameters._buffer[0].type = ad->type;
			opd->parameters._buffer[0].mode = CORBA_PARAM_IN;
			opd->parameters._buffer[0].type_def = CORBA_OBJECT_NIL;
			desc->operations._buffer[cur] = *opd;
			cur++;
		}
		g_free(t->data);
		t = t->next;
	}
	g_slist_free(idata->attrs);
	/* Done with attributes. */
	
	/* Setup base interfaces */
	inheritance_spec = IDL_INTERFACE (tree).inheritance_spec;
	len = IDL_list_length(inheritance_spec);
	desc->base_interfaces._length = len;
	desc->base_interfaces._buffer = 
	   CORBA_sequence_CORBA_RepositoryId_allocbuf(len);
	desc->base_interfaces._release = TRUE;
	
	for (i = 0; i < len; i++) {
		IDL_tree ident = IDL_LIST(inheritance_spec).data;
		desc->base_interfaces._buffer[i] = IDL_IDENT_REPO_ID(ident);
		inheritance_spec = IDL_LIST(inheritance_spec).next;
	}
	
	glue = register_interface (tree, desc);

	g_hash_table_insert (object_glue, glue->repo_id, glue);

	class_glue = g_new0 (Servant_PyClass_Glue, 1);
	class_glue->class_info     = glue->class_info;
	class_glue->class_id       = glue->class_id;
	class_glue->interface_glue = glue;
	
	g_hash_table_insert (servant_class_glue, idata->poa_class, class_glue);
	
}

static CORBA_OperationDescription *
do_operation (IDL_tree tree)
{
	gboolean f_oneway = IDL_OP_DCL (tree).f_oneway;
	IDL_tree op_type_spec = IDL_OP_DCL (tree).op_type_spec;
	IDL_tree ident = IDL_OP_DCL (tree).ident;
	IDL_tree dcls = IDL_OP_DCL (tree).parameter_dcls;
	IDL_tree raises_expr = IDL_OP_DCL (tree).raises_expr;
	CORBA_unsigned_long i;

	/* Start TODO: Replace */
	CORBA_OperationDescription *opd = g_new0(CORBA_OperationDescription, 1);
	opd->name = g_strdup(IDL_IDENT(ident).str);
	opd->id = g_strdup(IDL_IDENT_REPO_ID(ident));
	d_message(3, "do_operation: %s (%s)", opd->name, opd->id);
	/* End TODO */

	/* Set the return type for this function */
	if (op_type_spec) {
		opd->result = get_typecode(op_type_spec);
	} else {
		opd->result = dupe_tc(TC_void);
	}
	
	opd->mode = f_oneway ? CORBA_OP_ONEWAY : CORBA_OP_NORMAL;

	opd->parameters._length = IDL_list_length(dcls);
	opd->parameters._buffer = 
		CORBA_sequence_CORBA_ParameterDescription_allocbuf(IDL_list_length(dcls));


	for (i = 0; i < opd->parameters._length; i++) {
		CORBA_ParameterDescription *par_desc = &opd->parameters._buffer[i];
		IDL_tree dcl = IDL_LIST(dcls).data;
		enum IDL_param_attr attr;

		par_desc->name = IDL_IDENT(IDL_PARAM_DCL(dcl).simple_declarator).str;
		par_desc->type = get_typecode(IDL_PARAM_DCL(dcl).param_type_spec);
		attr = IDL_PARAM_DCL(dcl).attr;
		if (attr == IDL_PARAM_IN) par_desc->mode = CORBA_PARAM_IN;
		else if (attr == IDL_PARAM_OUT) par_desc->mode = CORBA_PARAM_OUT;
		else if (attr == IDL_PARAM_INOUT) par_desc->mode = CORBA_PARAM_INOUT;
		par_desc->type_def = CORBA_OBJECT_NIL;
		
		dcls = IDL_LIST(dcls).next;
		d_message(3, "do_operation: parameter %s (%s)", par_desc->name,
		          par_desc->type->name);
	}

	/* handle exception stuff */
	opd->exceptions._length = IDL_list_length(raises_expr);
	opd->exceptions._buffer =
            CORBA_sequence_CORBA_ExceptionDescription_allocbuf(
	                      opd->exceptions._length);
	opd->exceptions._release = TRUE;
	for (i = 0; i < opd->exceptions._length; i++) {
		CORBA_ExceptionDescription *exd = &opd->exceptions._buffer[i];
		IDL_tree ref = IDL_LIST(raises_expr).data;
		exd->type = get_ident_typecode(ref);
		if (exd->type->kind != CORBA_tk_except)
			d_error("Raises non-exception");
		exd->id = g_strdup(exd->type->repo_id);
		exd->name = exd->defined_in = exd->version = NULL;
		raises_expr = IDL_LIST(raises_expr).next;
	}
	/* done */
	
	return opd;	
}

static GSList *
do_attribute (IDL_tree tree)
{
	CORBA_AttributeDescription *attr_desc;
	gboolean f_readonly = IDL_ATTR_DCL(tree).f_readonly;
	GSList *result = NULL;
	IDL_tree pts = IDL_ATTR_DCL(tree).param_type_spec;
	IDL_tree simple_decls = IDL_ATTR_DCL(tree).simple_declarations;

	while (simple_decls) {
		IDL_tree ident = IDL_LIST(simple_decls).data;
		attr_desc = g_new0(CORBA_AttributeDescription, 1);
		attr_desc->name = g_strdup(IDL_IDENT(ident).str);
		attr_desc->id = g_strdup(IDL_IDENT_REPO_ID(ident));
		attr_desc->type = get_typecode(pts);
		attr_desc->mode = f_readonly ? CORBA_ATTR_READONLY : CORBA_ATTR_NORMAL;
		result = g_slist_prepend(result, attr_desc);

		simple_decls = IDL_LIST(simple_decls).next;
	}
	return result;
}

static void
do_exception (IDL_tree tree)
{
	PyObject *except;
	IDL_tree ident = IDL_TYPE_ENUM(tree).ident;
	char *full_name = IDL_ns_ident_to_qstring(ident, ".", 0),
	     *repo_id = IDL_IDENT_REPO_ID(ident);

	/* Is this exception done? */
	if (find_typecode(repo_id)) { 
		PyObject *except = g_hash_table_lookup(exceptions, repo_id);
		if (except) {
			add_object_to_hierarchy(tree, except, NULL, FALSE, FALSE);
		}
		return;
	}

	if (!strstr(full_name, ".")) {
		full_name = g_strconcat(".", full_name, "\n", NULL);
	} else {
		full_name = g_strdup(full_name);
	}
	
	except = PyErr_NewException(full_name, OPExc_UserException, NULL);
	if (except) {
		PyMethodDef *def = g_new(PyMethodDef, 1);
		PyObject *func, *meth;
		def->ml_name = g_strdup("__init__");
		def->ml_meth = (PyCFunction)UserExcept_PyClass__init;
		def->ml_flags = METH_VARARGS | METH_KEYWORDS;
		func = PyCFunction_New(def, except);
		meth = PyMethod_New(func, NULL, except);
		PyObject_SetAttrString(except, "__init__", meth);

		def = g_new(PyMethodDef, 1);
		def->ml_name = g_strdup("__str__");
		def->ml_meth = (PyCFunction)UserExcept_PyClass__str;
		def->ml_flags = METH_VARARGS | METH_KEYWORDS;
		func = PyCFunction_New(def, except);
		meth = PyMethod_New(func, NULL, except);
		PyObject_SetAttrString(except, "__str__", meth);

	}

	/* Hopefully this shouldn't happen! */
	if (PyErr_Occurred())  {
		d_warning(0, "Error in except: %s, %s", repo_id, full_name);
		PyErr_Print();
		d_error("bail");
	}
	
	g_hash_table_insert(exceptions, repo_id, except);
	PyObject_SetAttrString(except, "__repo_id", PyString_FromString(repo_id));
	add_object_to_hierarchy(tree, except, NULL, FALSE, FALSE);
	g_free(full_name);
}

/* TODO: give do_struct and do_exception a common backend */
static void
do_struct (IDL_tree tree)
{
	IDL_tree ident = IDL_TYPE_ENUM(tree).ident;
	char *repo_id = IDL_IDENT_REPO_ID(ident);
	PyObject *cl_name, *cl;

	/* Is this strict struct done? */
	if (find_typecode(repo_id)) { 
		cl = g_hash_table_lookup(object_glue, repo_id);
		if (cl) {
			add_object_to_hierarchy(tree, cl, NULL, FALSE, FALSE);
		}
		
		return;
	}

	/* SECOND VERSION */
	cl_name = PyString_FromString(IDL_IDENT(ident).str);
	cl = PyClass_New(NULL, PyDict_New(), cl_name);
	
	/* XXX: Find out why I get:
	 * TypeError: attribute-less object (assign or del)
	 */
	PyErr_Clear();

	if (cl) {
	   PyMethodDef *def = g_new(PyMethodDef, 1);
		PyObject *func, *meth;
	   def->ml_name = g_strdup("__init__");
	   def->ml_meth = (PyCFunction)UserExcept_PyClass__init;
	   def->ml_flags = METH_VARARGS | METH_KEYWORDS;
	   func = PyCFunction_New(def, cl);
		meth = PyMethod_New(func, NULL, cl);
		PyObject_SetAttrString(cl, "__init__", meth);
	}
	
	/* Hopefully this shouldn't happen! */
	if (PyErr_Occurred())  {
		PyErr_Print();
		return;
	}
	
	g_hash_table_insert(object_glue, repo_id, cl);
	PyObject_SetAttrString(cl, "__repo_id", PyString_FromString(repo_id));
	add_object_to_hierarchy(tree, cl, NULL, FALSE, FALSE);
}

static void
do_enum (IDL_tree tree)
{
	IDL_tree elist = IDL_TYPE_ENUM(tree).enumerator_list;
	PyObject *tuple;
	CORBA_unsigned_long i;
	IDL_tree tmp;

	/* Figure out how many members */
	i = 0;
	tmp = elist;
	while (tmp) {
		i++;
		tmp = IDL_LIST(tmp).next;
	}
	tuple = PyTuple_New(i);

	tmp = elist;
	i = 0;
	while (tmp) {
		IDL_tree ident = IDL_LIST(tmp).data;
		PyObject *v = PyInt_FromLong(i);
		add_object_to_hierarchy(tmp, v, NULL, FALSE, FALSE);
		PyTuple_SetItem(tuple, i++, PyString_FromString(IDL_IDENT(ident).str));
		tmp = IDL_LIST(tmp).next;
	}
	add_object_to_hierarchy(IDL_NODE_UP(elist), tuple, NULL, FALSE, FALSE);
}
	
static void
do_union (IDL_tree tree)
{
	IDL_tree ident = IDL_TYPE_ENUM(tree).ident;
	char *repo_id = IDL_IDENT_REPO_ID(ident);
	PyObject *cl_name, *cl;

	/* Is this union done? */
	if (find_typecode(repo_id)) { 
		cl = g_hash_table_lookup(object_glue, repo_id);
		if (cl)
			add_object_to_hierarchy(tree, cl, NULL, FALSE, FALSE);
		return;
	}

	cl_name = PyString_FromString(IDL_IDENT(ident).str);
	cl = PyClass_New(NULL, PyDict_New(), cl_name);
	
	/* XXX: Find out why I get:
	 * TypeError: attribute-less object (assign or del)
	 */
	PyErr_Clear();

	if (cl) {
	   PyMethodDef *def = g_new(PyMethodDef, 1);
		PyObject *func, *meth;
	   def->ml_name = g_strdup("__init__");
	   def->ml_meth = (PyCFunction)Union_PyClass__init;
	   def->ml_flags = METH_VARARGS;
	   func = PyCFunction_New(def, cl);
		meth = PyMethod_New(func, NULL, cl);
		PyObject_SetAttrString(cl, "__init__", meth);
	}
	
	/* Hopefully this shouldn't happen! */
	if (PyErr_Occurred())  {
		PyErr_Print();
		return;
	}
	
	g_hash_table_insert(object_glue, repo_id, cl);
	PyObject_SetAttrString(cl, "__repo_id", PyString_FromString(repo_id));
	add_object_to_hierarchy(tree, cl, NULL, FALSE, FALSE);
}

static void
do_const (IDL_tree tree)	
{
	CORBA_TypeCode tc;
	IDL_tree value = IDL_CONST_DCL(tree).const_exp;
	PyObject *o = NULL;

	tc = get_typecode (IDL_CONST_DCL(tree).const_type);

	switch (tc->kind) {
		case CORBA_tk_long:
		case CORBA_tk_ulong:
		case CORBA_tk_short:
		case CORBA_tk_ushort:
		case CORBA_tk_ulonglong:
			o = PyInt_FromLong(IDL_INTEGER(value).value);
			break;
		case CORBA_tk_boolean:
			o = PyInt_FromLong(IDL_BOOLEAN(value).value);
			break;
		case CORBA_tk_char:
			o = Py_BuildValue("c", IDL_CHAR(value).value);
			break;
		case CORBA_tk_float:
		case CORBA_tk_double:
			o = PyFloat_FromDouble(IDL_FLOAT(value).value);
			break;
		case CORBA_tk_string:
			o = PyString_FromString(IDL_STRING(value).value);
			break;

		case CORBA_tk_longdouble:
		case CORBA_tk_wchar:
		case CORBA_tk_fixed:
		case CORBA_tk_wstring:
		default:
			d_warning(0, "Unsupported constant type: %d", tc->kind);
			break;
	}

	if (o) {
		add_object_to_hierarchy(tree, o, IDL_CONST_DCL(tree).ident, FALSE, FALSE);
	}
	CORBA_Object_release((CORBA_Object)tc, NULL);
}

gboolean
tree_pre_func (IDL_tree_func_data *tfd,
	       gpointer user_data)
{
	IDL_tree tree = tfd->tree;
	
/*	g_message("%s: walking %d", __FUNCTION__, IDL_NODE_TYPE(tree)); */
	
	if (PyErr_Occurred()) {
		PyErr_Print();
	}
	
	switch (IDL_NODE_TYPE(tree)) {
		case IDLN_LIST:
			return TRUE;

		case IDLN_OP_DCL:
		{
			InterfaceData *idata = (InterfaceData *)tfd->up->up->data;
			idata->ops = g_slist_prepend(idata->ops, do_operation(tfd->tree));
			return FALSE;
		}
		case IDLN_ATTR_DCL:
		{
			InterfaceData *idata = (InterfaceData *)tfd->up->up->data;
			idata->attrs = g_slist_concat(do_attribute(tfd->tree), idata->attrs);
			return FALSE;
		}

		case IDLN_INTERFACE:
		{
			InterfaceData *idata = g_new0(InterfaceData, 1);
			IDL_tree ident;
			char *class_str, *repo_id;
			PyObject *func, *meth, *class_name, *new_class, *bases;
			PyObject *cldict;
			PyMethodDef *def;

			tfd->data = idata;
			ident = IDL_INTERFACE(tree).ident;
			repo_id = IDL_IDENT(ident).repo_id;
			if (remove_typecode(repo_id)) {
				/* remove the old interface if it exists
				 * (for forward declarations)
				 */
				/* g_message("INTERFACE %s ALREADY EXISTS!", repo_id); */
			}
	
			/* POA for the server */
			class_str = IDL_IDENT(ident).str;
			class_name = PyString_FromString(class_str);
			bases = PyTuple_New(1);
			Py_INCREF(servant_base);
			PyTuple_SetItem(bases, 0, servant_base);
			cldict = PyDict_New();
			new_class = PyClass_New(bases, cldict, class_name);
			Py_DECREF(bases);
			
			/* constructor for POA class */
			def = g_new(PyMethodDef, 1);
			def->ml_name = g_strdup("__init__");
			def->ml_meth = (PyCFunction)Servant_PyClass__init;
			def->ml_flags = METH_VARARGS;
			func = PyCFunction_New(def, new_class);
			meth = PyMethod_New(func, NULL, new_class);
			PyObject_SetAttrString(new_class, "__init__", meth);
			
			/* _this() method */
			def = g_new(PyMethodDef, 1);
			def->ml_name = g_strdup("_this");
			def->ml_meth = (PyCFunction)Servant_PyClass__this;
			
			def->ml_flags = METH_VARARGS;
			func = PyCFunction_New(def, new_class);
			meth = PyMethod_New(func, NULL, new_class);
			/*meth = PyMethod_New(func, NULL, new_class); */
			PyObject_SetAttrString(new_class, "_this", meth);
			
			/* _default_POA() method */
			def = g_new(PyMethodDef, 1);
			def->ml_name = g_strdup("_default_POA");
			def->ml_meth = (PyCFunction)Servant_PyClass__default_POA;
			def->ml_flags = METH_VARARGS;
			func = PyCFunction_New(def, new_class);
			meth = PyMethod_New(func, NULL, new_class);
			PyObject_SetAttrString(new_class, "_default_POA", meth);

			/* destructor */
			def = g_new(PyMethodDef, 1);
			def->ml_name = g_strdup("__del__");
			def->ml_meth = (PyCFunction)Servant_PyClass__del;
			def->ml_flags = METH_VARARGS;
			func = PyCFunction_New(def, new_class);
			meth = PyMethod_New(func, NULL, new_class);
			PyObject_SetAttrString(new_class, "__del__", meth);

			idata->poa_class = new_class;
			add_object_to_hierarchy(tree, new_class, NULL, TRUE, FALSE);


			class_name = PyString_FromString(class_str);
			new_class = PyClass_New(NULL, PyDict_New(), class_name);
/*			PyObject_SetAttrString(new_class, "__module__", cur_client_module); */
			add_object_to_hierarchy(tree, new_class, NULL, FALSE, FALSE);
			g_hash_table_insert(stub_repo_ids, new_class, repo_id);

			/* store the typecode for this interface */
			CORBA_Object_release((CORBA_Object)get_interface_typecode(tree), NULL);
		
			return TRUE;
		}

		case IDLN_MODULE:
		{
			IDL_tree ident = IDL_MODULE(tree).ident;
			char *p = IDL_IDENT(ident).str, *repo_id = IDL_IDENT(ident).repo_id,
			     *q, *module_name;
			PyObject *o;

			if (find_typecode(repo_id)) {
				return TRUE;
			}
			
			/* construct the name for the POA module */
			p = IDL_ns_ident_to_qstring(ident, ".", 0);
			if ((q = strstr(p, ".")) == 0) 
				module_name = g_strconcat(p, "__POA", NULL);
			else {
				*q = 0;
				module_name = g_strconcat(p, "__POA.", q + 1, NULL);
				*q = '.';
			}
			o = Py_InitModule(module_name, module_methods);
			g_free(module_name);
			add_object_to_hierarchy(tree, o, NULL, TRUE, TRUE);

			/* client module */
			o = Py_InitModule(p, module_methods);
			add_object_to_hierarchy(tree, o, NULL, FALSE, TRUE);
			g_free(p);
			CORBA_Object_release((CORBA_Object)get_typecode(tree), NULL);
			return TRUE;
		}

		case IDLN_EXCEPT_DCL:
			do_exception(tfd->tree);
			return FALSE;

		case IDLN_TYPE_STRUCT:
			do_struct(tfd->tree);
			return FALSE;

		case IDLN_TYPE_ENUM:
			do_enum(tfd->tree);
			return FALSE;

		case IDLN_TYPE_UNION:
			do_union(tfd->tree);
			CORBA_Object_release((CORBA_Object)get_union_typecode(tree), NULL);
			return FALSE;

		case IDLN_CONST_DCL:
			do_const(tfd->tree);
			return FALSE;

		case IDLN_CODEFRAG:
			/* #pragmas is IDL_CODEFRAG(tree)->list (GSList*) */
			return TRUE;
		default:
			return FALSE;
			
	}
}

gboolean tree_post_func(IDL_tree_func_data *tfd, gpointer user_data)
{
	IDL_tree tree = tfd->tree;
	switch(IDL_NODE_TYPE(tree)) {
		case IDLN_INTERFACE:
		{
			construct_interface(tree, (InterfaceData *)tfd->data);
			break;
		}
		default: 
			break;
			
	}
	return TRUE;
}

CORBA_TypeCode find_typecode(char *id)	
{
	if (type_codes) {
		CORBA_TypeCode r;
		r = (CORBA_TypeCode)g_hash_table_lookup(type_codes, id);
		if (r)
			return dupe_tc(r);
	}
	return NULL;
}

void
store_typecode (char *         id,
		CORBA_TypeCode tc)	
{
	g_hash_table_insert (type_codes, g_strdup(id), dupe_tc(tc));
}

gboolean remove_typecode(char *id)	
{
	CORBA_TypeCode r;
	gpointer key, value;
	if (g_hash_table_lookup_extended(type_codes, id, &key, &value)) {
		r = (CORBA_TypeCode)value;
		CORBA_Object_release((CORBA_Object)r, NULL);
		g_hash_table_remove(type_codes, id);
		g_free(key);
		return TRUE;
	}
	return FALSE;
}

void _find_repo_id_func(gpointer key, gpointer value, gpointer id)
{
	typedef struct {
		char *id;
		CORBA_TypeCode tc;
	} __tmp;
	__tmp *_tmp = (__tmp *)id;
	CORBA_TypeCode tc = (CORBA_TypeCode)value;
	switch (tc->kind) {
		case CORBA_tk_objref:
		case CORBA_tk_sequence:
		case CORBA_tk_array:
		case CORBA_tk_struct:
		case CORBA_tk_except:
		case CORBA_tk_union:
		case CORBA_tk_alias:
			if (tc == _tmp->tc)
				_tmp->id = (char *)key;
			break;
		default:
			/* Otherwise we just want to look at the kind of typecode
			 * (pedantic, because we handle the basic types explicitly
			 * before we get here.)
			 */
	   	if (tc->kind == _tmp->tc->kind)
				_tmp->id = (char *)key;
	}
}

char *
find_repo_id_from_typecode (CORBA_TypeCode tc)
{
	struct {
		char *id;
		CORBA_TypeCode tc;
	} _tmp = { NULL, tc };

	/* Typecode already has repo id defined, so use that. */
	if (tc->repo_id != 0 && strcmp((char *)tc->repo_id, "") != 0) {
		return (char *)tc->repo_id;
	}

	/* Handle the basic types
	 * Surely this could be handled more elegantly:
	 */
	#define BASIC_TYPE(type, str) case type: return str;
	switch(tc->kind) {
		BASIC_TYPE(CORBA_tk_short, "IDL:CORBA/Short:1.0");
		BASIC_TYPE(CORBA_tk_ushort, "IDL:CORBA/UShort:1.0");
		BASIC_TYPE(CORBA_tk_long, "IDL:CORBA/Long:1.0");
		BASIC_TYPE(CORBA_tk_ulong, "IDL:CORBA/ULong:1.0");
		BASIC_TYPE(CORBA_tk_longlong, "IDL:CORBA/LongLong:1.0");
		BASIC_TYPE(CORBA_tk_ulonglong, "IDL:CORBA/ULongLong:1.0");
		BASIC_TYPE(CORBA_tk_float, "IDL:CORBA/Float:1.0");
		BASIC_TYPE(CORBA_tk_double, "IDL:CORBA/Double:1.0");
		BASIC_TYPE(CORBA_tk_longdouble, "IDL:CORBA/LongDouble:1.0");
		BASIC_TYPE(CORBA_tk_boolean, "IDL:CORBA/Boolean:1.0");
		BASIC_TYPE(CORBA_tk_char, "IDL:CORBA/Char:1.0");
		BASIC_TYPE(CORBA_tk_wchar, "IDL:CORBA/WChar:1.0");
		BASIC_TYPE(CORBA_tk_octet, "IDL:CORBA/Octet:1.0");
		BASIC_TYPE(CORBA_tk_string, "IDL:CORBA/String:1.0");
		BASIC_TYPE(CORBA_tk_wstring, "IDL:CORBA/WString:1.0");
		BASIC_TYPE(CORBA_tk_TypeCode, "IDL:CORBA/TypeCode:1.0");
		BASIC_TYPE(CORBA_tk_any, "IDL:CORBA/Any:1.0");
		BASIC_TYPE(CORBA_tk_Principal, "IDL:CORBA/Principal:1.0");
		default: break;
	}

	g_hash_table_foreach(type_codes, _find_repo_id_func, &_tmp);

	/* If we're looking for an object reference and the search came up
	 * empty (so _tmp.id is null), we'll return IDL:CORBA/Object:1.0.
	 * We lookup the address of the string and return that.
	 */
	if (!_tmp.id && tc->kind == CORBA_tk_objref) {
		gpointer key, value;
		g_hash_table_lookup_extended(type_codes, "IDL:CORBA/Object:1.0",
		                             &key, &value);
		return key;
	}
	return _tmp.id;
}
	

static CORBA_TypeCode
alloc_typecode (void)
{
	CORBA_TypeCode tc;

	tc = g_new0 (struct CORBA_TypeCode_struct, 1);
	
        ORBit_RootObject_init (&tc->parent, &ORBit_TypeCode_epv);
	
        return ORBit_RootObject_duplicate (tc);
}

static CORBA_TypeCode
get_integer_typecode (IDL_tree tree)
{
	gboolean f_signed = IDL_TYPE_INTEGER(tree).f_signed;
	enum IDL_integer_type f_type = IDL_TYPE_INTEGER(tree).f_type;

	if (f_signed) {
		switch(f_type) {
			case IDL_INTEGER_TYPE_SHORT:
				return dupe_tc(TC_CORBA_short);
			case IDL_INTEGER_TYPE_LONG:
				return dupe_tc(TC_CORBA_long);
			case IDL_INTEGER_TYPE_LONGLONG:
				return dupe_tc(TC_CORBA_long_long);
		}
	} else {
		switch(f_type) {
			case IDL_INTEGER_TYPE_SHORT:
				return dupe_tc(TC_CORBA_unsigned_short);
			case IDL_INTEGER_TYPE_LONG:
				return dupe_tc(TC_CORBA_unsigned_long);
			case IDL_INTEGER_TYPE_LONGLONG:
				return dupe_tc(TC_CORBA_unsigned_long_long);
		}
	}
	g_assert_not_reached();
	return NULL;
}

static CORBA_TypeCode
get_string_typecode (IDL_tree tree)
{
	IDL_tree pic = IDL_TYPE_STRING(tree).positive_int_const;
	CORBA_TypeCode res = alloc_typecode();
	res->kind = CORBA_tk_string;
	res->name = "string";
	if (pic)
		res->length = IDL_INTEGER(pic).value;
	else 
		res->length = 0;

	return res;
}

static CORBA_TypeCode
get_wstring_typecode (IDL_tree tree)	
{
	IDL_tree pic = IDL_TYPE_WIDE_STRING(tree).positive_int_const;
	CORBA_TypeCode res = alloc_typecode();
	res->kind = CORBA_tk_wstring;
	res->name = "wstring";
	
	if (pic) {
		res->length = IDL_INTEGER(pic).value;
	} else {
		res->length = 0;
	}
	
	return res;
}

static CORBA_TypeCode
get_declarator_typecode (IDL_tree tree, CORBA_TypeCode base_tc)
{
	if (IDL_NODE_TYPE(tree) == IDLN_TYPE_ARRAY) {
		IDL_tree size_list = IDL_TYPE_ARRAY(tree).size_list;
		IDL_tree tmp_list;
		CORBA_TypeCode res = dupe_tc(base_tc);
		CORBA_TypeCode child_tc;

		tmp_list = IDL_LIST(size_list)._tail;
		while (tmp_list) {
			IDL_tree size = IDL_LIST(tmp_list).data;
			child_tc = res;
			res = alloc_typecode();
			res->kind = CORBA_tk_array;
			res->length = IDL_INTEGER(size).value;
			res->sub_parts = 1;
			res->subtypes = g_new(CORBA_TypeCode, 1);
			res->subtypes[0] = child_tc;

			tmp_list = IDL_LIST(tmp_list).prev;
		}
		return res;
	}
	else if (IDL_NODE_TYPE(tree) == IDLN_IDENT) {
		return dupe_tc(base_tc);
	} else {
		d_warning(0, "get_declarator_typecode() needs ident or array.");
	}
	
	return NULL;
}

static gchar *
get_declarator_name (IDL_tree tree)	
{	
	if (IDL_NODE_TYPE(tree) == IDLN_TYPE_ARRAY) {
		return g_strdup(IDL_IDENT(IDL_TYPE_ARRAY(tree).ident).str);
	} else if (IDL_NODE_TYPE(tree) == IDLN_IDENT) {
		return g_strdup(IDL_IDENT(tree).str);
	} else {
		d_warning(0, "get_declarator_name() needs ident or array.");
	}
	
	return NULL;
}

static CORBA_TypeCode
get_ident_typecode (IDL_tree tree)	
{
	IDL_tree parent;
	CORBA_TypeCode res;
	char *repo_id;

	repo_id = IDL_IDENT_REPO_ID(tree);
	res = find_typecode(repo_id);
	if (res) {
		return res;
	}
	
	parent = IDL_NODE_UP(tree);
	switch (IDL_NODE_TYPE(parent)) {
		case IDLN_TYPE_ENUM:
		case IDLN_EXCEPT_DCL:
		case IDLN_INTERFACE:
		case IDLN_TYPE_STRUCT:
		case IDLN_TYPE_UNION:
		case IDLN_FORWARD_DCL:
			return get_typecode(parent);

		case IDLN_TYPE_ARRAY:
		{
			IDL_tree list;
			IDL_tree dcl;
			CORBA_TypeCode base_tc;

			g_assert(IDL_NODE_TYPE(IDL_NODE_UP(parent)) == IDLN_LIST);
			list = IDL_NODE_UP(parent);
			g_assert(IDL_NODE_TYPE(IDL_NODE_UP(list)) == IDLN_TYPE_DCL);
			dcl = IDL_NODE_UP(list);
			base_tc = get_typecode(IDL_TYPE_DCL(dcl).type_spec);
			res = get_declarator_typecode(parent, base_tc);
			CORBA_Object_release((CORBA_Object)base_tc, NULL);
			store_typecode(repo_id, res);
			return res;
		}
		case IDLN_LIST:
		{
			IDL_tree dcl;
			g_assert(IDL_NODE_TYPE(IDL_NODE_UP(parent)) == IDLN_TYPE_DCL);
			dcl = IDL_NODE_UP(parent);
			res = get_typecode(IDL_TYPE_DCL(dcl).type_spec);
			store_typecode(repo_id, res);
			return res;
		}
		default:
			d_warning(0, "Reference to node type %s is invalid",
			          IDL_NODE_TYPE_NAME(parent));
	}
	g_assert_not_reached();
	return NULL;
}

static CORBA_TypeCode
get_exception_typecode (IDL_tree tree)	
{
	IDL_tree ident = IDL_EXCEPT_DCL(tree).ident;
	IDL_tree members = IDL_EXCEPT_DCL(tree).members;
	IDL_tree tmp1;
	char *repo_id = IDL_IDENT_REPO_ID(ident);
	CORBA_unsigned_long i = 0;

	CORBA_TypeCode res = find_typecode(repo_id);
	if (res)
		return res;
	res = alloc_typecode();
	res->kind = CORBA_tk_except;
	res->repo_id = g_strdup(repo_id);
	res->name = g_strdup(IDL_IDENT(ident).str);

	/* Figure out how many members we have */
	res->sub_parts = 0;
	tmp1 = members;
	while (tmp1) {
		IDL_tree member = IDL_LIST(tmp1).data;
		IDL_tree dcls = IDL_MEMBER(member).dcls;
		res->sub_parts += IDL_list_length(dcls);
		tmp1 = IDL_LIST(tmp1).next;
	}
	
	/* Allocate memory for these members. */
	res->subnames = g_new(gchar *, res->sub_parts);
	res->subtypes = g_new(CORBA_TypeCode, res->sub_parts);
	
	/* Now fill in the blanks */
	tmp1 = members;
	while (tmp1) {
		IDL_tree member = IDL_LIST(tmp1).data;
		IDL_tree type_spec = IDL_MEMBER(member).type_spec;
		IDL_tree dcls = IDL_MEMBER(member).dcls;
		CORBA_TypeCode base_tc = get_typecode(type_spec);

		IDL_tree tmp2 = dcls;
		while (tmp2) {
			IDL_tree dcl = IDL_LIST(tmp2).data;
			res->subnames[i] = get_declarator_name(dcl);
			res->subtypes[i++] = get_declarator_typecode(dcl, base_tc);
			tmp2 = IDL_LIST(tmp2).next;
		}
		CORBA_Object_release((CORBA_Object)base_tc, NULL);
		tmp1 = IDL_LIST(tmp1).next;
	}
	store_typecode(repo_id, res);
	return res;
}

static CORBA_TypeCode
get_interface_typecode (IDL_tree tree)
{
	IDL_tree ident = IDL_INTERFACE(tree).ident;
	CORBA_TypeCode res;
	char *repo_id = IDL_IDENT_REPO_ID(ident);

	res = find_typecode(repo_id);
	if (res)
		return res;
	res = alloc_typecode();
	res->kind = CORBA_tk_objref;
	res->repo_id = g_strdup(repo_id);
	res->name = g_strdup(IDL_IDENT(ident).str);
	store_typecode(repo_id, res);
	return res;
}

static CORBA_TypeCode
get_forward_dcl_typecode (IDL_tree tree)	
{
	IDL_tree ident = IDL_FORWARD_DCL(tree).ident;
	CORBA_TypeCode res;
	char *repo_id = IDL_IDENT_REPO_ID(ident);

	res = find_typecode(repo_id);
	if (res)
		return res;
	res = alloc_typecode();
	res->kind = CORBA_tk_objref;
	res->repo_id = g_strdup(repo_id);
	res->name = g_strdup(IDL_IDENT(ident).str);
	store_typecode(repo_id, res);
	return res;
}

static CORBA_TypeCode
get_struct_typecode (IDL_tree tree)	
{
	IDL_tree ident = IDL_TYPE_STRUCT(tree).ident,
	         members = IDL_TYPE_STRUCT(tree).member_list, tmp1;
	char *repo_id = IDL_IDENT_REPO_ID(ident);
	CORBA_unsigned_long i = 0;

	CORBA_TypeCode res = find_typecode(repo_id);
	if (res)
		return res;
	res = alloc_typecode();
	res->kind = CORBA_tk_struct;
	res->repo_id = g_strdup(repo_id);
	res->name = g_strdup(IDL_IDENT(ident).str);

	/* determine number of members */
	res->sub_parts = 0;
	tmp1 = members;
	while (tmp1) {
		IDL_tree member = IDL_LIST(tmp1).data;
		IDL_tree dcls = IDL_MEMBER(member).dcls;
		res->sub_parts += IDL_list_length(dcls);
		tmp1 = IDL_LIST(tmp1).next;
	}
	res->subnames = g_new(gchar *, res->sub_parts);
	res->subtypes = g_new(CORBA_TypeCode, res->sub_parts);
	
	tmp1 = members;
	while (tmp1) {
		IDL_tree member = IDL_LIST(tmp1).data;
		IDL_tree type_spec = IDL_MEMBER(member).type_spec;
		IDL_tree dcls = IDL_MEMBER(member).dcls;
		CORBA_TypeCode base_tc = get_typecode(type_spec);

		IDL_tree tmp2 = dcls;
		while (tmp2) {
			IDL_tree dcl = IDL_LIST(tmp2).data;
			res->subnames[i] = get_declarator_name(dcl);
			res->subtypes[i++] = get_declarator_typecode(dcl, base_tc);
			tmp2 = IDL_LIST(tmp2).next;
		}
		CORBA_Object_release((CORBA_Object)base_tc, NULL);
		tmp1 = IDL_LIST(tmp1).next;
	}
	store_typecode(repo_id, res);
	return res;
}

static CORBA_TypeCode
get_sequence_typecode (IDL_tree tree)	
{
	IDL_tree spec = IDL_TYPE_SEQUENCE(tree).simple_type_spec;
	IDL_tree pic = IDL_TYPE_SEQUENCE(tree).positive_int_const;

	CORBA_TypeCode res = alloc_typecode();
	res->kind = CORBA_tk_sequence;
	res->sub_parts = 1;
	res->subtypes = g_new(CORBA_TypeCode, 1);
	res->subtypes[0] = get_typecode(spec);
	res->name = "sequence";
		
	if (pic)
		res->length = IDL_INTEGER(pic).value;
	else
		res->length = 0;
	return res;
}

static CORBA_TypeCode
get_enum_typecode (IDL_tree tree)	
{
	IDL_tree ident = IDL_TYPE_ENUM(tree).ident,
	         elist = IDL_TYPE_ENUM(tree).enumerator_list, tmp;
	char *repo_id = IDL_IDENT_REPO_ID(ident);
	CORBA_unsigned_long i = 0;

	CORBA_TypeCode res = find_typecode(repo_id);
	if (res)
		return res;

	res = alloc_typecode();
	res->name = g_strdup(IDL_IDENT(ident).str);
	res->kind = CORBA_tk_enum;
	res->repo_id = g_strdup(repo_id);

	/* Figure out how many members */
	res->sub_parts = 0;
	tmp = elist;
	while (tmp) {
		res->sub_parts++;
		tmp = IDL_LIST(tmp).next;
	}
	
	res->subnames = g_new(gchar *, res->sub_parts);
	tmp = elist;
	while (tmp) {
		ident = IDL_LIST(tmp).data;
		res->subnames[i++] = g_strdup(IDL_IDENT(ident).str);
		tmp = IDL_LIST(tmp).next;
	}
	store_typecode(repo_id, res);
	return res;
}

static int
enumerator_index (IDL_tree label)	
{
	IDL_tree tmp = IDL_NODE_UP(label);
	int i = 0;
	do {
		tmp = IDL_LIST(tmp).prev;
		i++;
	} while (tmp);
	return i - 1;
}

static CORBA_TypeCode
get_union_typecode (IDL_tree tree)	
{
	IDL_tree ident = IDL_TYPE_UNION(tree).ident;
	IDL_tree switch_type = IDL_TYPE_UNION(tree).switch_type_spec;
	IDL_tree switch_body = IDL_TYPE_UNION(tree).switch_body, tmp1;
	CORBA_unsigned_long i = 0;

	char *repo_id = IDL_IDENT_REPO_ID(ident);
	CORBA_TypeCode res = find_typecode(repo_id);
	if (res)
		return res;

	res = alloc_typecode();
	res->kind = CORBA_tk_union;
	res->repo_id = g_strdup(repo_id);
	res->name = g_strdup(IDL_IDENT(ident).str);

	/* count arms */
	res->sub_parts = 0;
	tmp1 = switch_body;
	while (tmp1) {
		IDL_tree case_stmt = IDL_LIST(tmp1).data;
		IDL_tree labels = IDL_CASE_STMT(case_stmt).labels;
		gint length = 0;

		IDL_tree tmp2 = labels;
		while (tmp2) {
			if (!IDL_LIST(tmp2).data) {
				if (!IDL_LIST(tmp2).prev && !IDL_LIST(tmp2).next)
					length++;
			}
			else length++;
			tmp2 = IDL_LIST(tmp2).next;
		}
		res->sub_parts += length;
		tmp1 = IDL_LIST(tmp1).next;
	}

	res->subnames = g_new(gchar *, res->sub_parts);
	res->subtypes = g_new(CORBA_TypeCode, res->sub_parts);
	res->sublabels = g_new(CORBA_long, res->sub_parts);
	res->default_index = -1;
	res->discriminator = get_typecode(switch_type);

	tmp1 = switch_body;
	while (tmp1) {
		IDL_tree case_stmt = IDL_LIST(tmp1).data;
		IDL_tree labels = IDL_CASE_STMT(case_stmt).labels,
		         element_spec = IDL_CASE_STMT(case_stmt).element_spec,
		         type_spec = IDL_MEMBER(element_spec).type_spec,
		         dcls = IDL_MEMBER(element_spec).dcls,
		         declarator = IDL_LIST(dcls).data;

		IDL_tree tmp2 = labels;
		while (tmp2) {
			IDL_tree label = IDL_LIST(tmp2).data;
			if (!label) {
				res->default_index = i;
				if (IDL_LIST(tmp2).prev || IDL_LIST(tmp2).next) {
					tmp2 = IDL_LIST(tmp2).next;
					continue;
				}
			}
			res->subnames[i] = get_declarator_name(declarator);
			res->subtypes[i] = get_declarator_typecode(declarator,
			                                           get_typecode(type_spec));
			if (!label) {
				CORBA_octet val;
				res->sublabels[i] = (CORBA_long) val;
			}
			else {
				switch (res->discriminator->kind) {
					C_M(CORBA_tk_enum, CORBA_long, enumerator_index(label));
					C_M(CORBA_tk_long, CORBA_long, IDL_INTEGER(label).value);
					C_M(CORBA_tk_ulong, CORBA_unsigned_long, 
					    IDL_INTEGER(label).value);
					C_M(CORBA_tk_boolean, CORBA_boolean, IDL_INTEGER(label).value);
					C_M(CORBA_tk_char, CORBA_char, *IDL_CHAR(label).value);
					C_M(CORBA_tk_short, CORBA_short, IDL_INTEGER(label).value);
					C_M(CORBA_tk_ushort, CORBA_unsigned_short,
					    IDL_INTEGER(label).value);
					C_M(CORBA_tk_longlong, CORBA_long_long, 
					    IDL_INTEGER(label).value);
					C_M(CORBA_tk_ulonglong, CORBA_unsigned_long_long,
					    IDL_INTEGER(label).value);
					default:
						d_error("Bad union discriminator type %d",
						          res->discriminator->kind);
				}
			}
			tmp2 = IDL_LIST(tmp2).next;
			i++;
		}
		tmp1 = IDL_LIST(tmp1).next;
	}
	store_typecode(repo_id, res);
	return res;
}

CORBA_TypeCode get_float_typecode(IDL_tree tree)	
{
	enum IDL_float_type f_type = IDL_TYPE_FLOAT(tree).f_type;
	switch (f_type) {
		case IDL_FLOAT_TYPE_FLOAT:
			return dupe_tc(TC_CORBA_float);
		case IDL_FLOAT_TYPE_DOUBLE:
			return dupe_tc(TC_CORBA_double);
		case IDL_FLOAT_TYPE_LONGDOUBLE:
			return dupe_tc(TC_CORBA_long_double);
	}
	g_assert_not_reached();
	return NULL;
}

/* this doesn't actually do anything (no such thing as TypeCodes for
 * modules.  It's just a convenient way to keep track of what IDL modules
 * have been processed.
 */
CORBA_TypeCode
get_module_typecode (IDL_tree tree)	
{
	IDL_tree ident = IDL_INTERFACE(tree).ident;
	CORBA_TypeCode res;
	char *repo_id = IDL_IDENT_REPO_ID(ident);

	res = find_typecode(repo_id);
	if (res) {
		return res;
	}
	
	res = alloc_typecode();
	store_typecode(repo_id, res);
	
	return res;
}	
	
CORBA_TypeCode
get_typecode (IDL_tree tree)	
{
	switch (IDL_NODE_TYPE(tree)) {
		case IDLN_TYPE_ANY:
			return dupe_tc(TC_CORBA_any);
		case IDLN_TYPE_CHAR:
			return dupe_tc(TC_CORBA_char);
		case IDLN_TYPE_BOOLEAN:
			return dupe_tc(TC_CORBA_boolean);
		case IDLN_TYPE_OBJECT:
			return dupe_tc(TC_CORBA_Object);
		case IDLN_TYPE_OCTET:
			return dupe_tc(TC_CORBA_octet);
		case IDLN_TYPE_TYPECODE:
			return dupe_tc(TC_CORBA_TypeCode);
		case IDLN_TYPE_WIDE_CHAR:
			return dupe_tc(TC_CORBA_wchar);

		case IDLN_TYPE_ENUM:
			return get_enum_typecode(tree);
		case IDLN_TYPE_STRUCT:
			return get_struct_typecode(tree);
		case IDLN_TYPE_STRING:
			return get_string_typecode(tree);
		case IDLN_TYPE_INTEGER:
			return get_integer_typecode(tree);
		case IDLN_EXCEPT_DCL:
			return get_exception_typecode(tree);
		case IDLN_IDENT:
			return get_ident_typecode(tree);
		case IDLN_INTERFACE:
			return get_interface_typecode(tree);
		case IDLN_FORWARD_DCL:
			return get_forward_dcl_typecode(tree);
		case IDLN_TYPE_SEQUENCE:
			return get_sequence_typecode(tree);
		case IDLN_TYPE_UNION:
			return get_union_typecode(tree);

		case IDLN_TYPE_FLOAT:
			return get_float_typecode(tree);
		case IDLN_TYPE_WIDE_STRING:
			return get_wstring_typecode(tree);
		case IDLN_MODULE:
			return get_module_typecode(tree);
		
		default:
			d_error ("Oops!  Typecode %s is Not Yet Implemented!", 
				 IDL_NODE_TYPE_NAME(tree));
	}
	return NULL;
}

/* Local Variables: */
/* c-file-style: "python" */
/* End: */
