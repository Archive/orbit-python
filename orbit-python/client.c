/*
 * Copyright (C) 2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com> 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include "client.h"

#include "demarshal.h"
#include "exceptions.h"
#include "orbit-python-types.h"
#include "server.h"
#include "util.h"

typedef struct {
	CORBA_unsigned_long len;
	char opname[1];
} OpNameData;

GPtrArray *
marshal_call (CORBA_Object                obj,
	      GIOPConnection             *con,
	      GIOPMessageQueueEntry      *mqe,
	      CORBA_unsigned_long         req_id,
	      CORBA_OperationDescription *opd,
	      PyObject                   *args)
 {
	 OpNameData *ond;
	 static struct iovec ovec;
	 GIOPSendBuffer *send_buffer = NULL;
	 GPtrArray *return_types = g_ptr_array_new ();
	 CORBA_unsigned_long i = 0, cur = 0;
	 CORBA_Environment ev;

	 {
		 int          align;
		 int          len = sizeof (CORBA_unsigned_long) + strlen(opd->name) + 1;
		 guchar      *header = alloca (len + sizeof (CORBA_unsigned_long));

		 *(CORBA_unsigned_long *) header = strlen(opd->name) + 1;
		 memcpy (header + sizeof (CORBA_unsigned_long),
			 opd->name, strlen(opd->name) + 1);

		 align = len + (sizeof (CORBA_unsigned_long) - 1);
		 align &= ~(sizeof (CORBA_unsigned_long) - 1);
		 memset (header + len, 0, align - len);

		 ovec.iov_len  = align;
		 ovec.iov_base = header;
	 }

	 send_buffer = giop_send_buffer_use_request (
		 con->giop_version,
		 req_id,
		 CORBA_TRUE,
		 obj->object_key,
		 &ovec,
		 NULL);

	 if (!send_buffer) {
		 /* FIXME */
		 raise_system_exception(OPExc_COMM_FAILURE, 0, CORBA_COMPLETED_NO, NULL);
		 goto exception;
	 }

	 /* Now we marshal and stuff the return types into an array for
	  * demarshalling.
	  */
	 if (opd->result->kind != CORBA_tk_null) {
		 g_ptr_array_add (return_types, opd->result);
	 }

	 CORBA_exception_init (&ev);

	 for (i = 0, cur = 0; i < opd->parameters._length; i++) {
		 PyObject *a;
		 CORBA_TypeCode tc = (CORBA_TypeCode)CORBA_Object_duplicate (
			 (CORBA_Object)opd->parameters._buffer[i].type, &ev);
		 switch (opd->parameters._buffer[i].mode) {
			 case CORBA_PARAM_IN:
				 a = PyTuple_GetItem(args, cur);
				 if (!marshal_arg(a, send_buffer, tc)) {
					 g_warning ("exception, mode == CORBA_PARAM_IN");
					 goto exception;
				 }
				 cur++;
				 break;

			 case CORBA_PARAM_INOUT:
				 a = PyTuple_GetItem(args, cur);
				 if (!marshal_arg(a, send_buffer, tc)) {
					 g_warning ("exception, MODE == CORBA_PARAM_OUT");					
					 goto exception;
				 }

			 case CORBA_PARAM_OUT:
				 g_ptr_array_add(return_types, tc);
				 cur++;
				 break;
		 }
	 }

	 CORBA_exception_free (&ev);
	 /* still need to handle attribute stuff */

	 if (giop_send_buffer_write (send_buffer, con, FALSE)) {
		 g_warning ("Failed to send buffer");
		 giop_recv_list_destroy_queue_entry (mqe);
		 return NULL;
	 }
exception:

	giop_send_buffer_unuse(send_buffer);
	return return_types;
}

GIOPConnection *
demarshal_call (CORBA_Object                 obj,
		GIOPConnection             **con,
		GIOPMessageQueueEntry       *mqe,
		CORBA_unsigned_long          req_id,
		CORBA_OperationDescription  *opd,
		PyObject                    *args,
		GPtrArray                   *return_types,
		PyObject                   **tuple)
{
	GIOPRecvBuffer      *buf;
	CORBA_unsigned_long  i;
	gint status;
/*        d_message (2, "%s (0x%x 0x%x 0x%x 0x%x 0x%x 0x%x) entered",
	  __FUNCTION__, obj, con, mqe, req_id, opd, args); */
	
	buf = giop_recv_buffer_get (mqe, TRUE);

	if (!buf) {
		raise_system_exception(OPExc_COMM_FAILURE, 0, CORBA_COMPLETED_NO, NULL);
		goto exception;
	}

	/* TODO: figure out WTF this does. */
	status = giop_recv_buffer_reply_status (buf);

	if (status != GIOP_NO_EXCEPTION) {
		if (status == GIOP_LOCATION_FORWARD) {
			return ORBit_handle_location_forward (buf, obj);
		} else {
			demarshal_exception (buf,
					     NULL,
					     status,
					     opd,
					     obj->orb);
			goto exception;
		}
	}

	/* We are ready to demarshal now */
	*tuple = PyTuple_New(return_types->len);
	for (i = 0; i < return_types->len; i++) {
		CORBA_TypeCode tc = (CORBA_TypeCode)return_types->pdata[i];
		PyObject *o = demarshal_arg(buf, tc, obj->orb);
		if (!o) 
			goto exception;
		PyTuple_SetItem(*tuple, i, o);
	}
	
exception:
        /* TODO: Handle Python exceptions? */	
	
	giop_recv_buffer_unuse(buf);
	return NULL;	
}
		

PyObject *
_stub_func (CORBA_Object                 obj,
	    PyObject *                   args,
	    CORBA_OperationDescription * opd)
{
	GIOPConnection *con;
	CORBA_unsigned_long req_id;
	GPtrArray *r = NULL;
	PyObject *tuple = NULL;
	GIOPMessageQueueEntry mqe;
	
        /* Figure out how many arguments we need (total args - # OUT args) */
	int need_args = opd->parameters._length, i;

	for (i = 0; i < opd->parameters._length; i++)
		if (opd->parameters._buffer[i].mode == CORBA_PARAM_OUT)
			need_args--;
		
	if (PyTuple_Size(args) != need_args) {
		PyErr_Format(PyExc_TypeError, 
		   "function requires %d arguments; %d given", 
		    need_args, PyTuple_Size(args));
		goto bail;
	}

	con = ORBit_object_get_connection (obj);

retry_request:
	if (!con) {
		raise_system_exception (OPExc_COMM_FAILURE, 0, CORBA_COMPLETED_NO, NULL);
		return NULL;
	}
	
	req_id = GPOINTER_TO_UINT (&obj);
	giop_recv_list_setup_queue_entry (&mqe, con, GIOP_REPLY, req_id);
	r = marshal_call (obj, con, &mqe, req_id, opd, args);
	
	/* Demarshal the results if there are any */
	if (opd->mode == CORBA_OP_ONEWAY && r->len) {
		d_warning(0, "ONEWAY operation has output parameters!");
	} else if (opd->mode != CORBA_OP_ONEWAY && !PyErr_Occurred()) {
		GIOPConnection *newcon;

		newcon = demarshal_call (obj, &con, &mqe, req_id, opd, args, r, &tuple);

		if (newcon) {
			con = newcon;
			goto retry_request;
		}
	}
	g_ptr_array_free(r, TRUE);
bail:
	if (PyErr_Occurred())
		return NULL;

	if (!tuple) {
		Py_INCREF(Py_None);
		return Py_None;
	} else if (PyTuple_Size(tuple) == 0) {
		Py_INCREF(Py_None);
		Py_XDECREF(tuple);
		return Py_None;
	} else if (PyTuple_Size(tuple) == 1) {
		/* don't return a tuple for just one return value */
		PyObject *o = PyTuple_GetItem(tuple, 0);
		Py_INCREF(o);
		/* XXX: Calling Py_XDECREF on this tuple causes instability.  But why?!
		 * I think I fixed this, but I'll leave this comment here just in case.
		 */
		Py_XDECREF(tuple);
		return o;
	}
	return tuple;
}

PyObject *
get_attribute (CORBA_PyInstance_Glue *      glue, 
	       CORBA_AttributeDescription * ad)
{
	CORBA_OperationDescription *opd;
	PyObject *args, *result;
	char *method = g_strconcat("_get_", ad->name, NULL);
	
	opd = find_operation(glue->class_glue, method);
	g_free(method);
	if (!opd) {
		printf("opd == NULL\n");
		raise_system_exception(OPExc_INTERNAL, 0, CORBA_COMPLETED_NO, 0, 
		                "Interface %s not registered.", glue->repo_id);
		return NULL;
	}

	/* make a dummy tuple of size 0 */
	args = PyTuple_New(0);
	result = _stub_func(glue->obj, args, opd);
	Py_XDECREF(args);
	if (!result) {
		d_warning(1, "Apparently get attribute failed");
	}
	return result;
}

PyObject *
set_attribute (CORBA_PyInstance_Glue *      glue, 
	       CORBA_AttributeDescription * ad,
	       PyObject *                   value)
{
	PyObject *result, *tuple;
	CORBA_OperationDescription *opd;
	char *method = g_strconcat("_set_", ad->name, NULL);

	if (ad->mode == CORBA_ATTR_READONLY) {
		raise_system_exception(OPExc_NO_PERMISSION, 0,
				       CORBA_COMPLETED_NO, "attribute %s readonly",
				       ad->name);
		return 0;
	}
	
	opd = find_operation(glue->class_glue, method);
	g_free(method);
	if (!opd) {
		raise_system_exception(OPExc_INTERNAL, 0, CORBA_COMPLETED_NO, 
		                "Interface %s not registered.", glue->repo_id);
		return 0;
	}
	
	tuple = PyTuple_New(1);
	Py_INCREF(value);
	PyTuple_SetItem(tuple, 0, value);
	result = _stub_func(glue->obj, tuple, opd);
	Py_DECREF(tuple);
	if (!result)
		return 0;
	Py_INCREF(Py_None);
	return Py_None;
}

/* Local Variables: */
/* c-file-style: "python" */
/* End: */
