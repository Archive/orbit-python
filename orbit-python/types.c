/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-ofset: 8 -*- */
/*
 * Copyright (C) 2000,2001 Jason Tackaberry <tack@linux.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include "types.h"

#include "idl.h"
#include "util.h"

#define F_M(cv, v, TYPE)                                                   \
	case cv: {                                                         \
		TYPE val = v;                                              \
		CORBA_unsigned_long i;                                     \
		for (i = 0; i < tc->sub_parts; i++) {                      \
			TYPE label_val = *(TYPE *)tc->sublabels[i];        \
			if (label_val == val)                              \
				return i;                                  \
		}                                                          \
		break;                                                     \
	}

PyObject *
Union_PyClass__init (PyObject *_,
		     PyObject *args)
{
	PyObject *self = PyTuple_GetItem(args, 0);
	PyObject *repo_obj = PyObject_GetAttrString(self, "__repo_id");
	CORBA_TypeCode tc = find_typecode(PyString_AsString(repo_obj));\
	Py_DECREF(repo_obj);

	/* FIXME: throw exception */
	if (!tc) {
		d_warning(0, "Can't find typecode for %s", tc->repo_id);
	} else {
		PyObject *d = NULL, *v = NULL;
		PyArg_ParseTuple(args, "OO|O", &self, &d, &v);
		
		/* no discriminator given */
		if (!v) { 
			v = d;
			if (tc->default_index == -1) {
				d = Py_None;
			} else {
				d = PyInt_FromLong(tc->default_index);
			}
		/* verify this discriminator exists */
		} else { 
			CORBA_long i = find_union_arm(tc, d);
			if (i == -1) {
				d_warning(0, "Unknown discriminator value and no default case");
				v = d = Py_None;
			}
		}
		
		/* XXX: should we validate v's type here?  For now, we'll let the
		 * marshaller do that
		 */
		PyObject_SetAttrString(self, "d", d);
		PyObject_SetAttrString(self, "v", v);
	}
	Py_INCREF(Py_None);
	return Py_None;
}


CORBA_long
find_union_arm (CORBA_TypeCode tc,
		PyObject *     d)
{
	d_assert_val(tc, -1);
	if (d == Py_None)
		return tc->default_index;

	switch (tc->discriminator->kind) {
		F_M(CORBA_tk_short, PyInt_AsLong(d), CORBA_short);
		F_M(CORBA_tk_long, PyInt_AsLong(d), CORBA_long);
		F_M(CORBA_tk_ushort, PyInt_AsLong(d), CORBA_unsigned_short);
		F_M(CORBA_tk_ulong, PyInt_AsLong(d), CORBA_unsigned_long);
		F_M(CORBA_tk_longlong, PyInt_AsLong(d), CORBA_long_long);
		F_M(CORBA_tk_ulonglong, PyInt_AsLong(d), CORBA_unsigned_long_long);
		F_M(CORBA_tk_enum, PyInt_AsLong(d), CORBA_unsigned_long);
		case CORBA_tk_boolean:
		{
			CORBA_boolean val = PyInt_AsLong(d);
			CORBA_unsigned_long i;
			for (i = 0; i < tc->sub_parts; i++) {
				CORBA_boolean lv = *(CORBA_boolean *)tc->sublabels[i];
				if (!lv == !val)
					return i;
			}
			break;
		}
		default:
			d_warning (0, "unsupported discriminator: %d", tc->discriminator->kind);
			break;
	}
	return tc->default_index;
}	
		
