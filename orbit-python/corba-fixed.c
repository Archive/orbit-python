/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2001-2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include "corba-fixed.h"
#include "util.h"

static void       CORBA_fixed_PyObject__dealloc (CORBA_fixed_PyObject * self);

static PyObject * CORBA_fixed_PyObject__getattr (CORBA_fixed_PyObject * self,
						 char *                 name);

static int        CORBA_fixed_PyObject__setattr (CORBA_fixed_PyObject * self,
						 char *                 name,
						 PyObject *             obj);


/* Generic CORBA.Object */
PyTypeObject CORBA_fixed_PyObject_Type = {
	PyObject_HEAD_INIT(NULL)
	0,       /*ob_size*/
	"CORBA.fixed",         /*tp_name*/
	sizeof(CORBA_fixed_PyObject),   /*tp_basicsize*/
	0,       /*tp_itemsize*/
	/* methods */
	(destructor)CORBA_fixed_PyObject__dealloc, /*tp_dealloc*/
	0,       /*tp_print*/
	(getattrfunc)CORBA_fixed_PyObject__getattr, /*tp_getattr*/
	(setattrfunc)CORBA_fixed_PyObject__setattr, /*tp_setattr*/
	0,       /*tp_compare*/
	0,       /*tp_repr*/
	0,       /*tp_as_number*/
	0,       /*tp_as_sequence*/
	0,       /*tp_as_mapping*/
	0,       /*tp_hash*/
};

CORBA_fixed_PyObject *
CORBA_fixed_PyObject__new (PyObject *arg)
{
	CORBA_fixed_PyObject *self;
	
	self = PyObject_NEW (CORBA_fixed_PyObject,
			     &CORBA_fixed_PyObject_Type);
	if (!self) {
		return NULL;
	}
	
	return self;
}


static void
CORBA_fixed_PyObject__dealloc (CORBA_fixed_PyObject * self)
{
	d_message(0, "deallocing CORBA.fixed pyobject");
	PyMem_DEL(self);
}

static PyObject *
CORBA_fixed_PyObject__getattr (CORBA_fixed_PyObject * self,
			       char *                 name)
{
	return NULL;
}

static int
CORBA_fixed_PyObject__setattr(CORBA_fixed_PyObject * self,
			      char *                 name,
			      PyObject *             v)
{
	return -1;
}


