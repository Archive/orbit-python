/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000,2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2001,2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include "CORBAmodule.h"

#include "api.h"
#include "corba-any.h"
#include "corba-orb.h"
#include "corba-object.h"
#include "corba-typecode.h"
#include "idl.h"
#include "poa-manager.h"
#include "poa-object.h"
#include "process_idl.h"
#include "util.h"

#include <sys/stat.h>

#define __DOC__                                                                    \
"\n\n"                                                                             \
"ORBit-Python extends the __import__ semantics by automatically searching\n"       \
"IDLPATH for idl files that contain definitions for the requested module.\n"       \
"If an idl file is found, the specified module will be dynamically\n"              \
"generated and returned."                                                          \

PyObject *MainDict, *poa_instances, *main_module,
         *global_client_module, *global_poa_module;

/* maps TypeCodes to Python objects */
GHashTable *object_glue, *poa_modules, *client_modules;

static PyObject *libidl_args = 0;

/*
 *	main CORBA module
 *	Methods handled:
 *		ORB_init
 */

static PyObject * CORBA__ORB_init              (PyObject *self,
						PyObject *args);

static PyObject * CORBA___load_idl             (PyObject *self,
						PyObject *args);

static PyObject * CORBA__TypeCode              (PyObject *self,
						PyObject *args);

static PyObject * CORBA__fixed                 (PyObject *self,
						PyObject *args);

static PyObject * CORBA__Any                   (PyObject *self,
						PyObject *args);

static void       add_include_params_from_path (const char * path);

static char *     get_idl_params_as_string     (GSList *     defs,
						char *       idlfile);

static void       ORBit_Python_init_consts     (PyObject *   ModuleDict);

static void       ORBit_Python_setup_import    (PyObject *module);

static void       ORBit_Python_make_methods    (PyObject *ModuleDict);

static char *     remove_poa_from_str          (const char * _str);

static int        import_from_idl_list         (GSList *     idl_list);

static int        auto_load_module_idls        (const char * modulename,
						PyObject * fromlist);

static PyObject * import_func                  (PyObject * _,
						PyObject * args,
						PyObject * keys);

static PyMethodDef
CORBA_methods[] = {
	{ "ORB_init",  CORBA__ORB_init,  METH_VARARGS },
	{ "_load_idl", CORBA___load_idl, METH_VARARGS },
	{ "TypeCode",  CORBA__TypeCode,  METH_VARARGS },
	{ "Any",       CORBA__Any,       METH_VARARGS },
	{ "fixed",     CORBA__fixed,     METH_VARARGS },
	{ NULL,        NULL }
};



static PyObject *
CORBA__ORB_init (PyObject *self,
		 PyObject *args)
{
	CORBA_ORB_PyObject *orb;
	PyObject *orb_args = 0;
	CORBA_Environment ev;
	CORBA_boolean success, do_free_orbargs = CORBA_FALSE;
	int argc, i;
	char *id = 0, **argv;

	if (!PyArg_ParseTuple(args, "|Os:CORBA.ORB_init", &orb_args, &id)) {
		return NULL;
	}
	
	if (!id) {
		id = "orbit-local-orb";
	}
	
	if (orb_args) {
		if (PyList_Check(orb_args)) {
			orb_args = PyList_AsTuple(orb_args);
			do_free_orbargs = CORBA_TRUE;
		} else if (!PyTuple_Check(orb_args)) {
			PyErr_Format(PyExc_TypeError, 
			             "parameter 1 expected either list or tuple, got %s",
			             orb_args->ob_type->tp_name);
			return NULL;
		}
	
		/* setup the args from orb_args */
		argc = PyTuple_Size(orb_args) + 1;
	} else {
		argc = 1;
	}

	argv = g_new(char *, argc);
	argv[0] = g_strdup("orbit-python"); /* kludge */
	for (i = 1; i < argc; i++) {
		PyObject *o = PyObject_Str(PyTuple_GetItem(orb_args, i - 1));
		argv[i] = g_strdup(PyString_AsString(o));
		Py_DECREF(o);
	}
	
	CORBA_exception_init(&ev);
	orb = CORBA_ORB_PyObject__new(CORBA_ORB_init(&argc, argv, id, &ev));
	success = check_corba_ex(&ev);
	CORBA_exception_free(&ev);

	for (i = 0; i < argc; i++) {
		g_free(argv[i]);
	}
	g_free(argv);
	
	if (do_free_orbargs) {
		Py_DECREF(orb_args);
	}
	
	if (!success) {
		return NULL;
	}
	
	return (PyObject *)orb;
}

static PyObject *
CORBA___load_idl (PyObject *self, PyObject *args)
{
	char *file;
	GSList *defs;
	char *params;
	CORBA_boolean res;
	
	if (!PyArg_ParseTuple (args, "s:CORBA._load_idl", &file)) {
		return NULL;
	}
	
	defs = get_defines_for_file(file);
	if (defs) {
		g_slist_free (defs);
	}
		
	params = get_idl_params_as_string(defs, file);
	g_free(params);

	res = parse_idl (file, params);
	if (res) {
		set_file_as_loaded (file, NULL);
		
		Py_INCREF(Py_None);
		return Py_None;
	}
	
	return NULL;
}

static PyObject *
CORBA__TypeCode (PyObject *self,
		 PyObject *args)
{
	PyObject *                o;
	char *                    repo_id;
	CORBA_TypeCode            tc;
	CORBA_TypeCode_PyObject * tc_object;

	if (!PyArg_ParseTuple(args, "O", &o)) {
		return NULL;
	}
	
	if (PyString_Check(o)) {
		repo_id = PyString_AsString(o);
	} else {
		PyObject *repo_obj = PyObject_GetAttrString(o, "__repo_id");
		if (!repo_obj) {
			PyErr_Format(PyExc_TypeError, "Parameter is not a CORBA.Object");
			return NULL;
		}
		repo_id = PyString_AsString(repo_obj);
		Py_DECREF(repo_obj);
	}
	tc = find_typecode(repo_id);
	if (!tc) {
		PyErr_Format(PyExc_TypeError, "Unregistered repoid %s", repo_id);
		return NULL;
	}

	tc_object = CORBA_TypeCode_PyObject__new(tc);
	CORBA_Object_release((CORBA_Object)tc, NULL);
	return (PyObject *)tc_object;
}

static PyObject *
CORBA__fixed (PyObject *self,
	      PyObject *args)
{
	return (PyObject *)CORBA_fixed_PyObject__new(args);
}

static PyObject *
CORBA__Any (PyObject *self,
	    PyObject *args)
{
	CORBA_TypeCode_PyObject * tc;
	PyObject *                val;
	
	if (!PyArg_ParseTuple(args, "O!O:CORBA.Any", &CORBA_TypeCode_PyObject_Type, 
	                      &tc, &val)) {
		return NULL;
	}

	return (PyObject *)CORBA_Any_PyObject__new(tc, val);
}

static void
add_include_params_from_path (const char *path)
{
	char *p = g_strdup(path), *q, *save = p, *tmp;
	
	if (!p || !*p) p = ".";
	while ((q = strstr(p, ":")) != NULL) {
		*q = 0;
		tmp = g_strconcat("-I", p, NULL);
		PyList_Append(libidl_args, PyString_FromString(tmp));
		g_free(tmp);
		p = q + 1;
	}
	if (*p) {
		tmp = g_strconcat("-I", p, NULL);
		PyList_Append(libidl_args, PyString_FromString(tmp));
		g_free(tmp);
	}
	g_free(save);
}

static char *
get_idl_params_as_string (GSList *defs, char *idlfile)
{
	gchar *params;
	gint i;
	gchar *tmp;
	const gchar *base;
	gchar *arg;
	gchar *filename;
	
	params = g_strdup ("");
	
	for (i = 0; i < PyList_Size(libidl_args); i++) {
		arg = PyString_AsString(PyList_GetItem(libidl_args, i));
		tmp = g_strconcat(params, " ", arg, NULL);
		g_free(params);
		params = tmp;
	}

	base = g_basename (idlfile);
	if (strlen (base) <= 4) {
		g_message ("skipping case base is short base=%s params=%s", base, idlfile);
		return params;
	}
	
	filename = g_strndup (base, strlen(base)-4);
	
	/* Replace all - with _ */
	i = 0;
	while (filename[i]) {
		if (filename[i] == '-') {
			filename[i] = '_';
		}
		i++;
	}

	/* define -D__filename_COMPILATION, which bonobo etc uses */
	params = g_strconcat (params, " -D__", filename, "_COMPILATION", NULL);
	g_free (filename);
	
	return params;
}

PyMethodDef empty_methods[] = {
	{ NULL, NULL }
};

/* This function removes the __POA from the toplevel module and returns
 * a newly-allocated string.
 */
static char *
remove_poa_from_str (const char *_str)
{
	char *str = g_strdup(_str), /* save str so we can mess with it */
		 *p, *ret;

	p = strstr(str, ".");
	if (p)
		*p = 0;
	if (!strncmp(str + strlen(str) - 5, "__POA", 5))
		*(str + strlen(str) - 5) = 0;

	if (p)
		ret = g_strconcat(str, ".", p + 1, NULL);
	else
		ret = g_strdup(str);
	
	g_free(str);
	return ret;
}
	
int import_from_idl_list(GSList *idl_list)
{
	GSList *p;
	GSList *defs = NULL;
	char *params = NULL;
	GHashTable * loaded;
	int i;
	
	loaded = g_hash_table_new (g_str_hash, g_str_equal);

	for (p = idl_list; p; p = p->next) {
		defs = get_defines_for_file(p->data);
		params = get_idl_params_as_string(defs, p->data);
		if (defs) {
			g_slist_free(defs);
		}

		d_message(1, "auto-loading: %s %s", (char *)p->data, params);
		parse_idl (p->data, params);
		set_file_as_loaded(p->data, &loaded);
		g_free(params);
	}
	
	d_message (5, "%s: returning 1", __FUNCTION__);
	return 1;

}

static int
auto_load_module_idls (const char *modulename, PyObject *fromlist)
{
	GSList *idl_list = 0;
	char *modstr = 0;
	char *failed = NULL;
	int from_global = 0;
	int ret;
	int i;
	
	d_message (5, "%s: entered name=%s", __FUNCTION__, modulename);
	
	if ( !fromlist || fromlist == Py_None ) {
		
		/* Import in the form:
		 *	import SomeModule or __import__("SomeModule")
	 	 */
		modstr = remove_poa_from_str(modulename);
		if ( !strcmp(modstr, "_GlobalIDL") ) {
			idl_list = get_global_idl_files();
		} else {
			idl_list = get_idl_list_for_module( modstr, 0, &failed );
		}
		
		g_free(modstr);
		ret = import_from_idl_list(idl_list);
		g_slist_free(idl_list);

		return ret;
	} 
	
	/* Import in the format:
	 *	 from SomeModule import SomeInterface
	 */
	if (!strncmp(modulename, "_GlobalIDL", 10)) {
		modstr = g_strdup("");
		from_global = 1;
	} else {
		/* Save the existing modstr so we can muck around with it */
		modstr = g_strconcat(modulename, ".", NULL);
	}
	
	/* leak warning: care has been taken to assure that by this point 
	 * modstr will have been allocated into a new string. don't break 
         * that. 
	 */ 
	for (i = 0; i < PySequence_Length(fromlist); i++) {
		GSList *curmod;
		GSList *p;
		char *tmp, *idlmod;
		PyObject *from = PySequence_GetItem(fromlist, i);

		tmp = g_strconcat(modstr, PyString_AsString(from), NULL);
		Py_DECREF(from);
		idlmod = remove_poa_from_str(tmp);
		g_free(tmp);
		curmod = get_idl_list_for_module(idlmod, from_global, &failed);
		g_free(idlmod);

		if (!curmod) {
			/* failed is non-zero when the top-level module _does_
			 * exist as an IDL module.
			 */
			g_slist_free(idl_list);
			g_slist_free(curmod);
			g_free(modstr);
			if (failed) {
				PyErr_Format(PyExc_ImportError, 
				             "No module named %s", failed);
				g_free(failed);
				return 0;
			}
			
			/* Failed is zero so the top-level module doesn't exist as
			 * an IDL module. We return 1 so the caller will try 
			 * ImportModule
			 */
			
			return 1;
		}
		p = curmod;
		while (p) {
			GSList *q = idl_list;
			char flag = 0;
			while (q) {
				if (!strcmp(q->data, p->data)) {
					flag = 1;
					break;
				}
				q = q->next;
			}
			if (!flag) {
				idl_list = g_slist_append(idl_list, p->data);
			}
			p = p->next;
		}
		g_slist_free(curmod);
	}

	g_free(modstr);
	ret = import_from_idl_list(idl_list);
	g_slist_free(idl_list);
	return ret;

}

	
static PyObject *
import_func (PyObject *_, PyObject *args, PyObject *keys)
{

	char *modname;
	PyObject *globals = NULL; 
	PyObject *locals = NULL; 
	PyObject *fromlist = NULL;
	GHashTable *modulehash = NULL;
	
	if (!PyArg_ParseTuple (args, "s|OOO", &modname, &globals,
			       &locals, &fromlist)) {
		return NULL; 
	}

	if (!auto_load_module_idls (modname, fromlist)) {
		return 0;
	}
	
	return PyImport_ImportModuleEx (modname, globals, locals, fromlist);
}

static struct _ORBitPython_FunctionStruct functions = 
{
	VERSION,                       /* orbit_python_version */

	CORBA_Object_to_PyObject,
	PyORBit_Object_Check, 
	PyORBit_Object_Get, 

	&CORBA_ORB_PyObject_Type,      CORBA_ORB_PyObject__new,

	&CORBA_Any_PyObject_Type,
	PyORBit_Any_New,
	CORBA_Any_PyObject__new,
	PyORBit_Any_Get,

	&CORBA_TypeCode_PyObject_Type, CORBA_TypeCode_PyObject__new,
	&POA_PyObject_Type,            POA_Object_to_PyObject,
	&POAManager_PyObject_Type,     POAManager_Object_to_PyObject
};

static void
ORBit_Python_init_consts (PyObject *ModuleDict)
{
	PyDict_SetItemString (ModuleDict, "TRUE", PyInt_FromLong(1));
	PyDict_SetItemString (ModuleDict, "FALSE", PyInt_FromLong(0));
	PyDict_SetItemString (ModuleDict, "ORB_ID", 
			      PyString_FromString ("orbit-local-orb"));
	PyDict_SetItemString (ModuleDict, "COMPLETED_YES", PyInt_FromLong(0));
	PyDict_SetItemString (ModuleDict, "COMPLETED_NO", PyInt_FromLong(1));
	PyDict_SetItemString (ModuleDict, "COMPLETED_MAYBE", PyInt_FromLong(2));
}

static void
ORBit_Python_setup_import (PyObject *module)
{
	PyObject *old_import;
	PyObject *old_doc;
	PyObject *func;
	PyMethodDef *def;
	char *old_doc_str = 0, *new_doc_str = 0;
	
	/* get default __import__.__doc__ string */
	old_import = PyObject_GetAttrString (module,
					     "__import__");
	
	old_doc = PyObject_GetAttrString (old_import, "__doc__");
	old_doc_str = PyString_AsString (old_doc);
	new_doc_str = g_strconcat (old_doc_str, __DOC__, NULL );
	Py_DECREF(old_import);
	Py_DECREF(old_doc);

	/* attach our own import handler to __builtins__ */
	def = g_new(PyMethodDef, 1);
	def->ml_name  = g_strdup("__import__");
	def->ml_meth  = (PyCFunction)import_func;
	def->ml_flags = METH_VARARGS | METH_KEYWORDS;
	def->ml_doc   = new_doc_str;
	
	func = PyCFunction_New (def, module);
	PyObject_SetAttrString (module, "__import__", func);
}

static void
ORBit_Python_setup_idl (PyObject *ModuleDict)
{
	char *p;

	global_client_module = Py_InitModule("_GlobalIDL", empty_methods);
	global_poa_module = Py_InitModule("_GlobalIDL__POA", empty_methods);
	root_poa = NULL;

	libidl_args = PyList_New(0);
	PyList_Append(libidl_args, PyString_FromString("-D__ORBIT_IDL__"));
	PyDict_SetItemString(ModuleDict, "_libidl_args", libidl_args);

	p = g_strdup(getenv("IDLPATH"));
	if (!p || !*p) {
		struct stat b;
		g_free(p);
		p = g_strconcat(".:",
		                !stat("/usr/share/idl", &b) ? "/usr/share/idl:" : "",
		                !stat("/usr/local/share/idl", &b) ? "/usr/local/share/idl:" : "",
		                NULL);
		/* remove trailing ':' */
		if (*(p + strlen(p) - 1) == ':') {
			*(p + strlen(p) - 1) = 0;
		}
	}
	
	process_idl_paths(p);
	add_include_params_from_path(p);
	g_free(p);
}

#define make_method(name, f)                                   \
	def = g_new(PyMethodDef, 1);                           \
	def->ml_name = g_strdup(name);                         \
	def->ml_meth = (PyCFunction)f;                         \
	def->ml_flags = METH_VARARGS;                          \
	func = PyCFunction_New(def, corba_object_class);       \
	meth = PyMethod_New(func, NULL, corba_object_class);   \
	PyObject_SetAttrString(corba_object_class, name, meth)

static void
ORBit_Python_init_portable_server (void)
{
	servant_base = PyClass_New(NULL, PyDict_New(),
	                           PyString_FromString("Servant"));
	PyObject_SetAttrString(servant_base, "__module__",
	                       PyString_FromString("PortableServer"));
	
	poa_objects = g_hash_table_new(g_direct_hash, g_direct_equal);

}

static void
ORBit_Python_make_methods (PyObject *ModuleDict)
{
	PyMethodDef *def;
	PyObject *func;
	PyObject *meth;
	
	corba_object_class = PyClass_New (NULL,
					  PyDict_New(), 
					  PyString_FromString("Object"));
	
	PyObject_SetAttrString (corba_object_class,
				"__module__",
				PyString_FromString("CORBA"));
	
	PyDict_SetItemString (ModuleDict, "Object", corba_object_class);

	make_method("__init__", CORBA_PyClass__init);
	make_method("__del__", CORBA_PyClass__del);
	make_method("__invoke", CORBA_PyClass___invoke);
	make_method("__setattr__", CORBA_PyClass__setattr__);
	make_method("__getattr__", CORBA_PyClass__getattr__);
	make_method("_is_a", CORBA_PyClass___is_a);
	make_method("_is_equivalent", CORBA_PyClass___is_equivalent);
	make_method("_hash", CORBA_PyClass___hash);
	make_method("_non_existent", CORBA_PyClass___non_existent);
	make_method("_narrow", CORBA_PyClass___narrow);
}

void
initCORBA (void)
{
	PyObject *m;
	PyObject *ModuleDict;
	
	CORBA_ORB_PyObject_Type.ob_type = &PyType_Type;
	POAManager_PyObject_Type.ob_type = &PyType_Type;
	POA_PyObject_Type.ob_type = &PyType_Type;
	CORBA_Any_PyObject_Type.ob_type = &PyType_Type;
	CORBA_TypeCode_PyObject_Type.ob_type = &PyType_Type;
	CORBA_fixed_PyObject_Type.ob_type = &PyType_Type;


	m = Py_InitModule("CORBA", CORBA_methods);
	ModuleDict = PyModule_GetDict(m);
	
	PyDict_SetItemString (ModuleDict, "_ORBitPython_API",
			      PyCObject_FromVoidPtr(&functions, NULL));
	

	/* Global hashes */
	object_glue              = g_hash_table_new (g_str_hash, g_str_equal);
	poa_modules              = g_hash_table_new (g_str_hash, g_str_equal);
	client_modules           = g_hash_table_new (g_str_hash, g_str_equal);
	object_instance_glue     = g_hash_table_new (g_direct_hash, g_direct_equal);
	stub_repo_ids            = g_hash_table_new (g_direct_hash, g_direct_equal);
	object_to_instances_hash = g_hash_table_new (g_direct_hash, g_direct_equal);
	orb_objects              = g_hash_table_new (g_direct_hash, g_direct_equal);

	ORBit_Python_init_typecodes ();
	ORBit_Python_init_exceptions( ModuleDict);
	ORBit_Python_init_server ();
	ORBit_Python_init_consts (ModuleDict);
	ORBit_Python_init_marshal ();
	ORBit_Python_init_portable_server ();

	ORBit_Python_setup_idl (ModuleDict);
	ORBit_Python_make_methods (ModuleDict);

	m = PyImport_ImportModule ("PortableServer");
	servant_base = PyObject_GetAttrString (m, "Servant");

	/* Do this after PortableServer is imported */
	m = PyImport_ImportModule ("__builtin__");
	ORBit_Python_setup_import (m);

}

/* Local Variables: */
/* c-file-style: "python" */
/* vim: set noet ai fo=tcq sts=5 ts=5 sw=5 wrapmargin=8: */
/* End: */
