/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2001 Jason Tackaberry <tack@linux.com>
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com> 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public 
 * License as published by the Free Software Foundation; either 
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Jason Tackaberry
 *
 */

#include "api.h"

#include "corba-object.h"
#include "corba-typecode.h"
#include "corba-any.h"
#include "exceptions.h"
#include "orbit-python-types.h"
#include "util.h"

static gpointer     ORBit_demarshal_allocate_mem (CORBA_TypeCode tc,
						  gint           nelements);

static PyObject *   decode_any_value  (CORBA_TypeCode  tc,
				       gpointer       *value,
				       CORBA_ORB       orb);

static int          encode_any_value  (CORBA_TypeCode  tc,
				       gpointer       *ret,
				       PyObject       *value);

CORBA_Any_PyObject *
PyORBit_Any_New (CORBA_any *any)
{
	CORBA_TypeCode_PyObject *tc;
	CORBA_Any_PyObject *anyobj;
	PyObject *val;
	gpointer any_value;
	d_assert(any != CORBA_OBJECT_NIL);
	any_value = any->_value;

	val = decode_any_value(any->_type, &any_value, NULL);
	if (!val)
		return NULL;

	tc = CORBA_TypeCode_PyObject__new (any->_type);
	anyobj = CORBA_Any_PyObject__new (tc, val);
	Py_DECREF(tc);
	Py_DECREF(val);
	return anyobj;
}

CORBA_boolean
PyORBit_Object_Check (PyObject *obj)
{
	void *p = g_hash_table_lookup (object_instance_glue, obj);
	return p ? CORBA_TRUE : CORBA_FALSE;
}

CORBA_Object
PyORBit_Object_Get (PyObject *obj)
{
	CORBA_PyInstance_Glue *inst;
	
	inst = g_hash_table_lookup(object_instance_glue, obj);
	
	if (!inst) {
		return CORBA_OBJECT_NIL;
	} else {
		return inst->obj;
	}
}

CORBA_any *
PyORBit_Any_Get (CORBA_Any_PyObject *anyobj)
{
	gpointer        val = NULL;
	CORBA_any      *any;
	CORBA_TypeCode  tc;
	d_assert(anyobj != NULL && CORBA_Any_PyObject_Check(anyobj));

	tc = anyobj->tc_object->tc;

	val = ORBit_demarshal_allocate_mem(tc, 1);
	any = CORBA_any__alloc();
	any->_type = tc; // maybe need to duplicate this?
	any->_value = val;
	encode_any_value(tc, &val, anyobj->value);
	if (PyErr_Occurred()) // TODO: cleanup
		return NULL;

	return any;
}

/* The code below is alpha quality, and incomplete. */

static PyObject *
decode_sequence(CORBA_TypeCode tc, gpointer *value, CORBA_ORB orb)
{
	CORBA_sequence_CORBA_octet *seq = *value;
	gpointer subval;
	int i;
	PyObject *tuple;

	d_assert(seq != NULL);
	subval = seq->_buffer;
	
	tuple = PyTuple_New(seq->_length);
	for (i = 0; i < seq->_length; i++) {
		PyObject *svo = decode_any_value(tc->subtypes[0], &subval, orb);
		if (!svo)
			goto bail;
		PyTuple_SetItem(tuple, i, svo);
	}

	*value += sizeof(CORBA_sequence_CORBA_octet);
	return tuple;
bail:
	Py_DECREF(tuple);
	return NULL;
}
	
static PyObject *
decode_array (CORBA_TypeCode tc, gpointer *value, CORBA_ORB orb)
{
	int i;
	PyObject *tuple;

	tuple = PyTuple_New(tc->length);
	for (i = 0; i < tc->length; i++) {
		PyObject *svo = decode_any_value(tc->subtypes[0], value, orb);
		if (!svo)
			goto bail;
		PyTuple_SetItem(tuple, i, svo);
	}
	return tuple;
bail:
	Py_DECREF(tuple);
	return NULL;
}
	
static PyObject *
decode_struct(CORBA_TypeCode tc, gpointer *value, CORBA_ORB orb)
{
	PyObject *cl, *data;
	int i;

	cl = (PyObject *)g_hash_table_lookup(object_glue, tc->repo_id);
	if (!cl) // maybe it's an exception?
		cl = (PyObject *)g_hash_table_lookup(exceptions, tc->repo_id);

	if (!cl) // probably not really the correct exception to raise ...
		return raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_NO,
		                              "Asked to decode unknown struct (%s)",
		                               tc->repo_id);

	data = PyInstance_New(cl, NULL, NULL);
	for (i = 0; i < tc->sub_parts; i++) {
		PyObject *o = decode_any_value(tc->subtypes[i], value, orb);
		if (!o) goto bail;
		PyObject_SetAttrString(data, (char *)tc->subnames[i], o);
		Py_DECREF(o);
	}
	return data;
bail:
	Py_DECREF(data);
	return NULL;
}

static PyObject *
decode_union(CORBA_TypeCode tc, gpointer *value, CORBA_ORB orb)
{
	PyObject *cl, *inst = NULL, *d, *v, *tuple;
	CORBA_long arm;

	cl = (PyObject *)g_hash_table_lookup(object_glue, tc->repo_id);

	if (!cl) // probably not really the correct exception to raise ...
		return raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_NO,
		                              "Asked to decode unknown union (%s)",
		                               tc->repo_id);
	d = decode_any_value(tc->discriminator, value, orb);
	if (!d)
		return NULL;

	arm = find_union_arm(tc, d);
	if (PyErr_Occurred())
		goto bail; // exception from find_union_arm
	if (arm < 0) {
		raise_system_exception(OPExc_MARSHAL, 0, CORBA_COMPLETED_NO,
		                       "Unknown union arm");
		goto bail;
	}

	v = decode_any_value(tc->subtypes[arm], value, orb);
	if (!v)
		goto bail;

	tuple = Py_BuildValue("OO", d, v);
	Py_DECREF(v);
	inst = PyInstance_New(cl, tuple, NULL);
	Py_DECREF(tuple);
bail:
	Py_DECREF(d);
	return inst;
}

/* TODO: handle alignment
 */
static PyObject *
decode_any_value(CORBA_TypeCode tc, gpointer *value, CORBA_ORB orb)
{
	PyObject *ret = NULL;
	switch (tc->kind) {
		case CORBA_tk_null:
			Py_INCREF(Py_None);
			ret = Py_None;
			break;
		case CORBA_tk_void:
			break;
		case CORBA_tk_string:
			ret = PyString_FromString(*(CORBA_char **)*value);
			*value += sizeof(CORBA_char *);
			break;
		case CORBA_tk_short:
		case CORBA_tk_ushort:
		case CORBA_tk_boolean:
			ret = Py_BuildValue("h", *(CORBA_short *)*value);
			*value += sizeof(CORBA_short);
			break;
		case CORBA_tk_char:
			ret = Py_BuildValue("c", *(CORBA_char *)*value);
			*value += sizeof(CORBA_char);
			break;
		case CORBA_tk_octet:
			ret = Py_BuildValue("b", *(CORBA_octet *)*value);
			*value += sizeof(CORBA_octet);
			break;
		case CORBA_tk_long:
		case CORBA_tk_ulong:
		case CORBA_tk_longlong:
		case CORBA_tk_ulonglong:
		case CORBA_tk_enum:
			ret = PyInt_FromLong(*(CORBA_long *)*value);
			*value += sizeof(CORBA_long);
			break;
		case CORBA_tk_float: 
			ret = Py_BuildValue("f", *(CORBA_float *)*value);
			*value += sizeof(CORBA_float);
			break;
		case CORBA_tk_double: 
			ret = PyFloat_FromDouble(*(CORBA_double *)*value);
			*value += sizeof(CORBA_double);
			break;
		case CORBA_tk_alias: 
			return decode_any_value(tc->subtypes[0], value, orb);
			

		case CORBA_tk_sequence:
			ret = decode_sequence(tc, value, orb);
			break;
		case CORBA_tk_array:
			ret = decode_array(tc, value, orb);
			break;
		case CORBA_tk_struct:
		case CORBA_tk_except:
			ret = decode_struct(tc, value, orb);
			break;
		case CORBA_tk_union:
			ret = decode_union(tc, value, orb);
			break;
		case CORBA_tk_any:
			ret = (PyObject *)PyORBit_Any_New(*(CORBA_any **)*value);
			*value += sizeof(CORBA_any *);
			break;
		case CORBA_tk_objref: // XXX: I have no idea if this will work
			ret = CORBA_Object_to_PyObject(*(CORBA_Object *)*value);
			*value += sizeof(CORBA_Object);
			break;

		default:
			d_warning(0, "Can't decode unsupported typecode: %d", tc->kind);
			return NULL;
	}
	
	return ret;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

static gpointer
ORBit_demarshal_allocate_mem(CORBA_TypeCode tc, gint nelements)
{
	size_t block_size;
	gpointer retval = NULL;

	if (!nelements)
		return retval;

	block_size = ORBit_gather_alloc_info(tc);

	if (block_size) {
#ifdef OLD		
		retval = ORBit_alloc_2(block_size * nelements,
		                       (ORBit_free_childvals) ORBit_free_via_TypeCode,
		                       GINT_TO_POINTER (nelements),
		                       sizeof(CORBA_TypeCode));
		*(CORBA_TypeCode *)((char *)retval-sizeof(ORBit_mem_info)-sizeof(CORBA_TypeCode)) =
			(CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)tc, NULL);
#else
		retval = ORBit_alloc_with_free_fn(block_size * nelements,
						  nelements,
						  (ORBit_Mem_free_fn)ORBit_freekids_via_TypeCode);
		*(CORBA_TypeCode *)((char *)retval-sizeof (CORBA_TypeCode)) =
                        (CORBA_TypeCode)CORBA_Object_duplicate ((CORBA_Object)tc, NULL);

#endif
	}

	return retval;
}

static int
encode_sequence (CORBA_TypeCode tc,
		 gpointer *ret,
		 PyObject *value)
{
	int i, length;
	CORBA_sequence_CORBA_octet *seq = *ret;
	gpointer subvalbuf;

	d_assert_val(PySequence_Check(value), 0);
	length = PySequence_Length(value);
	d_assert_val(!tc->length || length <= tc->length, 0);

	seq->_length = length;
	seq->_maximum = length;
	if (length)
		seq->_buffer = ORBit_demarshal_allocate_mem(tc->subtypes[0], length);
	else 
		seq->_buffer = NULL;
	
	subvalbuf = seq->_buffer;
	for (i = 0; i < length; i++) {
		PyObject *subvalobj = PySequence_GetItem(value, i);
		if (!encode_any_value(tc->subtypes[0], &subvalbuf, subvalobj)) {
			Py_DECREF(subvalobj);
			return 0;
		}
		Py_DECREF(subvalobj);
	}

	return 1;
}

/*
 * TODO: handle alignment
 */
static int
encode_any_value (CORBA_TypeCode  tc,
		  gpointer       *ret,
		  PyObject       *value)
{
	d_assert_val(value != NULL, 0);

	switch (tc->kind) {
		case CORBA_tk_null:
		case CORBA_tk_void:
			break;
		case CORBA_tk_string:
			d_assert_val(PyString_Check(value), 0);
			*(CORBA_char **)*ret = PyString_AsString(value);
			*ret += sizeof(CORBA_char *);
			break;
		case CORBA_tk_short:
		case CORBA_tk_ushort:
		case CORBA_tk_boolean:
			d_assert_val(PyInt_Check(value), 0);
			*(CORBA_short *)*ret = (CORBA_short)PyInt_AsLong(value);
			*ret += sizeof(CORBA_short);
			break;
		case CORBA_tk_char:
			d_assert_val(PyString_Check(value), 0);
			*(CORBA_char *)*ret = PyString_AsString(value)[0];
			*ret += sizeof(CORBA_char);
			break;
		case CORBA_tk_octet:
			d_assert_val(PyInt_Check(value), 0);
			*(CORBA_octet *)*ret = (CORBA_octet)PyInt_AsLong(value);
			*ret += sizeof(CORBA_short);
			break;
		case CORBA_tk_long:
		case CORBA_tk_ulong:
		case CORBA_tk_longlong:
		case CORBA_tk_ulonglong:
		case CORBA_tk_enum:
			d_assert_val(PyInt_Check(value), 0);
			*(CORBA_long *)*ret = (CORBA_long)PyInt_AsLong(value);
			*ret += sizeof(CORBA_long);
			break;
		case CORBA_tk_float: 
			d_assert_val(PyFloat_Check(value), 0);
			*(CORBA_float *)*ret = (CORBA_float)PyFloat_AsDouble(value);
			*ret += sizeof(CORBA_float);
			break;
		case CORBA_tk_double: 
			d_assert_val(PyFloat_Check(value), 0);
			*(CORBA_double *)*ret = (CORBA_double)PyFloat_AsDouble(value);
			*ret += sizeof(CORBA_double);
			break;
		case CORBA_tk_alias: 
			return encode_any_value(tc->subtypes[0], ret, value);
			

		case CORBA_tk_sequence:
			return encode_sequence(tc, ret, value);
		case CORBA_tk_array:
//			ret = encode_array(tc, value);
			break;
		case CORBA_tk_struct:
		case CORBA_tk_except:
//			ret = encode_struct(tc, value);
			break;
		case CORBA_tk_union:
//			ret = encode_union(tc, value);
			break;
		case CORBA_tk_any:
//			ret = (PyObject *)PyORBit_Any_New(*(CORBA_any **)*value);
//			*value += sizeof(CORBA_any *);
			break;
		case CORBA_tk_objref: // XXX: I have no idea if this will work
//			ret = CORBA_Object_to_PyObject(*(CORBA_Object *)*value);
//			*value += sizeof(CORBA_Object);
			break;

		default:
			d_warning(0, "Can't encode unsupported typecode: %d", tc->kind);
			break;
	}
	return 1;
}

