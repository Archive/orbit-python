/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Johan Dahlin <jdahlin@telia.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Johan Dahlin
 *
 */

#ifndef __POA_OBJECT_H__
#define __POA_OBJECT_H__

#include "orbit-python.h"

/* PortableServer.Servant */
extern PyObject *   servant_base; 

extern PyMethodDef  PortableServer_methods[];
extern GHashTable * poa_objects;

POA_PyObject * POA_Object_to_PyObject             (PortableServer_POA object);

PyObject *     POA_PyObject__servant_to_reference (POA_PyObject *     self,
						   PyObject *         args);

PyObject *     POA_PyObject__deactivate_object    (POA_PyObject * self,
						   PyObject *     args);


#endif /* __POA_OBJECT_H__ */
